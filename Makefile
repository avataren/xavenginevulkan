CC = clang++
LIBS = -lvulkan -lglfw -ldl -lpthread -lassimp

OBJS = BufferHelper.o Camera.o CommandBuffers.o Context.o DescriptorPool.o DescriptorSetLayout.o DescriptorSets.o DeviceHelper.o FrameBuffer.o FullImage.o GeometryBuffers.o GraphicsPipeline.o ImageHelper.o ImgUIRenderer.o main.o MemoryHelper.o Mesh.o MeshModel.o Renderer.o RenderPass.o ResourceLoader.o SwapChain.o Synchronization.o TextureManager.o TextureSampler.o UI_Asset_Progress_Bars.o UI_PerformanceMonitor.o UI_Settings.o UI_TopMenu.o UI_XavUI.o UniformBuffers.o UniformData.o WindowManager.o

IMGUIOBJS = imgui.o imgui_demo.o imgui_draw.o imgui_impl_glfw.o imgui_impl_vulkan.o imgui_tables.o imgui_widgets.o implot.o implot_demo.o implot_items.o

default: xav

xav: $(OBJS) $(IMGUIOBJS)
	$(CC) $(LIBS) $^ -o xav

%.o: XavEngine/%.cpp
	$(CC) -c $< -o $@

%.o: XavEngine/imgui/%.cpp
	$(CC) -c $< -o $@

.PHONY: clean
clean:
	rm *.o
