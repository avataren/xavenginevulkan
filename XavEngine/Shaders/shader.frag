#version 450

#extension GL_ARB_separate_shader_objects : enable

layout (location = 0) in vec3 fragNormal;
layout (location = 1) in vec2 fragTex;
layout (location = 2) in vec3 worldPosFrag;
layout (location = 3) in mat3 tbn;

layout (set = 1, binding = 0) uniform sampler2D  textures[4096];

layout (push_constant) uniform PushModel{
	layout(offset = 112) vec4 drawInfo;
}pushModel;

layout(location = 0) out vec4 outColor;		// Final output color (must also have location, not located to input layout, but attachment location)

layout(set = 0, binding = 0) uniform UniformData{
	mat4 projection;
	mat4 view;
	vec2 screenExtent;
	float time;
	float aspect;	
} uniformData;

struct Light{
	vec3 position;
	vec4 color;
	vec4 info;
};

layout(set = 0, binding = 1) uniform LightData {
	Light lights[1024];
} lightData;


void main() {
	int textureOffset = int(pushModel.drawInfo.x);
	int lightCount = int(pushModel.drawInfo.y);

	vec3 normal = normalize(fragNormal);
	vec3 pixelNormal = normalize(tbn * (texture(textures[textureOffset+1], fragTex ).xyz * 2.0 - 1.0 ));
	vec4 tex = texture(textures[textureOffset], fragTex);
	float roughness = texture(textures[textureOffset+2], fragTex).r;
	float metallic = texture(textures[textureOffset+3], fragTex).r;
	
	if (tex.a < 0.4) discard;
	
	vec3 finalCol = tex.xyz*0.0025;
	
	for (int i=0;i<lightCount;i++)
	{
		float radius = lightData.lights[i].info.x;
		vec3 dir = lightData.lights[i].position - worldPosFrag;
		float dist = length(dir);
		//dir = normalize(dir);
		if (dist < radius)
		{
			float att = 1 - (dist / radius);
			att *= att;

			float l = min(1.0, max(0,dot(dir, pixelNormal)/dist));
			finalCol += ( l * tex.xyz * lightData.lights[i].color.xyz * att);
		}
	}
	
	outColor = vec4(finalCol,tex.a);
	
}
