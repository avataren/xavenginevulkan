#version 450 	// Use GLSL 4.5

#extension GL_ARB_separate_shader_objects : enable

layout (location = 0) in vec3 pos;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 tex;
layout (location = 3) in vec3 tangent;
layout (location = 4) in vec3 bitangent;


layout(set = 0, binding = 0) uniform UniformData{
	mat4 projection;
	mat4 view;
	vec2 screenExtent;
	float time;
	float aspect;	
} uniformData;

// NOT IN USE, LEFT FOR REFERENCE
layout(set = 0, binding = 1) uniform UboModel{
	mat4 model;
} uboModel;

layout (push_constant) uniform PushModel{
	mat4 worldTransform;
	vec4 v1;
	vec4 v2;
	vec4 v3;
	//layout(offset = 64) mat3 normalMatrix;
}pushModel;

layout (location = 0) out vec3 fragNormal;
layout (location = 1) out vec2 fragTex;
layout (location = 2) out vec3 worldPosFrag;
layout (location = 3) out mat3 tbn;

void main(){

	vec4 viewPos =uniformData.view * pushModel.worldTransform * vec4(pos,1.0);
	gl_Position = uniformData.projection  * viewPos;
	worldPosFrag = (pushModel.worldTransform * vec4(pos,1.0)).xyz;
	mat3 normalMatrix = mat3(pushModel.v1.xyz, pushModel.v2.xyz, pushModel.v3.xyz);

	vec3 T = normalize( normalMatrix * (tangent) ).xyz;
  	vec3 N = normalize( normalMatrix * (normal)).xyz;
 	vec3 B = normalize( normalMatrix * (bitangent)).xyz;
  	//vec3 B = normalize(cross(N, T) * tangent.w);

	tbn = mat3(T, B, N);

	fragNormal = N;
	fragTex = tex;
}

