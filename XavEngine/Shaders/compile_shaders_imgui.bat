#!/bin/bash
## -V: create SPIR-V binary
## -x: save binary output as text-based 32-bit hexadecimal numbers
## -o: output file
C:\VulkanSDK\1.2.162.1\Bin32\glslangValidator.exe  -V -x -o glsl_shader.frag.u32 glsl_shader.frag
C:\VulkanSDK\1.2.162.1\Bin32\glslangValidator.exe  -V -x -o glsl_shader.vert.u32 glsl_shader.vert
