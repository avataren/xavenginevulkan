#include "WindowManager.hpp"
#include <stdexcept>
#include <GLFW/glfw3.h>
#include <iostream>


namespace xav {
	WindowManager* WindowManager::instance = nullptr;

	WindowManager::WindowManager(int _width, int _height, const std::string& _title, bool _fullscreen) :
		width(_width), height(_height), title(_title), fullScreen(_fullscreen), minimized(false)
	{
		if (WindowManager::instance != nullptr)
			throw std::runtime_error("WindowManager is a singleton, instance already exists.");
		WindowManager::instance = this;

		glfwInit();
		// Set GLFW to not work with OpenGL

		glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
		glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
		// Create the actual window
		//
		window = glfwCreateWindow(width, height, title.c_str(), fullScreen ? glfwGetPrimaryMonitor() : nullptr, nullptr);
		if (!window)
			throw std::runtime_error("Failed to initialize window");
		lastX = width / 2;
		lastY = height / 2;

		glfwSetKeyCallback(window, key_callback);
		glfwSetWindowIconifyCallback(window, windowIconified);
		glfwSetCursorPosCallback(window, mouse_callback);
		glfwSetWindowSizeCallback(window, resize_callback);
	}

	WindowManager::~WindowManager()
	{
		instance = nullptr;
		if (window)	glfwDestroyWindow(window);
		glfwTerminate();
	}

	bool WindowManager::shouldClose()
	{
		glfwPollEvents();
		return glfwWindowShouldClose(window);
	}

	bool WindowManager::isMinimized()
	{
		return minimized;
	}

	GLFWwindow* WindowManager::getWindow()
	{
		return window;
	}

	void WindowManager::windowIconified(GLFWwindow* window, int iconified) {
		WindowManager::instance->minimized = static_cast<bool>(iconified);
	}

	void WindowManager::resize_callback(GLFWwindow * window, int _width, int _height)
	{
		//std::cout << "RESIZE CALLBACK:" << _width << "x" << _height << std::endl;
	    //auto g = static_cast<gui *>(glfwGetWindowUserPointer(window));
	    //g->window_resize(width, height);
		instance->width = _width;
		instance->height = _height;
	}

	void WindowManager::key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
	{
		if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		{
			glfwSetWindowShouldClose(window, GLFW_TRUE);
		}
	}

	void WindowManager::mouse_callback(GLFWwindow* window, double xpos, double ypos)
	{
		if (firstMouse)
		{
			lastX = xpos;
			lastY = ypos;
			firstMouse = false;
		}

		float xoffset = xpos - lastX;
		float yoffset = lastY - ypos;
		lastX = xpos;
		lastY = ypos;

		float sensitivity = 0.1f;
		xoffset *= sensitivity;
		yoffset *= sensitivity;

		yaw += xoffset;
		pitch += yoffset;

		if (pitch > 89.0f)
			pitch = 89.0f;
		if (pitch < -89.0f)
			pitch = -89.0f;
	}
}
