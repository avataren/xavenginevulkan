#pragma once
#include <string>
#include <glm/ext/vector_float2.hpp>

struct GLFWwindow;

namespace xav {

	class WindowManager
	{
	public:
		WindowManager(int _width, int _height, const std::string& _title="XavEngine", bool _fullScreen=false);
		~WindowManager();

		WindowManager(const WindowManager&) = delete;
		WindowManager& operator=(const WindowManager&) = delete;

		static WindowManager* getInstance() { return instance; }

		bool shouldClose();
		bool isMinimized();
		GLFWwindow* getWindow();
		
		int width;
		int height;
		std::string title;
		bool fullScreen;
		bool minimized;
		static inline bool firstMouse = false;
		static inline float lastX, lastY, yaw, pitch;
		
		static inline glm::vec2 movementAxis = glm::vec2(0);
	private:
		static WindowManager* instance;
		GLFWwindow* window;
		static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
		static void mouse_callback(GLFWwindow* window, double xpos, double ypos);
		static void windowIconified(GLFWwindow* window, int iconified);
		static void resize_callback(GLFWwindow * window, int _width, int _height);
	};
}
