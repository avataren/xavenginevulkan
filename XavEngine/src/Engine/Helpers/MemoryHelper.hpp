#pragma once

#include "Engine/Core/XavHeaders.hpp"

namespace xav {

class MemoryHelper
{
public:
	static uint32_t findMemoryTypeIndex(vk::PhysicalDevice physicalDevice, uint32_t allowedTypes, vk::MemoryPropertyFlags properties);
};

}

