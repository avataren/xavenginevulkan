#pragma once
#include "Engine/Core/XavHeaders.hpp"
#include "Engine/Core/Context.hpp"

namespace xav {

	class ImageHelper
	{
	public:
		static vk::ImageView createImageView(vk::Image image, vk::Format format, vk::ImageAspectFlags flags, Context& context, uint32_t mipLevels=1);
		static vk::UniqueImageView createImageViewUnique(vk::Image image, vk::Format format, vk::ImageAspectFlags aspectFlags, Context& context, uint32_t mipLevels=1);
		static void transitionImageLayout(vk::Image image, vk::ImageLayout oldLayout, vk::ImageLayout newLayout, Context& context, uint32_t mipLevels = 1, vk::CommandPool* commandPool = nullptr);
		static void copyImageBuffer(vk::Buffer srcBuffer, vk::Image image, uint32_t width, uint32_t height, Context& context, vk::CommandPool* commandPool);
		static vk::Format chooseSupportedFormat(const std::vector<vk::Format>& formats, vk::ImageTiling tiling, vk::FormatFeatureFlags featureFlags, const vk::PhysicalDevice& deviceP);
		static void generateMipmaps(vk::Image image, vk::Format imageFormat, int32_t texWidth, int32_t texHeight, uint32_t mipLevels, Context& context, vk::CommandPool* commandPool );
	};
}