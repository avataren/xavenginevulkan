#include "MemoryHelper.hpp"
namespace xav {

	uint32_t MemoryHelper::findMemoryTypeIndex(vk::PhysicalDevice physicalDevice, uint32_t allowedTypes, vk::MemoryPropertyFlags properties)
	{
		// Get properties of physical device memory
		vk::PhysicalDeviceMemoryProperties memProperties = physicalDevice.getMemoryProperties();

		for (uint32_t i = 0; i < memProperties.memoryTypeCount; i++) {
			if ((allowedTypes & (1 << i))															// Index of memory type must match corresponding bit in allowedTypes
				&& (memProperties.memoryTypes[i].propertyFlags & properties) == properties) {		// Desired properties are part of propertyFlags
				return i;
			}
		}

		throw std::runtime_error("No compatible memory found");
	}

}