
#include "MeshHelper.hpp"
#include <Engine/Scene/Scene.hpp>
#include <Engine/Scene/SceneComponents.hpp>
#include <assimp/pbrmaterial.h>
#include <regex>
#include <string>

namespace xav
{


    std::vector<PBRTextureFiles> MeshHelper::loadMaterials(const aiScene* scene)
	{
		// create 1:1 sized list of textures
		std::vector<PBRTextureFiles> textureList(scene->mNumMaterials);
		//DebugMaterials(scene);
		//go through each material and copy it's texture filename (if it exists)
		for (size_t i = 0; i < scene->mNumMaterials; i++) {
			// Get the material
			aiMaterial* material = scene->mMaterials[i];

			// Initialize the texture to empty string (will be replaced if texture exists)
			//std::get<0>(textureList[i]) = "";
			//std::get<1>(textureList[i]) = "";

			PBRTextureFiles textures;

			//material->Get(AI_MATKEY_OPACITY, AI_MATKEY_OPACITY, AI_MATKEY_OPACITY,)
			if (material->GetTextureCount(aiTextureType_DIFFUSE)) {
				aiString path;
				if (material->GetTexture(aiTextureType_DIFFUSE, 0, &path) == AI_SUCCESS) {
					
					// Cut off any directory information already present
					std::string _path(path.data);
					// make a windows-specific filepath to a UNIX-compatible one
					// (windows converts UNIX-filepaths automatically)
					std::replace( _path.begin(), _path.end(), '\\', '/');
					size_t idx = _path.rfind("/");
					std::string filename = std::string(_path).substr(idx + 1);
					textureList[i].diffuse = filename;
				}
			} else textureList[i].diffuse = "unknown diffuse :" +std::string(material->GetName().C_Str());

			if (material->GetTextureCount(aiTextureType_HEIGHT)) {
				aiString path;
				if (material->GetTexture(aiTextureType_HEIGHT, 0, &path) == AI_SUCCESS) {
					std::string _path(path.data);
					std::replace( _path.begin(), _path.end(), '\\', '/');
					size_t idx = _path.rfind("/");
					std::string filename = std::string(_path).substr(idx + 1);
					textureList[i].normal = filename;
				}
			} else textureList[i].normal = "unknown normals :" +std::string(material->GetName().C_Str());

			//textureList[i].roughness = std::regex_replace(textureList[i].diffuse, std::regex("diffuse"), "roughness");
			//textureList[i].metallic = std::regex_replace(textureList[i].diffuse, std::regex("diffuse"), "metallic");
			//std::replace(textureList[i].roughness.begin(), textureList[i].roughness.end(), std::string("Albedo"), std::string("Roughness"));
			//std::replace(textureList[i].metallic.begin(), textureList[i].metallic.end(),std::string("Albedo"), std::string("Metallic"));

			if (material->GetTextureCount(aiTextureType_SHININESS)) {
				aiString path;
				if (material->GetTexture(aiTextureType_SHININESS, 0, &path) == AI_SUCCESS) {
					std::string _path(path.data);
					std::replace( _path.begin(), _path.end(), '\\', '/');
					size_t idx = _path.rfind("/");
					std::string filename = std::string(_path).substr(idx + 1);
					textureList[i].roughness = filename;
					std::cout << "Found roughess texture :" << filename << std::endl;
				}
			} else textureList[i].roughness = "unknown roughness :" + std::string(material->GetName().C_Str());

			if (material->GetTextureCount(aiTextureType_AMBIENT)) 
			{
				aiString path;
				if (material->GetTexture(aiTextureType_AMBIENT,0, &path) == AI_SUCCESS) {
					std::string _path(path.data);
					std::replace( _path.begin(), _path.end(), '\\', '/');
					size_t idx = _path.rfind("/");
					std::string filename = std::string(_path).substr(idx + 1);
					textureList[i].metallic = filename;
					std::cout << "Found metal texture :" << filename << std::endl;
				}
			} else textureList[i].metallic = "unknown metallic :" + std::string(material->GetName().C_Str());				

			if (material->GetTextureCount(aiTextureType_OPACITY)) 
			{
				textureList[i].isOpaque = false;
			}
		}
		return textureList;
	}

	// std::vector<Mesh> MeshHelper::LoadNode(aiNode* node, const aiScene* scene, std::vector<int> matToTex, Context& context, GeometryBuffers* geometryBuffers, vk::CommandPool* commandPool)
	// {
	// 	std::vector<Mesh> meshList;
	// 	// Iterate over each mesh at this node and add it to meshList
	// 	for (size_t i = 0; i < node->mNumMeshes; i++) {
	// 		meshList.push_back(
	// 			LoadMesh(scene->mMeshes[node->mMeshes[i]], scene, matToTex, context, geometryBuffers, commandPool)
	// 		);
	// 	}

	// 	//go through each node attach to this node, and load it, then append their meshes to this node's mesh list
	// 	for (size_t i = 0; i < node->mNumChildren; i++) {
	// 		std::vector<Mesh> newList = LoadNode(node->mChildren[i], scene, matToTex, context, geometryBuffers, commandPool);
	// 		meshList.insert(meshList.end(), newList.begin(), newList.end());
	// 	}
	// 	//std::reverse(meshList.begin(), meshList.end());
	// 	return meshList;
	// }

	void MeshHelper::LoadNodeEntities(Entity entityNode, aiNode* node,const aiScene* ai_scene, Scene* scene, std::vector<int> matToTex, Context& context, GeometryBuffers* geometryBuffers, vk::CommandPool* commandPool)
	{
		// Iterate over each mesh at this node and add it to meshList
		for (size_t i = 0; i < node->mNumMeshes; i++) {
			if (ai_scene->mMeshes[node->mMeshes[i]]->mVertices->Length() > 0)
			{
				auto meshEntity = LoadMeshEntity(ai_scene->mMeshes[node->mMeshes[i]], scene, matToTex, context, geometryBuffers, commandPool);
				glm::mat4x4 transformMat = mat4_cast(node->mTransformation);
				meshEntity.addComponent(TransformComponent(transformMat));

				auto materialIdx = ai_scene->mMeshes[node->mMeshes[i]]->mMaterialIndex;
				auto material = ai_scene->mMaterials[materialIdx];
				if (material->GetTextureCount(aiTextureType_OPACITY))
				{
					meshEntity.addComponent(TransparentComponent());
				}
				//add to parent
				scene->AddChild(entityNode, meshEntity);
			}
		}

		// //go through each node attach to this node, and load it, then append their meshes to this node's mesh list
		for (size_t i = 0; i < node->mNumChildren; i++) {
			//LoadNodeEntities()
			auto child = scene->CreateEntity("Node: " + std::string(node->mChildren[i]->mName.C_Str()));
			scene->AddChild(entityNode, child);
			LoadNodeEntities(child, node->mChildren[i], ai_scene, scene, matToTex, context, geometryBuffers, commandPool);
		}
	}

	Entity MeshHelper::LoadMeshEntity(aiMesh* mesh,Scene* scene , std::vector<int> matToTex, Context& context, GeometryBuffers* geometryBuffers, vk::CommandPool* commandPool)
	{
		std::vector<Vertex> vertices;
		
		// Resize vertex list to hold all vertices for mesh
		vertices.resize(mesh->mNumVertices);
		//copy over vertices
		for (size_t i = 0; i < mesh->mNumVertices; i++) {
			//set position
			vertices[i].pos = { mesh->mVertices[i].x, mesh->mVertices[i].y, mesh->mVertices[i].z };
			//set texCoords (if they exist)
			if (mesh->mTextureCoords[0])
				vertices[i].tex = { mesh->mTextureCoords[0][i].x, mesh->mTextureCoords[0][i].y };
			else
				vertices[i].tex = { 0.0f, 0.0f };

			if (mesh->mTangents)
			{
				vertices[i].tangent = { mesh->mTangents[i].x, mesh->mTangents[i].y, mesh->mTangents[i].z };
			}
			if (mesh->mBitangents)
			{
				vertices[i].bitangent = { mesh->mNormals[i].x, mesh->mNormals[i].y, mesh->mNormals[i].z };
			}

			if (mesh->mNormals)
			{
				vertices[i].normal = { mesh->mNormals[i].x, mesh->mNormals[i].y, mesh->mNormals[i].z };
			}
			//else
				//vertices[i].tex = { 0.0f, 0.0f };				

		}

		std::vector<uint32_t> indices;
		uint32_t numIndices = 0;
		//iterate over indices over faces and copy across
		for (size_t i = 0; i < mesh->mNumFaces; i++) {
			//iterate over faces, and add to list
			numIndices += mesh->mFaces[i].mNumIndices;
		}
		indices.resize(numIndices);
		for (size_t i = 0; i < mesh->mNumFaces; i++) {
			//get a face
			aiFace face = mesh->mFaces[i];
			//iterate over faces, and add to list
			for (size_t j = 0; j < face.mNumIndices; j++) {
				indices.push_back(face.mIndices[j]);
			}
		}

		auto meshName =  std::string(mesh->mName.C_Str());
		if (meshName.empty()) meshName = "- unknown name -";
		auto ent = scene->CreateEntity("Mesh: " + meshName);
		
		std::lock_guard<std::mutex> lock(scene->sceneMutex);

		//int existingVertexCount = geometryBuffers->currentVertexOffset() / sizeof(Vertex);
		//auto vertexOffset = existingVertexCount;
		GeometryGPUComponent geometryComponent;
		
		geometryBuffers->copyToVertexBuffer(context, &vertices, &geometryComponent.vertexBufferOffset, commandPool);
		geometryBuffers->copyToIndexBuffer(context, &indices, &geometryComponent.indexBufferOffset, commandPool);
		geometryComponent.vertexCount = vertices.size();
		geometryComponent.indexCount = indices.size();
		MaterialComponent 	materialComponent;
		materialComponent.textureDescriptorIndex = matToTex[mesh->mMaterialIndex];

		ent.addComponent(geometryComponent);
		ent.addComponent(materialComponent);
		return ent;
	}

	// Mesh MeshHelper::LoadMesh(aiMesh* mesh, const aiScene* scene, std::vector<int> matToTex, Context& context, GeometryBuffers* geometryBuffers, vk::CommandPool* commandPool)
	// {
	// 	std::vector<Vertex> vertices;
		
	// 	// Resize vertex list to hold all vertices for mesh
	// 	vertices.resize(mesh->mNumVertices);
	// 	//copy over vertices
	// 	for (size_t i = 0; i < mesh->mNumVertices; i++) {
	// 		//set position
	// 		vertices[i].pos = { mesh->mVertices[i].x, mesh->mVertices[i].y, mesh->mVertices[i].z };
	// 		//set texCoords (if they exist)
	// 		if (mesh->mTextureCoords[0])
	// 			vertices[i].tex = { mesh->mTextureCoords[0][i].x, mesh->mTextureCoords[0][i].y };
	// 		else
	// 			vertices[i].tex = { 0.0f, 0.0f };

	// 		vertices[i].col = { 1.0f, 1.0f, 1.0f };
	// 	}

	// 	std::vector<uint32_t> indices;
	// 	uint32_t numIndices = 0;
	// 	//iterate over indices over faces and copy across
	// 	for (size_t i = 0; i < mesh->mNumFaces; i++) {
	// 		//iterate over faces, and add to list
	// 		numIndices += mesh->mFaces[i].mNumIndices;
	// 	}
	// 	indices.resize(numIndices);
	// 	for (size_t i = 0; i < mesh->mNumFaces; i++) {
	// 		//get a face
	// 		aiFace face = mesh->mFaces[i];
	// 		//iterate over faces, and add to list
	// 		for (size_t j = 0; j < face.mNumIndices; j++) {
	// 			indices.push_back(face.mIndices[j]);
	// 		}
	// 	}

	// 	// Create new mesh with details and return it
	// 	return Mesh(&vertices, &indices, matToTex[mesh->mMaterialIndex], context, geometryBuffers, commandPool);
	// }

}