#pragma once
#include <vector>
#include <string>
#include <fstream>

namespace xav {

	class FileHelper {
	public:
		static std::vector<char> readFile(const std::string fileName)
		{
			// Open stream from given file
			// std::ios::binary tell stream to read file as binary
			// std::ios::ate tell stream to start reading from end if file, to easily read file size
			std::ifstream file(fileName, std::ios::binary | std::ios::ate);

			if (!file.is_open()) {
				throw std::runtime_error("Failed to open file");
			}

			size_t fileSize = static_cast<size_t>(file.tellg()); // get read position in file, which will be equal to filesize

			std::vector<char> fileBuffer(fileSize);
			file.seekg(0);										// reset read position to beginning of file
			file.read(fileBuffer.data(), fileSize);				// read all the data into the buffer
			file.close();

			return fileBuffer;
		}
	};
}
