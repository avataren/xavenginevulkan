#include "DeviceHelper.hpp"

namespace xav
{
	vk::PhysicalDevice DeviceHelper::FindSuitableDevice(std::vector<vk::PhysicalDevice> deviceList)
	{
		return deviceList.front();
	}

	vk::Device DeviceHelper::CreateLogicalDevice(const vk::PhysicalDevice& physicalDevice) {
		// get the QueueFamilyProperties of the first PhysicalDevice
		std::vector<vk::QueueFamilyProperties> queueFamilyProperties = physicalDevice.getQueueFamilyProperties();
		// get the first index into queueFamiliyProperties which supports graphics
		size_t graphicsQueueFamilyIndex = std::distance(
			queueFamilyProperties.begin(),
			std::find_if(
				queueFamilyProperties.begin(), queueFamilyProperties.end(), [](vk::QueueFamilyProperties const& qfp) {
					return qfp.queueFlags & vk::QueueFlagBits::eGraphics;
				}));
		assert(graphicsQueueFamilyIndex < queueFamilyProperties.size());
		const float deviceQueuePriority{ 1.0f };
		// Hold a list of queue creation objects to use in our logical device,
		// initialising it with the graphics queue configuration.
		std::vector<vk::DeviceQueueCreateInfo> queueCreateInfos{
			vk::DeviceQueueCreateInfo{
				vk::DeviceQueueCreateFlags(),   // Flags
				static_cast<uint32_t>(graphicsQueueFamilyIndex),       // Queue family index
				2,                              // Queue count
				&deviceQueuePriority            // Queue priority
			} };

		vk::PhysicalDeviceFeatures deviceFeatures;
		deviceFeatures.samplerAnisotropy = VK_TRUE;
		deviceFeatures.sampleRateShading = VK_TRUE;

		// create a UniqueDevice
		std::vector<const char*> extensionNames{ VK_KHR_SWAPCHAIN_EXTENSION_NAME };
		vk::DeviceCreateInfo deviceCreateInfo{
		vk::DeviceCreateFlags(),                        // Flags
		static_cast<uint32_t>(queueCreateInfos.size()), // Queue create info list count
		queueCreateInfos.data(),                        // Queue create info list
		0,                                              // Enabled layer count
		nullptr,                                        // Enabled layer names
		static_cast<uint32_t>(extensionNames.size()),   // Enabled extension count
		extensionNames.data(),                          // Enabled extension names
		&deviceFeatures                                 // Physical device features
		};

		vk::Device device = physicalDevice.createDevice(deviceCreateInfo);
		return device;
	}
}
