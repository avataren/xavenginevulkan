#pragma once

#include "Engine/Core/XavHeaders.hpp"
#include "Engine/Core/Context.hpp"

namespace xav {
	class BufferHelper
	{
	public:
		static void createBuffer(
			vk::DeviceSize bufferSize, 
			vk::BufferUsageFlags bufferUsage,
			vk::MemoryPropertyFlags bufferProperties, 
			vk::Buffer* buffer, 
			vk::DeviceMemory* bufferMemory,
			Context& context
			) {

			// CREATE VERTEX BUFFER
			vk::BufferCreateInfo bufferInfo;
			bufferInfo.size = bufferSize;								// size of buffer
			bufferInfo.usage = bufferUsage;
			bufferInfo.sharingMode = vk::SharingMode::eExclusive;		// Similar to swap chain images, can share vertex buffers

			*buffer = context.device.createBuffer(bufferInfo);

			// Get buffer memory requirements
			auto memRequirements = context.device.getBufferMemoryRequirements(*buffer);
			
			// Allocate memory to buffer
			vk::MemoryAllocateInfo memAllocInfo = {};
			memAllocInfo.allocationSize = memRequirements.size;
			memAllocInfo.memoryTypeIndex = findMemoryTypeIndex(context, memRequirements.memoryTypeBits, bufferProperties);	// Index of memory type on Physical Device that has required bitflags
																																	// (otherwise would have to specify manually, flush etc)
			// Allocate to VkDeviceMemory
			//result = vkAllocateMemory(device, &memAllocInfo, nullptr, bufferMemory);
			*bufferMemory = context.device.allocateMemory(memAllocInfo);

			context.device.bindBufferMemory(*buffer, *bufferMemory, 0);
			// Allocate memory to given vertex buffer
		}

		static void createBufferUnique(
			vk::DeviceSize bufferSize,
			vk::BufferUsageFlags bufferUsage,
			vk::MemoryPropertyFlags bufferProperties,
			vk::UniqueBuffer& buffer,
			vk::UniqueDeviceMemory& bufferMemory,
			Context& context
		) {

			// CREATE VERTEX BUFFER
			vk::BufferCreateInfo bufferInfo;
			bufferInfo.size = bufferSize;								// size of buffer
			bufferInfo.usage = bufferUsage;
			bufferInfo.sharingMode = vk::SharingMode::eExclusive;		// Similar to swap chain images, can share vertex buffers

			buffer = (context.device.createBufferUnique(bufferInfo));

			// Get buffer memory requirements
			auto memRequirements = context.device.getBufferMemoryRequirements(*buffer);

			// Allocate memory to buffer
			vk::MemoryAllocateInfo memAllocInfo = {};
			memAllocInfo.allocationSize = memRequirements.size;
			memAllocInfo.memoryTypeIndex = findMemoryTypeIndex(context, memRequirements.memoryTypeBits, bufferProperties);	// Index of memory type on Physical Device that has required bitflags
																																	// (otherwise would have to specify manually, flush etc)
			// Allocate to VkDeviceMemory
			//result = vkAllocateMemory(device, &memAllocInfo, nullptr, bufferMemory);
			bufferMemory = (context.device.allocateMemoryUnique(memAllocInfo));

			context.device.bindBufferMemory(*buffer, *bufferMemory, 0);
			// Allocate memory to given vertex buffer
		}


		static uint32_t findMemoryTypeIndex(
			Context& context, 
			uint32_t allowedTypes, 
			vk::MemoryPropertyFlags properties)
		{
			// Get properties of physical device memory
			auto memProperties = context.physicalDevice.getMemoryProperties();

			for (uint32_t i = 0; i < memProperties.memoryTypeCount; i++) {
				if ((allowedTypes & (1 << i))															// Index of memory type must match corresponding bit in allowedTypes
					&& (memProperties.memoryTypes[i].propertyFlags & properties) == properties) {		// Desired properties are part of propertyFlags
					return i;
				}
			}

			throw std::runtime_error("No compatible memory found");
		}

		static void copyBuffer( vk::Buffer srcBuffer, vk::Buffer dstBuffer, VkDeviceSize bufferSize, Context& context, vk::CommandPool* commandPool = nullptr)
		{
			// Create buffer
			auto transferCommandBuffer = beginCommandBuffer(context, commandPool);
			// Region of data to copy from and to
			VkBufferCopy bufferCopyRegion = {};
			bufferCopyRegion.srcOffset = 0;
			bufferCopyRegion.dstOffset = 0;
			bufferCopyRegion.size = bufferSize;
			// Command to copy src buffer to dst buffer
			vkCmdCopyBuffer(transferCommandBuffer.get(), srcBuffer, dstBuffer, 1, &bufferCopyRegion);
			endAndSubmitCommandBuffer(context, transferCommandBuffer.get(), commandPool);
		}

		static void copyBufferWithDstOffset(vk::Buffer srcBuffer, vk::Buffer dstBuffer, VkDeviceSize bufferSize, Context& context,vk::DeviceSize dstOffset, vk::CommandPool* commandPool = nullptr)
		{
			// Create buffer
			auto transferCommandBuffer = beginCommandBuffer(context, commandPool);
			// Region of data to copy from and to
			VkBufferCopy bufferCopyRegion = {};
			bufferCopyRegion.srcOffset = 0;
			bufferCopyRegion.dstOffset = dstOffset;
			bufferCopyRegion.size = bufferSize;
			// Command to copy src buffer to dst buffer
			vkCmdCopyBuffer(transferCommandBuffer.get(), srcBuffer, dstBuffer, 1, &bufferCopyRegion);
			endAndSubmitCommandBuffer(context, transferCommandBuffer.get(), commandPool);
		}


		static vk::UniqueCommandBuffer beginCommandBuffer(Context& context, vk::CommandPool* commandPool = nullptr) {
			auto activePool = (commandPool == nullptr) ? context.getGraphicsCommandPool() : *commandPool;
			// Command buffer details
			vk::CommandBufferAllocateInfo allocInfo;
			allocInfo.level = vk::CommandBufferLevel::ePrimary;
			allocInfo.commandPool = activePool;
			allocInfo.commandBufferCount = 1;
			// Allocate command buffer from pool
			auto commandBuffers = context.device.allocateCommandBuffersUnique(allocInfo);
			auto commandBuffer = std::move(commandBuffers.front());
			// Information to begin the command buffer record
			vk::CommandBufferBeginInfo bufferBeginInfo;
			bufferBeginInfo.flags = vk::CommandBufferUsageFlagBits::eOneTimeSubmit; // We're only using the command buffer once, so set up for one time submit
			// Begin recording transfer commands
			commandBuffer.get().begin(bufferBeginInfo);
			return commandBuffer;
		}

		static void endAndSubmitCommandBuffer(Context& context, vk::CommandBuffer commandBuffer, vk::CommandPool* commandPool = nullptr)
		{
			uint32_t queueIndex = commandPool == nullptr ? 0 : 1;
			auto gfxQueue = context.getGraphicsQueue(queueIndex);
			// End commands
			commandBuffer.end();
			// Execure command buffer by submitting to queue
			vk::SubmitInfo submitInfo;
			submitInfo.commandBufferCount = 1;
			submitInfo.pCommandBuffers = &commandBuffer;
			//Submit transfer command to transfer queue and wait until it finishes
			auto result = gfxQueue.submit(1, &submitInfo, vk::Fence());
			if (result != vk::Result::eSuccess)
				throw std::runtime_error("Failed to submit command buffer");
			gfxQueue.waitIdle();
		}
	};
}

