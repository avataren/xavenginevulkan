#include "ImageHelper.hpp"
#include "MemoryHelper.hpp"
#include "BufferHelper.hpp"
#include <Engine/Core/SwapChain.hpp>
#include <cmath>

namespace xav {
	vk::ImageView ImageHelper::createImageView(vk::Image image, vk::Format format, vk::ImageAspectFlags aspectFlags, Context& context, uint32_t mipLevels)
	{
		vk::ImageSubresourceRange subresourceRangeInfo{
					aspectFlags, // Flags
					0,           // Base mip level
					mipLevels,   // Mip level count
					0,           // Base array layer
					1            // Layer count
		};

		vk::ImageViewCreateInfo imageViewInfo{
			vk::ImageViewCreateFlags(), // Flags
			image,                      // Image
			vk::ImageViewType::e2D,     // View type
			format,                     // Format
			vk::ComponentMapping(),     // Components
			subresourceRangeInfo        // Subresource range
		};

		return context.device.createImageView(imageViewInfo);
	}

	vk::UniqueImageView ImageHelper::createImageViewUnique(vk::Image image, vk::Format format, vk::ImageAspectFlags aspectFlags, Context& context, uint32_t mipLevels)
	{
		vk::ImageSubresourceRange subresourceRangeInfo{
					aspectFlags, // Flags
					0,           // Base mip level
					mipLevels,   // Mip level count
					0,           // Base array layer
					1            // Layer count
		};

		vk::ImageViewCreateInfo imageViewInfo{
			vk::ImageViewCreateFlags(), // Flags
			image,                      // Image
			vk::ImageViewType::e2D,     // View type
			format,                     // Format
			vk::ComponentMapping(),     // Components
			subresourceRangeInfo        // Subresource range
		};

		return context.device.createImageViewUnique(imageViewInfo);
	}

	vk::Format ImageHelper::chooseSupportedFormat(
		const std::vector<vk::Format>& formats, 
		vk::ImageTiling tiling, 
		vk::FormatFeatureFlags featureFlags, 
		const vk::PhysicalDevice& deviceP)
	{
		// Loop through options and find a compatible one
		for (auto format : formats) {
			// Get properties for a given format on this device
			vk::FormatProperties properties;
			properties = deviceP.getFormatProperties(format);

			if (tiling == vk::ImageTiling::eLinear && ((properties.linearTilingFeatures & featureFlags) == featureFlags)) {
				return format;
			}
			else if (tiling == vk::ImageTiling::eOptimal && ((properties.optimalTilingFeatures & featureFlags) == featureFlags)) {
				return format;
			}
		}
		throw std::runtime_error("Failed to find a matching format");
	}

	void ImageHelper::transitionImageLayout(vk::Image image, vk::ImageLayout oldLayout, vk::ImageLayout newLayout, Context& context, uint32_t mipLevels, vk::CommandPool* commandPool) {
		auto commandBuffer = BufferHelper::beginCommandBuffer(context, commandPool);

		vk::ImageMemoryBarrier imageMemoryBarrier;
		imageMemoryBarrier.oldLayout = oldLayout;									// Layout to transition from
		imageMemoryBarrier.newLayout = newLayout;									// Layout to transition to
		imageMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;			// Queue family to transition from
		imageMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;			// Queue family to transition to
		imageMemoryBarrier.image = image;
		imageMemoryBarrier.subresourceRange.aspectMask = vk::ImageAspectFlagBits::eColor; // Aspect of image being altered
		imageMemoryBarrier.subresourceRange.baseMipLevel = 0;						// First miplevel to start alterations on
		imageMemoryBarrier.subresourceRange.levelCount = mipLevels;							// Number of miplevels to alter starting from baseMipLevel
		imageMemoryBarrier.subresourceRange.baseArrayLayer = 0;						// First layer to start alterations on
		imageMemoryBarrier.subresourceRange.layerCount = 1;							// Number of layers to alter starting form baseArrayLayer

		vk::PipelineStageFlags srcStage;
		vk::PipelineStageFlags dstStage;

		// If transitioning to new image to image ready to recieve data...
		if (oldLayout == vk::ImageLayout::eUndefined &&
			newLayout == vk::ImageLayout::eTransferDstOptimal ) {
			imageMemoryBarrier.srcAccessMask = vk::AccessFlags();										// Memory access stage transition must happen after...
			imageMemoryBarrier.dstAccessMask = vk::AccessFlagBits::eTransferWrite;						// Memory access stage transition must happen before...
			srcStage = vk::PipelineStageFlagBits::eTopOfPipe;
			dstStage = vk::PipelineStageFlagBits::eTransfer;
		}
		// If transitioning from transfer destination to shader readable
		else if (oldLayout == vk::ImageLayout::eTransferDstOptimal && 
				newLayout == vk::ImageLayout::eShaderReadOnlyOptimal) {
			imageMemoryBarrier.srcAccessMask = vk::AccessFlagBits::eTransferWrite;			// Memory access stage transition must happen after...
			imageMemoryBarrier.dstAccessMask = vk::AccessFlagBits::eShaderRead;				// Memory access stage transition must happen before...
			srcStage = vk::PipelineStageFlagBits::eTransfer;
			dstStage = vk::PipelineStageFlagBits::eFragmentShader;
		}
		else {
			throw std::runtime_error("Unhandled oldLayout and newLayout combination");
		}

		commandBuffer.get().pipelineBarrier(
			srcStage,
			dstStage,
			vk::DependencyFlags(),
			0, nullptr,
			0, nullptr,
			1, &imageMemoryBarrier
		);
		BufferHelper::endAndSubmitCommandBuffer(context, commandBuffer.get(), commandPool);
	}

	void ImageHelper::copyImageBuffer(vk::Buffer srcBuffer, vk::Image image, uint32_t width, uint32_t height, Context& context, vk::CommandPool* commandPool) {
		// Create buffer
		auto transferCommandBuffer = BufferHelper::beginCommandBuffer(context, commandPool);

		vk::BufferImageCopy imageRegion;
		imageRegion.bufferOffset = 0;											// Offset into data
		imageRegion.bufferRowLength = 0;										// Row length of data to calculate data spacing 
		imageRegion.bufferImageHeight = 0;										// Image height to calculate image spacing
		imageRegion.imageSubresource.aspectMask = vk::ImageAspectFlagBits::eColor;
		imageRegion.imageSubresource.mipLevel = 0;								// Mipmap level to copy
		imageRegion.imageSubresource.baseArrayLayer = 0;						// Starting array layer (if array)
		imageRegion.imageSubresource.layerCount = 1;							// Number of layers to copy, starting at baseArrayLayer (for cubemaps etc)
		imageRegion.imageOffset = vk::Offset3D(0, 0, 0);						// Offset into image (as opposed to raw data in bufferOffset)
		imageRegion.imageExtent = vk::Extent3D(width, height, 1);				// Size of region to copy as (x,y,z) values
		// Copy buffer to given image
		transferCommandBuffer.get().copyBufferToImage(srcBuffer, image, vk::ImageLayout::eTransferDstOptimal, 1, &imageRegion);
		BufferHelper::endAndSubmitCommandBuffer(context, transferCommandBuffer.get(), commandPool);
	}

	void ImageHelper::generateMipmaps(vk::Image image, vk::Format imageFormat, int32_t texWidth, int32_t texHeight, uint32_t mipLevels, Context& context, vk::CommandPool* commandPool) {
		// Check if image format supports linear blitting
		vk::FormatProperties formatProperties = context.physicalDevice.getFormatProperties(imageFormat);
		//vkGetPhysicalDeviceFormatProperties(physicalDevice, imageFormat, &formatProperties);

		if (!(formatProperties.optimalTilingFeatures & vk::FormatFeatureFlagBits::eSampledImageFilterLinear)) {
			throw std::runtime_error("texture image format does not support linear blitting!");
		}

		auto commandBuffer = BufferHelper::beginCommandBuffer(context, commandPool);

		vk::ImageMemoryBarrier barrier;
		barrier.image = image;
		barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		barrier.subresourceRange.aspectMask = vk::ImageAspectFlagBits::eColor;
		barrier.subresourceRange.baseArrayLayer = 0;
		barrier.subresourceRange.layerCount = 1;
		barrier.subresourceRange.levelCount = 1;

		int32_t mipWidth = texWidth;
		int32_t mipHeight = texHeight;

		for (uint32_t i = 1; i < mipLevels; i++) {
			barrier.subresourceRange.baseMipLevel = i - 1;
			barrier.oldLayout = vk::ImageLayout::eTransferDstOptimal;
			barrier.newLayout = vk::ImageLayout::eTransferSrcOptimal; 
			barrier.srcAccessMask = vk::AccessFlagBits::eTransferWrite;
			barrier.dstAccessMask = vk::AccessFlagBits::eTransferRead; 

			commandBuffer.get().pipelineBarrier(
				vk::PipelineStageFlagBits::eTransfer,
				vk::PipelineStageFlagBits::eTransfer, 
				vk::DependencyFlags(),
				0, nullptr,
				0, nullptr,
				1, &barrier
			);

			vk::ImageBlit blit;
			blit.srcOffsets[0] = vk::Offset3D( 0, 0, 0 );
			blit.srcOffsets[1] = vk::Offset3D(mipWidth, mipHeight, 1 );
			blit.srcSubresource.aspectMask = vk::ImageAspectFlagBits::eColor;
			blit.srcSubresource.mipLevel = i - 1;
			blit.srcSubresource.baseArrayLayer = 0;
			blit.srcSubresource.layerCount = 1;
			blit.dstOffsets[0] = vk::Offset3D(0, 0, 0 );
			blit.dstOffsets[1] = vk::Offset3D(mipWidth > 1 ? mipWidth / 2 : 1, mipHeight > 1 ? mipHeight / 2 : 1, 1 );
			blit.dstSubresource.aspectMask = vk::ImageAspectFlagBits::eColor;
			blit.dstSubresource.mipLevel = i;
			blit.dstSubresource.baseArrayLayer = 0;
			blit.dstSubresource.layerCount = 1;

			commandBuffer.get().blitImage(
			image, vk::ImageLayout::eTransferSrcOptimal,
			image, vk::ImageLayout::eTransferDstOptimal,
				1, &blit,
				vk::Filter::eLinear
			);

			barrier.oldLayout = vk::ImageLayout::eTransferSrcOptimal;
			barrier.newLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
			barrier.srcAccessMask = vk::AccessFlagBits::eTransferRead;
			barrier.dstAccessMask = vk::AccessFlagBits::eShaderRead;

			commandBuffer.get().pipelineBarrier(
				vk::PipelineStageFlagBits::eTransfer,
				vk::PipelineStageFlagBits::eFragmentShader,
				vk::DependencyFlags(),
				0, nullptr,
				0, nullptr,
				1, &barrier
			);

			if (mipWidth > 1) mipWidth /= 2;
			if (mipHeight > 1) mipHeight /= 2;
		}

		barrier.subresourceRange.baseMipLevel = mipLevels - 1;
		barrier.oldLayout = vk::ImageLayout::eTransferDstOptimal;
		barrier.newLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
		barrier.srcAccessMask = vk::AccessFlagBits::eTransferWrite;
		barrier.dstAccessMask = vk::AccessFlagBits::eShaderRead;

		commandBuffer.get().pipelineBarrier(
			vk::PipelineStageFlagBits::eTransfer,
			vk::PipelineStageFlagBits::eFragmentShader,
			vk::DependencyFlags(),
			0, nullptr,
			0, nullptr,
			1, &barrier
		);

		BufferHelper::endAndSubmitCommandBuffer(context, commandBuffer.get(), commandPool);
	}
}