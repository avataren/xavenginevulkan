#pragma once
#include <Engine/Core/XavHeaders.hpp>
#include <Engine/Core/Context.hpp>

namespace xav {

	class ShaderHelper {
	public:
		static vk::UniqueShaderModule createShaderModuleUnique(const std::vector<char>& code,const Context& context )
		{
			// Shader module creation information
			vk::ShaderModuleCreateInfo shaderModuleCreateInfo;
			shaderModuleCreateInfo.codeSize = code.size();
			shaderModuleCreateInfo.pCode = reinterpret_cast<const uint32_t*>(code.data());
			return context.device.createShaderModuleUnique(shaderModuleCreateInfo);
		}

		static vk::ShaderModule createShaderModule(const std::vector<char>& code, const Context& context)
		{
			// Shader module creation information
			vk::ShaderModuleCreateInfo shaderModuleCreateInfo;
			shaderModuleCreateInfo.codeSize = code.size();
			shaderModuleCreateInfo.pCode = reinterpret_cast<const uint32_t*>(code.data());
			return context.device.createShaderModule(shaderModuleCreateInfo);
		}
	};
}
