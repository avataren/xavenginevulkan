#pragma once

#include <Engine/Core/XavHeaders.hpp>

namespace xav
{
	class DeviceHelper
	{
	public:
		static vk::PhysicalDevice	FindSuitableDevice	(std::vector<vk::PhysicalDevice> deviceList);
		static vk::Device			CreateLogicalDevice	(const vk::PhysicalDevice& physicalDevice);

        static vk::SampleCountFlagBits getMaxUsableSampleCount(vk::PhysicalDevice physicalDevice) {
            vk::PhysicalDeviceProperties physicalDeviceProperties =
                physicalDevice.getProperties();

            vk::SampleCountFlags counts = physicalDeviceProperties.limits.framebufferColorSampleCounts & physicalDeviceProperties.limits.framebufferDepthSampleCounts;
            if (counts & vk::SampleCountFlagBits::e64)  return vk::SampleCountFlagBits::e64;
            if (counts & vk::SampleCountFlagBits::e32)  return vk::SampleCountFlagBits::e32;
            if (counts & vk::SampleCountFlagBits::e16)  return vk::SampleCountFlagBits::e16;
            if (counts & vk::SampleCountFlagBits::e8)  return vk::SampleCountFlagBits::e8;
            if (counts & vk::SampleCountFlagBits::e4)  return vk::SampleCountFlagBits::e4;
            if (counts & vk::SampleCountFlagBits::e2)  return vk::SampleCountFlagBits::e2;
            if (counts & vk::SampleCountFlagBits::e1)  return vk::SampleCountFlagBits::e1;

            return vk::SampleCountFlagBits::e1;
        }
	};
}

