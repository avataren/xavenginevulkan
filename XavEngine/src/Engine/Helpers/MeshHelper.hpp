#pragma once
#include <string>
#include <vector>
#include <assimp/scene.h>
#include <Engine/Scene/Scene.hpp>
#include <Engine/Core/GeometryBuffers.hpp>

namespace xav {

    class MeshHelper
    {
    public:
        static  std::vector<PBRTextureFiles> loadMaterials(const aiScene* scene);
        //static std::vector<Mesh> LoadNode(aiNode* node, const aiScene* scene, std::vector<int> matToTex, Context& context, GeometryBuffers* geometryBuffers, vk::CommandPool* commandPool);
        //static Mesh LoadMesh(aiMesh* mesh, const aiScene* scene, std::vector<int> matToTex, Context& context, GeometryBuffers* geometryBuffers, vk::CommandPool* commandPool = nullptr);

        static void LoadNodeEntities(Entity entityNode, aiNode* node, const aiScene* ai_scene, Scene* scene, std::vector<int> matToTex, Context& context, GeometryBuffers* geometryBuffers, vk::CommandPool* commandPool);
        static Entity LoadMeshEntity(aiMesh* mesh, Scene* scene, std::vector<int> matToTex, Context& context, GeometryBuffers* geometryBuffers, vk::CommandPool* commandPool);

        static void DebugMaterials(const aiScene* scene)
        {
            for (size_t i = 0; i < scene->mNumMaterials; i++) {
                // Get the material
                aiMaterial* material = scene->mMaterials[i];
                std::cout << material->GetName().C_Str() << std::endl;

                for (int i = 0;i < aiTextureType_UNKNOWN;i++)
                {
                    auto count = material->GetTextureCount((aiTextureType)i);
                    std::cout << i << " count " << count << std::endl;
                    if (count)
                    {
                        aiString path;
                        material->GetTexture((aiTextureType)i,0,&path);
                        std::cout << "filename:" << path.C_Str() << std::endl;
                    }
                }
            }
        }
    };
}