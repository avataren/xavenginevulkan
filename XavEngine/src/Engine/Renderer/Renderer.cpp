#include "Renderer.hpp"
#include <Engine/Helpers/DeviceHelper.hpp>
#include <iostream>
#include "AnsiColors.hpp"
#include <Engine/Core/Globals.hpp>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <Engine/Helpers/BufferHelper.hpp>
#include <Engine/Helpers/ImageHelper.hpp>
#include "UniformData.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include "imgui/imgui_impl_glfw.h"
#include <Engine/UI/UI_XavUI.hpp>
#include <Engine/UI/UI_PerformanceMonitor.hpp>
#include <imgui/ImGuizmo/ImGuizmo.h>
#include <Engine/Components/LightComponent.hpp>

#include <cstdlib> 
#include <ctime> 
#include <cmath>

namespace xav
{
	Renderer::Renderer(const std::string& appName, GLFWwindow* _window, Config& _config) :
		window(_window),
		context(window, _config),
		swapChain(std::make_unique<SwapChain>(SwapChain(context))),
		renderPass(std::make_unique<RenderPass>(RenderPass(swapChain.get(), context))),
		graphicsCommandBuffers(std::make_unique<CommandBuffers>(CommandBuffers(swapChain.get(), context))),
		//uniformBuffers(std::make_unique<UniformBuffers>(UniformBuffers(sizeof(UniformData), swapChain.get(), context))),
		descriptorPool(std::make_unique <DescriptorPool>(swapChain.get(), context)),
		frameBuffer(std::make_unique<FrameBuffer>(renderPass.get(), swapChain.get(), context)),
		descriptorSetLayout(std::make_unique<DescriptorSetLayout>(DescriptorSetLayout(context))),
		graphicsPipeline(std::make_unique<GraphicsPipeline>(GraphicsPipeline(renderPass.get(), descriptorSetLayout.get(), swapChain.get(), context))),
		synchronization(std::make_unique<Synchronization>(swapChain->getSwapChainSize(), context)),
		bufferManager(std::make_unique<BufferManager>(context, swapChain.get())),
		descriptorSets(std::make_unique<DescriptorSets>(swapChain.get(), descriptorSetLayout.get(), descriptorPool.get(), bufferManager.get(), context)),
		geometryBuffers(context),
		textureSampler(context),
		textureManager(descriptorPool.get(), descriptorSetLayout.get(),&textureSampler, &context),
		resourceLoader(this, &geometryBuffers, &textureManager,  &context, std::bind(&Renderer::recieveMeshModel, this, std::placeholders::_1)),
		imgUIRenderer(&*swapChain, context),
		camera(45.f),
		grid(context, &geometryBuffers, &textureSampler, &scene, &textureManager, &context.getGraphicsCommandPool())
	{
		lastFrame = glfwGetTime();
		#ifdef _DEBUG
		std::cout << ansi::foreground_bright << ansi::foreground_white;
		std::cout << "XAVENGINE SUCCESSFULLY STARTED" << std::endl;
		std::cout << ansi::foreground_normal << ansi::foreground_white;
		#endif
		//modelList.reserve(1000);
		//bufferManager->createUniformBuffer("VertexPerFrame", 1);
		vertexPerFrame = bufferManager->getUniformBufferPtr("VertexPerFrame");
		lightsPerFrame = bufferManager->getUniformBufferPtr("LightsPerFrame");

		std::srand(static_cast<unsigned int>(std::time(nullptr))); 
		float range = 16.0f;
		for (int i=0;i<32;i++)
		{
			float x = ((float)std::rand()/RAND_MAX)*2.0-1.0;
			float y = ((float)std::rand()/RAND_MAX);
			float z = ((float)std::rand()/RAND_MAX)*2.0-1.0;

			float r = ((float)std::rand()/RAND_MAX)*0.9+0.1;
			float g = ((float)std::rand()/RAND_MAX)*0.9+0.1;
			float b = ((float)std::rand()/RAND_MAX)*0.9+0.1;

			glm::vec3 pos = glm::vec3(x*range *1.2f ,y * range,z * range * 0.75f) ;
			auto Light1 = scene.CreateEntity("Light1");
			Light1.addComponent<LightSourceComponent>({pos, glm::vec4(r,g,b,1), glm::vec4(16.0f,1,1,1)});
		}
/*
		auto Light2 = scene.CreateEntity("Light2");
		Light2.addComponent<LightSourceComponent>({glm::vec3(2,2,2), glm::vec4(0,1,0,1), glm::vec4(6.0f,1,1,1)});

		auto Light3 = scene.CreateEntity("Light3");
		Light3.addComponent<LightSourceComponent>({glm::vec3(-2,2,-2), glm::vec4(0,0,1,1), glm::vec4(6.0f,1,1,1)});
*/

		// Clear the screen just because!
		Draw();
	}

	Renderer::~Renderer()
	{
		context.device.waitIdle();

	}


	bool loaded = false;
	void Renderer::update() {

		if (!loaded)// && glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS)
		{
			loaded = true;
			{
				//for (int i=0;i<10;i++)
				loadModel("Models/axe_low-poly.fbx");
				//loadModel("Models/Sponza.fbx");
				loadModel("Models/SponzaPBR.obj");
			}
		}

		if (loaded && glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_RELEASE)
		{
			//loaded = false;
		}

		if (showCursor && glfwGetMouseButton(window, 1))
		{
			camera.resetMouseCenter();
			glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
			showCursor = false;
		}

		if (!showCursor && !glfwGetMouseButton(window, 1))
		{
			glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
			showCursor = true;
		}

		double currentFrame = glfwGetTime();
		deltaTime = (float)(currentFrame - lastFrame);
		lastFrame = currentFrame;

		//camera.lookAt(glm::vec3(0, 1.5f, 0), glm::vec3(0.f, 2.0f, 1.0f));
		camera.update(deltaTime, !showCursor);

		float screenWidth = (float)swapChain->getExtent().width;
		float screenHeight = (float)swapChain->getExtent().height;
		float aspect = screenWidth / screenHeight;

		uniformData.projection = camera.getProjectionMatrix();
		uniformData.view = camera.getViewMatrix();
		uniformData.screenExtent = glm::vec2(screenWidth, screenHeight);
		uniformData.aspect = aspect;
		uniformData.time = (float)glfwGetTime();

	}

	void Renderer::loadModel(const std::string& modelFile)
	{
		resourceLoader.loadModel(modelFile);
	}

	void Renderer::Draw()
	{
		std::lock_guard<std::mutex> lock(recieveMeshMutex);		
		std::lock_guard<std::mutex> texlock(textureManager.textureMutex);
		update();

		ImGui_ImplVulkan_NewFrame();
		ImGui_ImplGlfw_NewFrame();
		ImGui::NewFrame();
      	ImGuizmo::SetOrthographic(false);//!isPerspective);
      	ImGuizmo::BeginFrame();
		ui::UI_XavUI settings(*this,context);
		ImGui::Render();
		//ImGui::UpdatePlatformWindows();				
		// 1. Get the next available image to draw to, and set something to signal when we're finished with the image (a semaphore)
		// 2. Submit command buffer to queue for execution, making sure it waits for the image to be signalled as available before drawing
		//	  and signals when it has finished rendering
		// 3. Present image to screen when it has signalled finished rendering

		// -- Get next image --
		// Wait for given fence to signal (open) from last draw before continuing
		try
		{
			auto timeout = std::numeric_limits<uint64_t>::max();

			auto result = context.device.waitForFences(*synchronization->drawFences[currentFrame], VK_TRUE, timeout);
			if (result != vk::Result::eSuccess)
			{
				throw std::runtime_error("Failed waiting for fences");
			}
			// Manually reset (close) fences
			context.device.resetFences(*synchronization->drawFences[currentFrame]);
			// Get index of next image to be drawn to, and signal semaphore when ready to be drawn to
			auto imageIndex = context.device.acquireNextImageKHR(swapChain->getSwapChain(), timeout, *synchronization->imageAvailable[currentFrame]).value;
			
			// do the actual rendering
			{
				// Update uniforms
				updateUniformBuffers(imageIndex);
				// Update all command buffers that require realtime updates
				recordCommands(imageIndex);
				imgUIRenderer.recordCommands(&*swapChain, imageIndex);

				// -- Submit command buffer to render --
				// Queue submission information
				std::array<vk::CommandBuffer, 2> commandbuffers = {
					graphicsCommandBuffers->getGraphicsCommandBuffer(imageIndex),	//"default" render commands
					(vk::CommandBuffer)imgUIRenderer.commandBuffers[imageIndex]		//ui render commands
				};

				vk::SubmitInfo submitInfo = {};
				submitInfo.waitSemaphoreCount = 1;												// Number of semaphores to wait on
				submitInfo.pWaitSemaphores = &*synchronization->imageAvailable[currentFrame];	// List of semaphores to wait on
				vk::PipelineStageFlags waitStages[] = {
					vk::PipelineStageFlagBits::eColorAttachmentOutput
				};
				submitInfo.pWaitDstStageMask = waitStages;											// Stages to wait for semaphores at
				submitInfo.commandBufferCount = static_cast<uint32_t>(commandbuffers.size());		// Number of command buffers to submit
				submitInfo.pCommandBuffers = commandbuffers.data();
				submitInfo.signalSemaphoreCount = 1;												// Number of semaphores to signal when command buffer is finished
				submitInfo.pSignalSemaphores = &*synchronization->renderFinished[currentFrame];		// Semaphore to signal when command buffer finishes
				auto gfxQueue = context.getGraphicsQueue();
				result = gfxQueue.submit(1, &submitInfo, *synchronization->drawFences[currentFrame]);
				if (result != vk::Result::eSuccess)
				{
					throw std::runtime_error("Failed waiting for fences");
				}

				// -- Present rendered image to screen --
				vk::PresentInfoKHR presentInfo = {};
				presentInfo.waitSemaphoreCount = 1;							// Number of semaphores to wait on
				presentInfo.pWaitSemaphores = &*synchronization->renderFinished[currentFrame];// Semaphore to wait on
				presentInfo.swapchainCount = 1;								// Number of swapchains to present to
				presentInfo.pSwapchains = swapChain->getSwapChainPtr();		// Swapchains to present images to
				presentInfo.pImageIndices = &imageIndex;					// Index of images in swapchains to present
				// Present image
				result = gfxQueue.presentKHR(presentInfo);
				
				if (result != vk::Result::eSuccess)
				{
#ifdef _DEBUG
					std::cerr << "Failed presenting" << std::endl;
#endif
					recreate();

				}
				// Get next frame
				else
					currentFrame = (currentFrame + 1) % swapChain->getSwapChainSize();
			}
	
		}
		catch (vk::OutOfDateKHRError ex)
		{
#ifdef _DEBUG
			std::cout << ex.what() << std::endl;
			std::cout << "Trying to recreate swapchain etc.." << std::endl;
#endif
			recreate();
		}

		
	}	//MAX_FRAME_DRAWS;

	void Renderer::recreate() {
		context.device.waitIdle();

		SwapChain* old = swapChain.release();
		delete old;
		context.reCreateSurface();

		swapChain = std::make_unique<SwapChain>(SwapChain(context));
		renderPass = std::make_unique<RenderPass>(RenderPass(swapChain.get(), context));
		frameBuffer = std::make_unique<FrameBuffer>(FrameBuffer(renderPass.get(), swapChain.get(), context));
		graphicsPipeline = std::make_unique<GraphicsPipeline>(GraphicsPipeline(renderPass.get(), descriptorSetLayout.get(), swapChain.get(), context));

		imgUIRenderer.recreate(swapChain.get());
		currentFrame = 0;
		camera.updateProjection();
	}

	void Renderer::recordCommands(uint32_t currentImage)
	{
		// Information about how to begin each command buffer
		vk::CommandBufferBeginInfo bufferBeginInfo;

		// Clear values for color and depth buffer
		std::array<vk::ClearValue, 2> clearValues = {};
		clearValues[0].color = vk::ClearColorValue(std::array<float, 4>{0.3f, 0.45f, 0.7f, 1.0f});
		clearValues[1].depthStencil.depth = 1.0f;

		// Information about how to begin a render pass (only needed for graphical application)
		vk::RenderPassBeginInfo renderPassBeginInfo;
		renderPassBeginInfo.renderPass = renderPass->getRenderPass();   // Render pass to begin
		renderPassBeginInfo.renderArea.offset.x = 0;
		renderPassBeginInfo.renderArea.offset.y = 0;
		renderPassBeginInfo.renderArea.extent = swapChain->getExtent(); // Size of region to run render pass on (starting at offset)
		renderPassBeginInfo.pClearValues = clearValues.data();			// List of clear values (TODO: Depth attachment clear value)
		renderPassBeginInfo.clearValueCount = static_cast<uint32_t>(clearValues.size());
		renderPassBeginInfo.framebuffer = frameBuffer->getFrameBuffer(currentImage);

		// Start recording commands to command buffer
		auto cmd = graphicsCommandBuffers->getGraphicsCommandBuffer(currentImage);
		cmd.begin(bufferBeginInfo);
		// Begin render pass
		cmd.beginRenderPass(renderPassBeginInfo,vk::SubpassContents::eInline);
		// Bind pipeline to be used in render pass
		cmd.bindPipeline(vk::PipelineBindPoint::eGraphics, graphicsPipeline->getPipeline());
		vk::Buffer vertexBuffers[] = { geometryBuffers.getVertexBuffer() };					// Buffers to bind
		vk::DeviceSize offsets[] = { 0 };												// Offsets into buffers being bound
		cmd.bindVertexBuffers(0, 1, vertexBuffers, offsets);
		//// Bind mesh index buffer, with 0 offeset and using uint32 type
		cmd.bindIndexBuffer(geometryBuffers.getIndexBuffer(), 0, vk::IndexType::eUint32);
		scene.Draw(cmd, graphicsPipeline->getPipelineLayout(),descriptorSets->getDescriptorSet(currentImage), &textureManager, camera, lightCount);
		cmd.endRenderPass(); // End render pass
		cmd.end(); // End recording commands to command buffer
	}

	void Renderer::updateUniformBuffers(uint32_t imageIndex)
	{
		updateLights();
		// Copy VP data
		//auto vertexPerFramePtr = context.device.mapMemory(vertexPerFrame->getMemory(imageIndex), 0, sizeof(UniformData));
		auto vertexPerFramePtr = vertexPerFrame->getMappedMemory(imageIndex);
		memcpy(vertexPerFramePtr, &uniformData, sizeof(UniformData));
		auto lightsPerFramePtr = static_cast<LightSourceComponent*>(lightsPerFrame->getMappedMemory(imageIndex));
		//memcpy(lightsPerFramePtr, lights, sizeof(UniformData));
		//context.device.unmapMemory(vertexPerFrame->getMemory(imageIndex));

        auto lightsView = scene.GetRegistry().view<const LightSourceComponent>(entt::exclude<TransparentComponent, HiddenComponent>);
        lightCount = 0;
        lightsView.each([&](auto& lightsource) {
			lightsPerFramePtr[lightCount++] = lightsource;
        });

	}
	
	void Renderer::updateLights() 
	{
		auto time = glfwGetTime()*.25f;
		std::srand(static_cast<unsigned int>(666)); 
        auto lightsView = scene.GetRegistry().view<LightSourceComponent>(entt::exclude<TransparentComponent, HiddenComponent>);
		float range = 20.0f;
        lightsView.each([&](auto& lightsource) {
		
			float x = sin(time+(float)std::rand());
			float y = cos(time*1.5+(float)std::rand()) + 1.0f;
			float z = cos(time*1.75+(float)std::rand());
			
			glm::vec3 pos = glm::vec3(x*range *1.2f ,y * range,z * range * 0.75f) ;
			lightsource.position = pos;
		});	
	}

	void Renderer::recieveMeshModel(std::shared_ptr<AssetLoaderUpdate> assetUpdate)
	{
		std::lock_guard<std::mutex> lock(recieveMeshMutex);
		try
		{
			
			if (assetUpdate->percentDone >= 1.0f)
			{
				assetProgressList.erase(assetUpdate->assetName);
			}
			else
			{
				assetProgressList[assetUpdate->assetName] = assetUpdate->percentDone;
			}

		}
		catch (...)
		{
			std::cerr << "Exception caught in recieveMeshModel, probably due to shutdown while loading" << std::endl;
		}
		
	}


}
