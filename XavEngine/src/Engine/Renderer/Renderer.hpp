#pragma once
#include <Engine/Core/Context.hpp>
#include <Engine/Core/SwapChain.hpp>
#include <Engine/Core/RenderPass.hpp>
#include <Engine/Core/DescriptorSetLayout.hpp>
#include <Engine/Core/GraphicsPipeline.hpp>
#include <Engine/Core/FrameBuffer.hpp>
#include <Engine/Core/CommandBuffers.hpp>
#include <Engine/Core/TextureSampler.hpp>
#include <Engine/Core/UniformBuffers.hpp>
#include <Engine/Core/DescriptorPool.hpp>
#include <Engine/Core/DescriptorSets.hpp>
#include <Engine/Core/Synchronization.hpp>
#include <Engine/Core/UboViewProjection.hpp>
#include <Engine/Core/ImgUIRenderer.hpp>
#include "stb_image.h"
#include <Engine/ResourceHandling/TextureManager.hpp>
#include "UniformData.hpp"
#include <Engine/ResourceHandling/ResourceLoader.hpp>
#include "Camera.hpp"
#include "Engine/ResourceHandling/AssetLoaderUpdate.hpp"
#include <unordered_map>
#include <Engine/Core/GeometryBuffers.hpp>
#include <Engine/Scene/Scene.hpp>
#include <Engine/UI/UI_Grid.hpp>
#include <Engine/Core/BufferManager.hpp>

namespace xav
{
	class Renderer
	{
	public:
		Renderer(const std::string& appName, GLFWwindow* window, Config& _config );
		~Renderer();

		Renderer(const Renderer&) = delete;
		Renderer& operator=(const Renderer&) = delete;

		void Draw();
		void update();
		void loadModel(const std::string& modelFile);
		void recreate();
		void recieveMeshModel(std::shared_ptr<AssetLoaderUpdate> model);
		const std::unordered_map<std::string, float>& getAssetProgressList() {
			return assetProgressList;
		}

		Scene& getScene() {return scene;}
		Camera camera;
	private:
		std::mutex recieveMeshMutex;
		UniformData uniformData;
		uint32_t currentFrame = 0;
		GLFWwindow* window;
		Context context;
		std::unique_ptr<SwapChain>				swapChain;
		std::unique_ptr<RenderPass>				renderPass;
		std::unique_ptr<CommandBuffers>			graphicsCommandBuffers;
		//std::unique_ptr<UniformBuffers>			uniformBuffers;
		std::unique_ptr<DescriptorPool>			descriptorPool;
		std::unique_ptr<FrameBuffer>			frameBuffer;
		std::unique_ptr<DescriptorSetLayout>	descriptorSetLayout;
		std::unique_ptr<GraphicsPipeline>		graphicsPipeline;
		std::unique_ptr<Synchronization>		synchronization;
		std::unique_ptr<BufferManager> 			bufferManager;
		std::unique_ptr<DescriptorSets>			descriptorSets;
		GeometryBuffers	geometryBuffers;
		TextureSampler	textureSampler;
		TextureManager	textureManager;
		ResourceLoader	resourceLoader;
		ImgUIRenderer	imgUIRenderer;
		
		UniformBuffers* vertexPerFrame;
		UniformBuffers* lightsPerFrame;
		Scene scene;
		xav::ui::UI_Grid grid;
		void recordCommands(uint32_t currentImage);
		void updateUniformBuffers(uint32_t imageIndex);
		double deltaTime;
		double lastFrame;
		bool showCursor = true;

		std::unordered_map<std::string, float> assetProgressList;
 		uint32_t lightCount;

		 void updateLights();
	};
}