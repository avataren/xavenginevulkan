#pragma once
#include "Engine/Core/XavHeaders.hpp"
#include "GLFW/glfw3.h"
#include <glm/ext/matrix_clip_space.hpp>
#include <glm/ext/matrix_transform.hpp>

#include "OS_Integration/WindowManager.hpp"

namespace xav
{
	class Camera
	{
	public:
		Camera(float = 45.f);
		void updateProjection();
		void lookAt(const glm::vec3& lookAtPos);
		void lookAt(const glm::vec3& eye, const glm::vec3& lookAtPos);
		void update(float deltaTime, bool mouseMove);
		void resetMouseCenter();
		void processInput(float deltaTime);
		glm::mat4& getViewMatrix();
		glm::mat4& getProjectionMatrix();

	void setProjectionMatrix(const glm::mat4& mat);
	void setViewMatrix(const glm::mat4& mat);

	private:
		void updateInput(double xpos, double ypos);

		float aspect;
		float fov;
		glm::mat4 projection;
		glm::mat4 view;
		glm::vec3 position = glm::vec3(-3.f,1.f, 0.f);
		glm::vec3 up = glm::vec3(0.f, 1.f, 0.f);
		glm::vec3 front = glm::vec3(0.0f, 0.0f, -1.0f);
		glm::vec3 direction = glm::vec3(1,0,0);
		float zFar = 10000.f;
		float zNear = 0.1f;
		float yaw = 0.f;
		float pitch = 0.f;
		bool firstMouse = false;
		double lastX = 0.f;
		double lastY = 0.f;
	};
}

