#pragma once
#include <glm/ext/matrix_float4x4.hpp>
namespace xav
{
	struct UniformData
	{
		glm::mat4 projection;
		glm::mat4 view;
		glm::vec2 screenExtent;
		float time;
		float aspect;
	};
}


