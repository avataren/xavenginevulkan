#include "Camera.hpp"
#include <glm/gtx/matrix_decompose.hpp>

namespace xav
{
	Camera::Camera(float _fov) {
		fov = _fov;
//		view = glm::mat4(1.f);
		front = glm::normalize(direction);
		view = glm::lookAt(position, position + front, up);		
		updateProjection();
	}

	void Camera::updateProjection() {
		auto window = WindowManager::getInstance()->getWindow();
		int width, height;
		glfwGetFramebufferSize(window, &width, &height);
		aspect = width / (float)height;

		projection = glm::perspective(glm::radians(fov), aspect, zNear, zFar);
		projection[1][1] *= -1;
	}

	void Camera::lookAt(const glm::vec3& lookAtPos) {

		view = glm::lookAt(glm::vec3(position), lookAtPos, up);
	}

	void Camera::lookAt(const glm::vec3& eye, const glm::vec3& lookAtPos) {

		position = eye;
		view = glm::lookAt(glm::vec3(position), lookAtPos, up);
	}

	void Camera::resetMouseCenter()
	{
		firstMouse = true;
	}

	void Camera::update(float deltaTime, bool mouseMove) {
		
		//return;
		if (mouseMove)
		{
			processInput(deltaTime);
			direction.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
			direction.y = sin(glm::radians(pitch));
			direction.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
		}
		front = glm::normalize(direction);

		view = glm::lookAt(position, position + front, up);
	}

	void Camera::processInput(float deltaTime)
	{
		auto window = WindowManager::getInstance()->getWindow();
		double cursorX, cursorY;
		glfwGetCursorPos(window, &cursorX, &cursorY);
		updateInput(cursorX, cursorY);

		glm::vec3 right = glm::normalize(glm::cross(up, direction));
		float cameraSpeed = 5.f * deltaTime;
		auto moveDirection = glm::vec3(0);

		if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
			moveDirection += front;
		if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
			moveDirection -= front;
		if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
			moveDirection += right;
		if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
			moveDirection -= right;


		if (glm::length(moveDirection) > 0.1f)
		{
			if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
				cameraSpeed *= 2.0f;

			moveDirection = glm::normalize(moveDirection);
			position += moveDirection * cameraSpeed;
		}
	}

	glm::mat4& Camera::getProjectionMatrix()
	{
		return projection;
	}

	glm::mat4& Camera::getViewMatrix()
	{
		return view;
	}

	void Camera::setProjectionMatrix(const glm::mat4& mat)
	{
		projection = mat;
	}

	void Camera::setViewMatrix(const glm::mat4& mat)
	{
		view = mat;
		const auto inverted = glm::inverse(mat);
		const auto forward = normalize(glm::vec3(inverted[2]));
		direction = -forward;
	}

	void Camera::updateInput(double xpos, double ypos)
	{
		if (firstMouse)
		{
			lastX = xpos;
			lastY = ypos;
			firstMouse = false;
		}

		float xoffset = xpos - lastX;
		float yoffset = lastY - ypos;
		lastX = xpos;
		lastY = ypos;

		float sensitivity = 0.1f;
		xoffset *= sensitivity;
		yoffset *= sensitivity;

		yaw += xoffset;
		pitch += yoffset;

		if (pitch > 89.999f)
			pitch = 89.999f;
		if (pitch < -89.999f)
			pitch = -89.999f;
	}
}