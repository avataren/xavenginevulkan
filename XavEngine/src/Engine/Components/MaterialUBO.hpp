#pragma once
#include <glm/glm.hpp>

namespace xav
{


    struct MaterialUBO
    {
        glm::bvec4 DNRM_HasTextures;
        glm::ivec4 DNRM_Indices;
        glm::vec4 diffuseColor;
        bool recieveShadows;
        bool castShadows;
        float roughness;
        float metalness;

    };


}