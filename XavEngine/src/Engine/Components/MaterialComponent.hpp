#pragma once

#include <string>
#include <unordered_map>

namespace xav
{
    enum TransparencyMode {
        XAV_OPAQUE,
        XAV_TRANSPARENT
    };

    struct MaterialComponent
    {
        int32_t textureDescriptorIndex;
        int32_t normalTextureDescriptorIndex;
        //
        std::string vertexShader;
        std::string fragmentShader;
        std::string computeShader;
        std::string geometryShader;

        uint32_t pipelineHandle;
        TransparencyMode transparencyMode;
    };
/*
    struct MaterialInfoComponent {
        std::string baseEffect;
        std::unordered_map<std::string, std::string> textures; //name -> path
        std::unordered_map<std::string, std::string> customProperties;
        TransparencyMode transparency;
        uint32_t materialUBOIndex;

    };

*/
/*
for each view {
  bind global resourcees          // set 0

  for each shader {
    bind shader pipeline  
    for each material {
      bind material resources  // sets 1,2
    }
  }
}
*/
}