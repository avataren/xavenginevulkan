#pragma once
#include <glm/glm.hpp>

namespace xav
{
    struct LightSourceComponent
    {
        __declspec(align(16)) glm::vec3 position;
        __declspec(align(16)) glm::vec4 color;
        __declspec(align(16)) glm::vec4 description; //range, cast_shadows, type, angle
    };
}