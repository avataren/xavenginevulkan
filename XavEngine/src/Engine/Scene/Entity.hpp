#pragma once

#include <entt/entt.hpp>

namespace xav {

  class Entity {
  public:
    Entity() { entityId = entt::null; }

    Entity(const Entity& other) {
      entityId = other.entityId;
      registry = other.registry;
    }

    Entity(const entt::null_t&) { entityId = entt::null; }

    Entity(const std::string& _name, std::shared_ptr<entt::registry> _registry);

    Entity(const entt::entity& _entity,
      std::shared_ptr<entt::registry> _registry) {
      entityId = _entity;
      registry = _registry;
    }

    template <typename T> bool hasComponent() {
      return registry->has<T>(entityId);
    }

    template <typename T> T& getComponent() { return registry->get<T>(entityId); }

    template <typename T> void addComponent(const T& component) {
      registry->emplace<T>(entityId, component);
    }
    
    template <typename T> void removeComponent() {
      registry->remove<T>(entityId);
    }

    void destroy() { registry->destroy(entityId); }

    Entity& operator=(const Entity& other) noexcept {
      entityId = other.entityId;
      registry = other.registry;
      return *this;
    }

    bool operator==(const Entity& other) noexcept {
      return (entityId == other.entityId && registry == other.registry);
    }

    bool operator!=(const Entity& other) noexcept {
      return (entityId != other.entityId || registry != other.registry);
    }

    bool operator==(const entt::null_t& other) noexcept {
      return (entityId == other);
    }

    bool operator!=(const entt::null_t& other) noexcept {
      return (entityId != other);
    }

    bool isNull() { return entityId == entt::null; }

    static inline constexpr auto& null() { return entt::null; }

  private:
    entt::registry& GetRegistry() { return *registry.get(); }

    std::shared_ptr<entt::registry> registry;
    entt::entity entityId;
  };

} // namespace xav