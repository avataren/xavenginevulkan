#include "Scene.hpp"
#include <iostream>
#include <Engine/Core/XavHeaders.hpp>
#include "SceneComponents.hpp"
#include <Engine/Geometry/ModelData.hpp>


namespace xav
{

    Scene::Scene() : registry(std::make_shared<entt::registry>(entt::basic_registry<entt::entity>()))
    {
        rootNodeEntity = Entity("root", registry);
    }

    entt::entity Scene::CreateEntity()
    {
        return GetRegistry().create();
    }

    Entity Scene::CreateEntity(const std::string& name)
    {
        auto ent = CreateEntity();
        GetRegistry().emplace<NameComponent>(ent, name);
        return Entity(ent, registry);
    }

    Entity& Scene::Root()
    {

        return rootNodeEntity;
    }

    void Scene::Draw(const vk::CommandBuffer& cmd, const vk::PipelineLayout& layout, const vk::DescriptorSet descriptorSet, TextureManager* textureManager, Camera& camera, uint32_t lightCount)
    {

        std::lock_guard<std::mutex>  lock(sceneMutex);


#pragma region per frame bindings
        {
            // Bind descriptor sets
            std::array<vk::DescriptorSet, 2> descriptorSetGroup = {
                descriptorSet,  textureManager->getDescriptorSet()
            };

            cmd.bindDescriptorSets(vk::PipelineBindPoint::eGraphics,
                layout,
                0,
                static_cast<uint32_t>(descriptorSetGroup.size()),
                descriptorSetGroup.data(), 0, nullptr
            );
        }
#pragma endregion


            auto view = GetRegistry().view<const MaterialComponent, const TransformComponent, const GeometryGPUComponent>(entt::exclude<TransparentComponent, HiddenComponent>);
            uint32_t lastTextureDescriptorIndex = -1;
            view.each([&](auto& material, auto& transform, auto& geometry) {

                auto transformMatrix = transform.getTransform();
                ModelData modeldata;
                modeldata.modelView = /*camera.getViewMatrix() **/ transformMatrix;
                //modeldata.normalMatrix 
                auto normalMatrix = glm::transpose(glm::inverse(transformMatrix));
                modeldata.v1 = normalMatrix[0];
                modeldata.v2 = normalMatrix[1];
                modeldata.v3 = normalMatrix[2];

                cmd.pushConstants(
                    layout,
                    vk::ShaderStageFlagBits::eVertex,
                    0,
                    112,
                    &modeldata
                );

                //int32_t texIdx = material.textureDescriptorIndex;
                glm::vec4 info = glm::vec4((float)material.textureDescriptorIndex, lightCount, 0, 0);
                cmd.pushConstants(
                    layout,
                    vk::ShaderStageFlagBits::eFragment,
                    112,
                    sizeof(glm::vec4),
                    &info
                );

                // Execute a pipeline
                uint32_t firstIndex = geometry.indexBufferOffset / sizeof(uint32_t);
                cmd.drawIndexed(geometry.indexCount, 1, firstIndex, geometry.vertexBufferOffset, 0);
                });

            DrawTransparent(cmd, layout, descriptorSet, textureManager, camera, lightCount);
    }

    void Scene::DrawTransparent(const vk::CommandBuffer& cmd, const vk::PipelineLayout& layout, const vk::DescriptorSet descriptorSet, TextureManager* textureManager, Camera& camera, uint32_t lightCount) {

        auto view = GetRegistry().view<const MaterialComponent, const TransformComponent, const GeometryGPUComponent, const TransparentComponent>(entt::exclude<HiddenComponent>);
        uint32_t lastTextureDescriptorIndex = -1;
        view.each([&](auto& material, auto& transform, auto& geometry, auto& transparancy) {

            auto transformMatrix = transform.getTransform();
            ModelData modeldata;
            modeldata.modelView = /*camera.getViewMatrix() **/ transformMatrix;
            //modeldata.normalMatrix 
            auto normalMatrix = glm::transpose(glm::inverse(transformMatrix));
            modeldata.v1 = normalMatrix[0];
            modeldata.v2 = normalMatrix[1];
            modeldata.v3 = normalMatrix[2];

            cmd.pushConstants(
                layout,
                vk::ShaderStageFlagBits::eVertex,
                0,
                112,
                &modeldata
            );

                glm::vec4 info = glm::vec4((float)material.textureDescriptorIndex, lightCount, 0, 0);
                cmd.pushConstants(
                    layout,
                    vk::ShaderStageFlagBits::eFragment,
                    112,
                    sizeof(glm::vec4),
                    &info
                );

            if (lastTextureDescriptorIndex != material.textureDescriptorIndex)
            {
                std::array<vk::DescriptorSet, 1> descriptorSetGroup = {
                    descriptorSet
                };
                lastTextureDescriptorIndex = material.textureDescriptorIndex;

                // Bind descriptor sets
                cmd.bindDescriptorSets(vk::PipelineBindPoint::eGraphics,
                    layout,
                    0,
                    static_cast<uint32_t>(descriptorSetGroup.size()),
                    descriptorSetGroup.data(), 0, nullptr
                );
            }
            // Execute a pipeline
            uint32_t firstIndex = geometry.indexBufferOffset / sizeof(uint32_t);
            cmd.drawIndexed(geometry.indexCount, 1, firstIndex, geometry.vertexBufferOffset, 0);

            });
    }


    void Scene::AddChild(Entity& parent, Entity& child)
    {
        std::lock_guard<std::mutex> lock(sceneMutex);
        Entity currentChild;
        if (parent.hasComponent<HierarchyParentComponent>())
        {
            currentChild = parent.getComponent<HierarchyParentComponent>().firstChild;
        }

        if (currentChild != Entity())
        {
            auto childHierarchy = currentChild.getComponent<HierarchyComponent>();
            while (childHierarchy.next != Entity())
            {
                currentChild = childHierarchy.next;
                childHierarchy = currentChild.getComponent<HierarchyComponent>();
            }
        }
        else
        {
            auto comp = HierarchyParentComponent(child);
            parent.addComponent<HierarchyParentComponent>(comp);
        }
        HierarchyComponent childHierarchy(parent);
        childHierarchy.prev = currentChild;
        if (currentChild != Entity::null())
        {
            auto& prevHierarchy = currentChild.getComponent<HierarchyComponent>();
            prevHierarchy.next = child;
            //set next pointer from previous child to current
        }
        child.addComponent(childHierarchy);
    }

    void Scene::AddToScene(Entity& sceneEntity)
    {
        //std::lock_guard<std::mutex> lock(sceneMutex);
        AddChild(Root(), sceneEntity);
    }

    void Scene::DestroyEntity(Entity& entityToDestroy)
    {
        std::lock_guard<std::mutex> lock(sceneMutex);
        //if a parent exist, update next/prev pointers of siblings
        auto childHierarchy = entityToDestroy.getComponent<HierarchyComponent>();
        if (childHierarchy.parent != Entity::null())
        {
            if (childHierarchy.prev != Entity::null())
            {
                auto prevHierarchy = childHierarchy.prev.getComponent<HierarchyComponent>();
                prevHierarchy.next = childHierarchy.next;
            }
            if (childHierarchy.next != Entity::null())
            {
                auto nextHierarchy = childHierarchy.next.getComponent<HierarchyComponent>();
                nextHierarchy.prev = childHierarchy.prev;
            }
        }
        entityToDestroy.destroy();
    }

} // namespace xav