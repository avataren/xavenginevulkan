#pragma once

#include <Engine/Core/XavHeaders.hpp>
#include <Engine/Scene/Entity.hpp>
#include <glm/gtx/matrix_decompose.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <Engine/Components/MaterialComponent.hpp>

namespace xav
{

    struct TransformComponent
    {
        TransformComponent(const glm::mat4& inTransform) {
            setTransform(inTransform);
        }

        void setTransform(const glm::mat4 inTransform) {
            glm::vec3 skew;
            glm::vec4 perspective;
            glm::decompose(inTransform, scale, rotation, translation, skew, perspective);
            //rotation=glm::conjugate(rotation);
            euler = glm::eulerAngles(rotation) * 180.f / glm::pi<float>();
        }        

        glm::mat4 getTransform() const {
            glm::mat4 translate = glm::translate(glm::mat4(1.0), translation);
            glm::mat4 rotate    = glm::mat4_cast(rotation);
            glm::mat4 scaling   = glm::scale(glm::mat4(1.0), scale);
            return translate * rotate * scaling;
        }

        //glm::mat4 transform;
        glm::vec3 translation;
        glm::quat rotation;
        glm::vec3 scale;
        glm::vec3 euler;

        void setEulerPitchYawRollDegrees(const glm::vec3 angle) {
            euler = angle;
            auto rad = angle * glm::pi<float>() / 180.f;
            rotation = glm::quat(glm::vec3(rad.x, rad.y, rad.z));
        }

        TransformComponent& operator=(const glm::mat4& trans) noexcept
        {
            *this = TransformComponent(trans);
            return *this;
        }
    };

    struct HiddenComponent
    {
        uint8_t dummy;
    };

    struct ComputedTransformComponent
    {
        glm::mat4 transform;
    };

    struct TransparentComponent
    {
        uint32_t ordering;
    };    

    struct HierarchyParentComponent
    {
        HierarchyParentComponent(Entity _child) : firstChild(_child) {}
        Entity firstChild;
    };

    struct HierarchyComponent
    {
        HierarchyComponent(Entity _parent) : parent(_parent) {};
        Entity parent;
        Entity next;
        Entity prev;
    };

    struct NameComponent
    {
        NameComponent(const std::string& _name) : name(_name) {}
        std::string name;

        NameComponent& operator=(const std::string& other) noexcept
        {
            name = other;
            return *this;
        }
    };

    struct GeometryGPUComponent
    {
        vk::DeviceSize vertexBufferOffset;
        vk::DeviceSize vertexCount;
        vk::DeviceSize indexBufferOffset;
        vk::DeviceSize indexCount;
    };

    struct AABoundingBoxComponent
    {
        glm::vec3 min;
        glm::vec3 max;
    };

    struct MeshComponent
    {
        // hide mesh complexety (like scene graph of submeshes) behind a mesh component?
        // this will prevent hierarchical transforms, but might be worth it for static meshes
        // it will also possibly prevent mouse-picking submeshes, is this something I want?
        std::vector<GeometryGPUComponent> geometry;
        std::vector<MaterialComponent> materials;
    };

} // namespace xav