#include "Entity.hpp"
#include "SceneComponents.hpp"

namespace xav
{

    Entity::Entity(const std::string& _name, std::shared_ptr<entt::registry> _registry) : registry(_registry)
    {
        entityId = registry->create();
        NameComponent comp = _name;
        addComponent<NameComponent>(comp);
    }

} // namespace xav