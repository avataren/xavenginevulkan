#pragma once
#include "Entity.hpp"
#include <Engine/Core/XavHeaders.hpp>
#include <Engine/ResourceHandling/TextureManager.hpp>
#include <Engine/Renderer/Camera.hpp>
#include <entt/entt.hpp>
#include <mutex>

namespace xav {
  class Scene {
  public:
    Scene();

    Entity CreateEntity(const std::string& name);
    Entity& Root();

    void AddChild(Entity& parent, Entity& child);
    void AddToScene(Entity& sceneEntity);
    void DestroyEntity(Entity& entityToDestroy);
    void Draw(const vk::CommandBuffer& cmd, const vk::PipelineLayout& layout,
      const vk::DescriptorSet descriptorSet,
      TextureManager* textureManager,
      Camera& camera,
      uint32_t lightCount);
    entt::registry& GetRegistry() { return *registry.get(); }
    std::mutex sceneMutex;

  private:
    void DrawTransparent(const vk::CommandBuffer& cmd, const vk::PipelineLayout& layout, const vk::DescriptorSet descriptorSet, TextureManager* textureManager, Camera& camera, uint32_t lightCount);
    Entity rootNodeEntity;
    std::shared_ptr<entt::registry> registry;
    entt::entity CreateEntity();
  };

} // namespace xav