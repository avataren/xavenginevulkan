#pragma once
#include <glm/glm.hpp>

namespace xav {

	struct ModelData {
		__declspec(align(16)) glm::mat4 modelView;
		__declspec(align(16)) glm::vec4 v1;
		__declspec(align(16)) glm::vec4 v2;
		__declspec(align(16)) glm::vec4 v3;
		//__declspec(align(16)) glm::mat3 normalMatrix;
		//fragment data
	};
}
