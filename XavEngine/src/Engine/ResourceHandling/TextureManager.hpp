#pragma once
#include <Engine/Core/FullImage.hpp>
#include <Engine/Helpers/ImageHelper.hpp>
#include <Engine/Core/DescriptorPool.hpp>
#include <Engine/Core/DescriptorSetLayout.hpp>
#include <Engine/Core/Context.hpp>
#include <Engine/Core/TextureSampler.hpp>
#include <Engine/Core/XavHeaders.hpp>
#include <mutex>
#include <map>

#include <stb_image.h>

namespace xav
{
	struct PBRTextureFiles{
		std::string diffuse;
		std::string normal;
		std::string roughness;
		std::string metallic;
		bool isOpaque = true;
	};

	class TextureManager
	{
	public:
		TextureManager(DescriptorPool* _descriptorPool, DescriptorSetLayout* _descriptorSetLayout, TextureSampler* _sampler, Context* _context);
		vk::DescriptorSet getDescriptorSet();
		uint32_t loadTexture(std::string filename, vk::CommandPool* commandPool = nullptr);
		uint32_t loadTextures(const PBRTextureFiles& textureFilenames, vk::CommandPool* commandPool = nullptr);

		std::mutex textureMutex;
	private:
		void initializeDescriptorSetWithDefaultTexture();
		DescriptorPool* descriptorPool;
		DescriptorSetLayout* descriptorSetLayout;
		TextureSampler* sampler;
		Context* context;

		vk::UniqueDescriptorSet descriptorSet;
		uint32_t currentDescriptorIndex = 0;

		uint32_t createTextureDescriptor(vk::ImageView textureView);
		uint32_t findFreeTextureDescriptor(std::vector<vk::ImageView>& textureViews);
		stbi_uc* loadTextureFile(std::string fileName, int* width, int* height, VkDeviceSize* imageSize, int formatChannels = STBI_rgb_alpha, int channelSize = 1);
		const vk::ImageView& createTextureImage(std::string fileName, vk::Format format, vk::CommandPool* commandPool);

		std::map< std::string, std::unique_ptr<FullImage> > textureHash;
		std::map< size_t, int > textureDescriptonMap;
		std::vector<vk::UniqueDescriptorSet> textureDescriptorSets;
		//std::vector<std::unique_ptr<FullImage>> images;
	};

}