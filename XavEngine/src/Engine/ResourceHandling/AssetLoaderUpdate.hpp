#pragma once

#include <memory>

namespace xav
{
	class AssetLoaderUpdate {
	public:
		AssetLoaderUpdate(float _percentDone, std::string _assetName) :
			percentDone(_percentDone), assetName(_assetName)
		{}

		float percentDone;
		std::string assetName;
	};
}
