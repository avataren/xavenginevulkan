#include "TextureManager.hpp"
#include <Engine/Helpers/BufferHelper.hpp>
#include <Engine/Core/Globals.hpp>
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#include <array>
#include <Engine/Core/Globals.hpp>
#include <unordered_map>

namespace xav {

	TextureManager::TextureManager(DescriptorPool* _descriptorPool, DescriptorSetLayout* _descriptorSetLayout, TextureSampler* _sampler, Context* _context) :
		descriptorPool(_descriptorPool), descriptorSetLayout(_descriptorSetLayout), sampler(_sampler), context(_context)
	{
		initializeDescriptorSetWithDefaultTexture();

	}

	// vk::DescriptorSet TextureManager::getDescriptorSet(uint32_t texId)
	// {
	// 	return textureDescriptorSets[texId].get();
	// }

	vk::DescriptorSet TextureManager::getDescriptorSet()
	{
		return descriptorSet.get();
	}

	uint32_t TextureManager::loadTexture(std::string filename, vk::CommandPool* commandPool)
	{
		std::hash<std::string> hasher;
		size_t hash = hasher(filename);

		if (textureDescriptonMap.find(hash) == textureDescriptonMap.end()) {
			//std::cout << "Creating new descriptor for " << filename << std::endl;
			auto view = createTextureImage(filename, vk::Format::eR8G8B8A8Srgb, commandPool);
			std::vector<vk::ImageView> textureViews;
			textureViews.push_back(view);textureViews.push_back(view);textureViews.push_back(view);textureViews.push_back(view);
			auto descIndex = findFreeTextureDescriptor(textureViews);
			//auto descIndex = createTextureDescriptor(view);
			textureDescriptonMap[hash] = descIndex;
		}
		//std::cout << "Reusing descriptor for " << filename << std::endl;
		return textureDescriptonMap[hash];
	}

	//uint32_t TextureManager::loadTextures(std::string colorMap, std::string normalMap, vk::CommandPool* commandPool)
	uint32_t TextureManager::loadTextures(const PBRTextureFiles& textureFilenames, vk::CommandPool* commandPool)
	{
		auto tohash = textureFilenames.diffuse + textureFilenames.normal + textureFilenames.roughness + textureFilenames.metallic;
		std::hash<std::string> hasher;
		size_t hash = hasher(tohash);

		if (textureDescriptonMap.find(hash) == textureDescriptonMap.end()) {
			std::vector<vk::ImageView> textureViews;
			//std::cout << "Creating new descriptor for " << filename << std::endl;
					//auto format = vk::Format::eR8G8B8A8Srgb;
		//auto format = vk::Format::eR8G8B8A8Unorm;
			auto diffuseView = createTextureImage(textureFilenames.diffuse, vk::Format::eR8G8B8A8Srgb, commandPool);
			auto normalView = createTextureImage(textureFilenames.normal, vk::Format::eR8G8B8A8Unorm, commandPool);
			auto roughnessView = createTextureImage(textureFilenames.roughness, vk::Format::eR8Unorm, commandPool);
			auto metallicView = createTextureImage(textureFilenames.metallic, vk::Format::eR8Unorm, commandPool);

			textureViews.push_back(diffuseView);
			textureViews.push_back(normalView);
			textureViews.push_back(roughnessView);
			textureViews.push_back(metallicView);

			auto descIndex = findFreeTextureDescriptor(textureViews);
			textureDescriptonMap[hash] = descIndex;
			//std::cout << "Reusing descriptor for " << filename << std::endl;
			return textureDescriptonMap[hash];
		}
		return textureDescriptonMap[hash];
	}

	void TextureManager::initializeDescriptorSetWithDefaultTexture()
	{
		auto defaultTextureview = createTextureImage("white.png", vk::Format::eR8G8B8A8Srgb, &context->getGraphicsCommandPool());

		// Descriptor set allocation
		vk::DescriptorSetAllocateInfo setAllocInfo;
		setAllocInfo.descriptorPool = descriptorPool->getSamplerDescriptorPool();
		setAllocInfo.descriptorSetCount = 1;
		setAllocInfo.pSetLayouts = &descriptorSetLayout->getSamplerDescriptorSetLayout();
		// Allocate descriptor sets
		descriptorSet = std::move(context->device.allocateDescriptorSetsUnique(setAllocInfo).front());

		std::vector<vk::DescriptorImageInfo> imageInfos;

		for (uint32_t i = 0;i < MAX_TEXTURES;i++)
		{
			// Texture image info
			vk::DescriptorImageInfo imageInfo;
			imageInfo.imageLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
			imageInfo.imageView = defaultTextureview;
			imageInfo.sampler = sampler->getSampler();
			imageInfos.push_back(imageInfo);
		}

		// Sampler to use for set
		//descriptor write info
		vk::WriteDescriptorSet descriptorWrite;
		descriptorWrite.dstSet = descriptorSet.get();
		descriptorWrite.dstBinding = 0;
		descriptorWrite.dstArrayElement = 0;
		descriptorWrite.descriptorType = vk::DescriptorType::eCombinedImageSampler;
		descriptorWrite.descriptorCount = static_cast<uint32_t>(imageInfos.size());
		descriptorWrite.pImageInfo = imageInfos.data();
		context->device.updateDescriptorSets(1, &descriptorWrite, 0, nullptr);
	}

	uint32_t TextureManager::findFreeTextureDescriptor(std::vector<vk::ImageView>& textureViews)
	{
		std::lock_guard<std::mutex>  texlock(textureMutex);
		context->device.waitIdle(); // without this I'm getting a ton of validation errors due to queue being active

		auto thisIdx = currentDescriptorIndex;
		std::vector<vk::DescriptorImageInfo> imageInfos;

		for (auto view : textureViews)
		{
			// Texture image info
			vk::DescriptorImageInfo imageInfo;
			imageInfo.imageLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
			imageInfo.imageView = view;
			imageInfo.sampler = sampler->getSampler();
			imageInfos.push_back(imageInfo);
		}

		//descriptor write info
		vk::WriteDescriptorSet descriptorWrite;
		descriptorWrite.dstSet = descriptorSet.get();
		descriptorWrite.dstBinding = 0;
		descriptorWrite.dstArrayElement = currentDescriptorIndex;
		descriptorWrite.descriptorType = vk::DescriptorType::eCombinedImageSampler;
		descriptorWrite.descriptorCount = static_cast<uint32_t>(imageInfos.size());
		descriptorWrite.pImageInfo = imageInfos.data();

		context->device.updateDescriptorSets(1, &descriptorWrite, 0, nullptr);
		currentDescriptorIndex += textureViews.size();
		return thisIdx;
	}
	/*
		uint32_t TextureManager::findFreeTextureDescriptor(std::vector<vk::ImageView>& textureViews)
		{
			//TODO:
			//claim n descriptors from array, starting at last free index
			//return index to first descriptor, then it's up to the app/shader to index the other n-1 descriptors successively


			// Descriptor set allocation
			vk::DescriptorSetAllocateInfo setAllocInfo;
			setAllocInfo.descriptorPool = descriptorPool->getSamplerDescriptorPool();
			setAllocInfo.descriptorSetCount = 1;
			setAllocInfo.pSetLayouts = &descriptorSetLayout->getSamplerDescriptorSetLayout();
			// Allocate descriptor sets
			auto descriptorSet = context->device.allocateDescriptorSetsUnique(setAllocInfo);

			std::vector<vk::DescriptorImageInfo> imageInfos;

			for (auto& texView : textureViews)
			{
				// Texture image info
				vk::DescriptorImageInfo imageInfo;
				imageInfo.imageLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
				imageInfo.imageView = texView;
				imageInfo.sampler = sampler->getSampler();
				imageInfos.push_back(imageInfo);
			}

			// Sampler to use for set
			//descriptor write info
			vk::WriteDescriptorSet descriptorWrite;
			descriptorWrite.dstSet = descriptorSet.front().get();
			descriptorWrite.dstBinding = 0;
			descriptorWrite.dstArrayElement = 0;
			descriptorWrite.descriptorType = vk::DescriptorType::eCombinedImageSampler;
			descriptorWrite.descriptorCount = static_cast<uint32_t>(imageInfos.size());
			descriptorWrite.pImageInfo = imageInfos.data();

			//std::array<vk::WriteDescriptorSet, 2> descSets = { descriptorWrite,descriptorWrite };
			std::lock_guard<std::mutex>  texlock(textureMutex);
			// Update the new descriptor set
			context->device.updateDescriptorSets(1, &descriptorWrite, 0, nullptr);
			// Add descriptor set to list
			textureDescriptorSets.push_back(std::move(descriptorSet.front()));
			// Return descriptor set location
			return static_cast<uint32_t>(textureDescriptorSets.size() - 1);
		}

	*/
	uint32_t TextureManager::createTextureDescriptor(vk::ImageView textureView)
	{

		std::vector<vk::ImageView> textureViews;
		textureViews.push_back(textureView);
		textureViews.push_back(textureView);
		return findFreeTextureDescriptor(textureViews);
	}

	const vk::ImageView& TextureManager::createTextureImage(std::string fileName, vk::Format format, vk::CommandPool* commandPool)
	{
		auto existing = textureHash.find(fileName);
		if (existing != textureHash.end())
		{
			return textureHash[fileName]->getView();
		}

		int channels = 4;
		int channelSize = 1;
		if (format == vk::Format::eR16Unorm)
		{
			channels = 1;
			channelSize = 2;
		}
		if (format == vk::Format::eR8Unorm)
			channels = 1;
		// Load in image file
		int width, height;
		VkDeviceSize imageSize;
		auto imageData = loadTextureFile(fileName, &width, &height, &imageSize, channels, channelSize);

		if (!imageData)
			return textureHash["white.png"]->getView();

		// Create staging buffer to hold loaded data, ready to copy to device
		vk::UniqueBuffer imageStagingBuffer;
		vk::UniqueDeviceMemory imageStagingBufferMemory;
		BufferHelper::createBufferUnique(imageSize, vk::BufferUsageFlagBits::eTransferSrc,
			vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent,
			imageStagingBuffer, imageStagingBufferMemory, *context);

		// copy image data to staging buffer
		auto data = context->device.mapMemory(imageStagingBufferMemory.get(), 0, imageSize);
		memcpy(data, imageData, static_cast<size_t>(imageSize));
		context->device.unmapMemory(imageStagingBufferMemory.get());

		// Free original image data
		stbi_image_free(imageData);

		uint32_t mipLevels = 1;
		if (context->getConfig().autoGenerateMipMaps)
			mipLevels = static_cast<uint32_t>(std::floor(std::log2((double)std::max(width, height)))) + 1;


		// Create image to hold final texture on GPU
		textureHash[fileName] = std::make_unique<FullImage>(
			FullImage(vk::Extent2D(width, height),
				format,
				vk::ImageTiling::eOptimal,
				vk::ImageUsageFlagBits::eTransferSrc |
				vk::ImageUsageFlagBits::eTransferDst | vk::ImageUsageFlagBits::eSampled,
				vk::MemoryPropertyFlagBits::eDeviceLocal,
				*context,
				vk::ImageAspectFlagBits::eColor,
				mipLevels)
			);

		// Transision image to be DST for copy operation
		auto image = textureHash[fileName]->getImage();
		//textureImages[imageIndex].getImage();

		ImageHelper::transitionImageLayout(image,
			vk::ImageLayout::eUndefined,
			vk::ImageLayout::eTransferDstOptimal,
			*context,
			mipLevels,
			commandPool);

		// Copy staging buffer to device image
		ImageHelper::copyImageBuffer(imageStagingBuffer.get(), image, width, height, *context, commandPool);

		if (context->getConfig().autoGenerateMipMaps)
		{
			ImageHelper::generateMipmaps(image, format, width, height, mipLevels, *context, commandPool);
		}
		else
		{
			// Transition image to be shader readable for shader usage
			ImageHelper::transitionImageLayout(image,
				vk::ImageLayout::eTransferDstOptimal,
				vk::ImageLayout::eShaderReadOnlyOptimal,
				*context, 1, commandPool);
		}
		// Add texture data to list for reference
		//auto descIndex = createTextureDescriptor(textureHash[fileName]->getView());
		//textureDescriptonMap[fileName] = descIndex;
		//return descIndex;
		return textureHash[fileName]->getView();
	}

	stbi_uc* TextureManager::loadTextureFile(std::string fileName, int* width, int* height, VkDeviceSize* imageSize, int formatchannels, int channelSize)
	{
		// Number of channels image uses
		int channels;
		// Load pixel data for image
		std::string fileLoc = TEXTURE_FOLDER + fileName;
		stbi_uc* image = stbi_load(fileLoc.c_str(), width, height, &channels, formatchannels);

		if (!image)
		{
			std::cerr << "Failed loading texture file (" + fileName + ")" << std::endl;
			//throw std::runtime_error("Failed loading texture file (" + fileName + ")");
			return nullptr;
		}
		// Calculate image size
		*imageSize = *width * *height * formatchannels * channelSize;
		return image;
	}
}
