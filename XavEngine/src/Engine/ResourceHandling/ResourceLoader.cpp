#include "ResourceLoader.hpp"
#include <Engine/Core/Context.hpp>
#include <Engine/Renderer/Renderer.hpp>
#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <assimp/ProgressHandler.hpp>
#include <Engine/Helpers/MeshHelper.hpp>

namespace xav {

	class ProgressWatcher : public Assimp::ProgressHandler
	{
	public:
		ProgressWatcher(std::function< void(std::shared_ptr<AssetLoaderUpdate>)> _recieveMeshCallbacFunc,
			std::string _filename):
			recieveMeshCallbacFunc(_recieveMeshCallbacFunc),
			filename(_filename)
		{

		}
		virtual bool Update(float percentage = -1.f)
		{
			recieveMeshCallbacFunc(std::make_shared<AssetLoaderUpdate>(percentage*0.1f,  filename));
			return true;
		}
	private: 
		std::function< void(std::shared_ptr<AssetLoaderUpdate>)> recieveMeshCallbacFunc;
		std::string filename;
	};

	ResourceLoader::ResourceLoader(Renderer* _renderer,GeometryBuffers* _geometryBuffers, TextureManager* _textureManager, Context* _context,std::function< void(std::shared_ptr<AssetLoaderUpdate>)> func) :
		renderer(_renderer),
		textureManager(_textureManager),
		geometryBuffers(_geometryBuffers),
		context(_context),
		recieveMeshCallbackFunc(func)
	{
		StartControlThread();
	}

	ResourceLoader::~ResourceLoader()
	{
		runProcessor = false;
		process_asset_cv.notify_one();

		if (controlThread.joinable())
			controlThread.join();

		if (loaderThread.joinable())
			loaderThread.join();
	}

	void ResourceLoader::StartControlThread()
	{
		if (controlThread.joinable())
			throw std::runtime_error("Control thread is already running");

		controlThread = std::thread(&ResourceLoader::RunResourceLoaderControl, this);
	}

	void ResourceLoader::RunResourceLoaderControl()
	{
		vk::CommandPoolCreateInfo poolInfo;
		poolInfo.flags = vk::CommandPoolCreateFlagBits::eResetCommandBuffer;// vk::CommandPoolCreateFlagBits::eTransient;
		poolInfo.queueFamilyIndex = context->getGraphicsQueueIndex();
		// Create a graphics queue family command pool
		commandPool = context->device.createCommandPoolUnique(poolInfo);

		while (runProcessor)
		{
			std::unique_lock<std::mutex> lk(process_mutex);
			process_asset_cv.wait(lk);
			while (assetQueue.size() > 0)
			{
				std::string asset;
				{
					asset = assetQueue.front();
					std::lock_guard<std::mutex> lk(add_to_queue_mutex);
					assetQueue.pop_front();
				}
				if (loaderThread.joinable())
				{
					loaderThread.join();
				}
				loaderThread = std::thread(&ResourceLoader::RunLoadModel, this, asset);
				process_asset_cv.wait(lk);
				if (loaderThread.joinable())
				{
					loaderThread.join();
				}
			}
			lk.unlock();
		}
	}

	void ResourceLoader::loadModel(const std::string& filename)
	{
		{
			std::lock_guard<std::mutex> lk(add_to_queue_mutex);
			assetQueue.push_back(filename);
		}
		process_asset_cv.notify_one();
	}

	void ResourceLoader::RunLoadModel(const std::string& filename) {
		//std::lock_guard<std::mutex> lk(process_mutex);
		recieveMeshCallbackFunc(std::make_shared<AssetLoaderUpdate>(0,  filename));

		//std::shared_ptr<MeshModel> meshModel = 
			//std::make_shared<MeshModel>( createMeshModel(filename, &commandPool.get()) );
		createMeshModel(filename, &commandPool.get());
		std::shared_ptr<AssetLoaderUpdate> assetUpdate =
			std::make_shared<AssetLoaderUpdate>(1, filename);
		recieveMeshCallbackFunc(assetUpdate);
		process_asset_cv.notify_one();
	}

	void ResourceLoader::createMeshModel(const std::string& modelFile, vk::CommandPool* commandPool)
	{
		recieveMeshCallbackFunc(std::make_shared<AssetLoaderUpdate>(0.0f, modelFile));
		// Import model "scene"
		Assimp::Importer importer;
		auto callback = std::make_unique<ProgressWatcher>(ProgressWatcher(recieveMeshCallbackFunc, modelFile));
		importer.SetProgressHandler(callback.get());
		importer.SetPropertyFloat(AI_CONFIG_GLOBAL_SCALE_FACTOR_KEY, .01f);
		//importer.SetProgressHandler(new ProgressWatcher(callback, fileName));
		const aiScene* aiscene = importer.ReadFile(modelFile,
			aiProcess_Triangulate |
			aiProcess_FlipUVs |
			aiProcess_JoinIdenticalVertices |
			aiProcess_ImproveCacheLocality |
			aiProcess_SortByPType |
			aiProcess_OptimizeMeshes |
			aiProcess_OptimizeGraph |
			aiProcess_CalcTangentSpace |
			aiProcess_GlobalScale  
			//aiProcess_PreTransformVertices
			) ;
		if (!aiscene)
		{
			throw std::runtime_error("Failed to load model: " + modelFile);
		}
		
		recieveMeshCallbackFunc(std::make_shared<AssetLoaderUpdate>(0.1f, modelFile));
		// get vector of all materials, with 1:1 ID placement
		auto textureNames = MeshHelper::loadMaterials(aiscene);

		// Conversion from the materials list IDs to our Descriptor array IDs
		std::vector<int> matToTex(textureNames.size());

		// Loop over textureNames and create textures for them
		for (size_t i = 0; i < textureNames.size(); i++)
		{
			// If material has no texture, set '0' to indicate no texture. Texture 0 will be reserved for a default texture
			//if (std::get<0>(textureNames[i]).empty()) {
			//	matToTex[i] = 0;
			//}
			//else
			{
				// Otherwise, create texture and set value to index of new texture
				// and add model prefix to texture name
				std::string::size_type const p(modelFile.find_last_of('.'));
				std::string mesh_file_no_extension = modelFile.substr(0, p);
				std::string texture_prefix = mesh_file_no_extension.substr(mesh_file_no_extension.find_last_of("/\\") + 1);

				auto& textures = textureNames[i];
				textures.diffuse = texture_prefix + "/" + textures.diffuse;
				textures.normal = texture_prefix + "/" + textures.normal;
				textures.roughness = texture_prefix + "/" + textures.roughness;
				textures.metallic = texture_prefix + "/" + textures.metallic;

				matToTex[i] = textureManager->loadTextures(textures, commandPool);
			}
			float pct = 0.1f + (float)(i / ((float)textureNames.size())) * 0.9f;
			recieveMeshCallbackFunc(std::make_shared<AssetLoaderUpdate>(pct, modelFile));
		}
		recieveMeshCallbackFunc(std::make_shared<AssetLoaderUpdate>(0.9999, modelFile));
		// Load in all our meshes
		//std::vector<Mesh> modelMeshes = MeshHelper::LoadNode(scene->mRootNode, scene, matToTex, *context, geometryBuffers, commandPool);
		Entity rootEntity = renderer->getScene().CreateEntity(modelFile);
		MeshHelper::LoadNodeEntities(rootEntity,aiscene->mRootNode, aiscene, &renderer->getScene(), matToTex, *context, geometryBuffers, commandPool);
		renderer->getScene().AddToScene(rootEntity);
		
		importer.SetProgressHandler(nullptr);
	}
}