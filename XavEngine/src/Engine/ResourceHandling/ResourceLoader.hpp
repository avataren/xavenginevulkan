#pragma once

#include <Engine/Core/GeometryBuffers.hpp>
#include <queue>
#include <string>
#include <mutex>
#include <functional>
#include <thread>
#include <condition_variable>
#include "AssetLoaderUpdate.hpp"

namespace xav
{
	class Renderer;
	class TextureManager;
	class TextureSampler;

	class ResourceLoader
	{
	public:
		ResourceLoader(Renderer* renderer_, GeometryBuffers* _geometryBuffers, TextureManager* _textureManager, Context* _context, std::function< void(std::shared_ptr<AssetLoaderUpdate>)> func);
		~ResourceLoader();
		void loadModel(const std::string& filename);
	private:
		bool runProcessor = true;
		std::deque<std::string> assetQueue;
		vk::UniqueCommandPool commandPool;
		std::mutex process_mutex;
		std::mutex add_to_queue_mutex;
		std::condition_variable process_asset_cv;
		std::thread controlThread;
		std::thread loaderThread;
		void RunLoadModel(const std::string& filename);
		void StartControlThread();
		void RunResourceLoaderControl();
		Renderer* renderer;
		TextureManager* textureManager;
		GeometryBuffers* geometryBuffers;
		Context* context;
		std::function< void(std::shared_ptr<AssetLoaderUpdate>)> recieveMeshCallbackFunc;
		void createMeshModel(const std::string& modelFile, vk::CommandPool* commandPool);
	};
}
