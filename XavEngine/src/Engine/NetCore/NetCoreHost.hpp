#pragma once



#define MANAGED_ASSEMBLY "ManagedLibrary.dll"
#include <string>
// Define OS-specific items like the CoreCLR library's name and path elements

#include "coreclrhost.h"

namespace xav
{
    class NetCoreHost{
        public:
        NetCoreHost();
        ~NetCoreHost();
        private:
        void BuildTpaList(const char* directory, const char* extension, std::string& tpaList);

        coreclr_shutdown_ptr shutdownCoreClr;
        coreclr_create_delegate_ptr createManagedDelegate;
        coreclr_initialize_ptr initializeCoreClr;

        void* hostHandle = nullptr;
        unsigned int domainId = -1;



    };
}