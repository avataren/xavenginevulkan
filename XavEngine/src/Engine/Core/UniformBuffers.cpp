#include "UniformBuffers.hpp"
#include "UboViewProjection.hpp"

#include <stdio.h>

namespace xav {

	UniformBuffers::UniformBuffers(size_t _bufferSize, SwapChain* _swapChain, Context& _context,
	vk::BufferUsageFlags bufferUsageBits,
	vk::MemoryPropertyFlags memoryPropertyBits, 
	bool memoryMapped) 
	: context(&_context), swapChain(_swapChain), bufferSize(_bufferSize)
	{
		auto swapChainSize = swapChain->getSwapChainSize();

		uniformBuffers.resize(swapChainSize);
		uniformBufferMemories.resize(swapChainSize);

		//create uniform buffers
		for (size_t i = 0; i < swapChainSize; i++) {

			BufferHelper::createBufferUnique(
				bufferSize,
				bufferUsageBits,
				memoryPropertyBits,
				uniformBuffers[i],
				uniformBufferMemories[i],
				_context);
		}
		//memset(mappedMemory.data(), 0, sizeof(mappedMemory.size() * sizeof(void*)));
		if (memoryMapped)
			mapMemory();
	}

	const void UniformBuffers::mapMemory()
	{
		auto swapChainSize = swapChain->getSwapChainSize();
		for (size_t i = 0; i < swapChainSize; i++) {
			mappedMemory.push_back(context->device.mapMemory(getMemory(i), 0, bufferSize));
		}
	}

	const void UniformBuffers::unmapMemory()
	{
		auto swapChainSize = swapChain->getSwapChainSize();
		for (size_t i = 0; i < mappedMemory.size(); i++) {
			context->device.unmapMemory(getMemory(i));
		}
	}
}