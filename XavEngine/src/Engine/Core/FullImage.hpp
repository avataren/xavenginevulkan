#pragma once	
#include "Context.hpp"
#include "XavHeaders.hpp"
#include "Engine/Helpers/ImageHelper.hpp"

namespace xav 
{
	class FullImage {
	public:

		FullImage(const vk::Extent2D& extent,
			vk::Format format,
			vk::ImageTiling tiling,
			vk::ImageUsageFlags usageFlags,
			vk::MemoryPropertyFlags propFlags,
			Context& context,
			vk::ImageAspectFlags aspectFlags= vk::ImageAspectFlagBits::eColor,
			uint32_t mipLevels = 1,
			vk::SampleCountFlagBits sampleCount = vk::SampleCountFlagBits::e1,
			vk::ImageLayout imageLayout = vk::ImageLayout::eUndefined);

		const vk::Image& getImage() {
			return image.get();
		}

		const vk::ImageView& getView() {
			return view.get();
		}

		const vk::DeviceMemory& getMemory() {
			return memory.get();
		}
		vk::Format format;

		static FullImage depthBufferImage(const vk::Extent2D& extent, Context& context)
		{
			// Get supported format for depth buffer
			auto depthFormat = ImageHelper::chooseSupportedFormat(
				{
					vk::Format::eD32SfloatS8Uint,
					vk::Format::eD32Sfloat,
					vk::Format::eD24UnormS8Uint,
					vk::Format::eD16UnormS8Uint,
				},
				vk::ImageTiling::eOptimal,
				vk::FormatFeatureFlagBits::eDepthStencilAttachment,
				context.physicalDevice
				);
			// Create depth buffer image
			return FullImage(
				extent,
				depthFormat,
				vk::ImageTiling::eOptimal,
				vk::ImageUsageFlagBits::eDepthStencilAttachment,
				vk::MemoryPropertyFlagBits::eDeviceLocal,
				context,
				vk::ImageAspectFlagBits::eDepth, 1, context.getConfig().msaaSamples);
		}

	private:
		vk::UniqueImage			image;
		vk::UniqueImageView		view;
		vk::UniqueDeviceMemory	memory;
	};

	class SwapChainImage {
	public:
		SwapChainImage(vk::Image _image, vk::Format format, Context& context) : image(_image)
		{
			view = ImageHelper::createImageViewUnique(image, format, vk::ImageAspectFlagBits::eColor, context);
		}
		vk::Image			image;
		vk::UniqueImageView	view;

		const vk::ImageView& getView() {
			return view.get();
		}
	};

}