#include "DescriptorPool.hpp"
#include "Globals.hpp"


namespace xav {
	DescriptorPool::DescriptorPool(SwapChain* swapChain, Context& context)
	{
		// Type of descriptors + how many DESCRIPTORS, not descriptor sets (combined makes the pool size)
		// CREATE UNIFORM DESCRIPTOR POOL
		auto binding_size = swapChain->getSwapChainSize();
		vk::DescriptorPoolSize ub_poolSize = vk::DescriptorPoolSize(vk::DescriptorType::eUniformBuffer, binding_size * static_cast<uint32_t>(2));
		
		// Model pool (DYNAMIC)
		//VkDescriptorPoolSize modelPoolSize = {};
		//modelPoolSize.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
		//modelPoolSize.descriptorCount = static_cast<uint32_t>(modelDUniformBuffer.size());
		std::vector<vk::DescriptorPoolSize> descriptorPoolSizes{ ub_poolSize };

		//data to create descriptor pool
		vk::DescriptorPoolCreateInfo poolCreateInfo;
		poolCreateInfo.maxSets = swapChain->getSwapChainSize();									// Maximum number of descriptor sets that can be created from pool
		poolCreateInfo.poolSizeCount = 1;// static_cast<uint32_t>(descriptorPoolSizes.size());	// Amount of poole sizes being passed
		poolCreateInfo.pPoolSizes = &ub_poolSize;// descriptorPoolSizes.data();					// Pool sizes to create pool with
		poolCreateInfo.flags = vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet;
		// Create descriptor pool
		descriptorPool = context.device.createDescriptorPoolUnique(poolCreateInfo);

		// CREATE SAMPLER DESCRIPTOR POOL
		vk::DescriptorPoolSize samplerPoolSize;
		samplerPoolSize.descriptorCount = MAX_OBJECTS;

		vk::DescriptorPoolCreateInfo samplerPoolCreateInfo;
		samplerPoolCreateInfo.maxSets = MAX_OBJECTS;
		samplerPoolCreateInfo.poolSizeCount = 1;
		samplerPoolCreateInfo.pPoolSizes = &samplerPoolSize;
		samplerPoolCreateInfo.flags = vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet;
		samplerDescriptorPool = context.device.createDescriptorPoolUnique(samplerPoolCreateInfo);
	}
}