#pragma once
#define GLFW_INCLUDE_VULKAN

#include <GLFW/glfw3.h>
#include "Context.hpp"

#include "XavHeaders.hpp"
#include "FullImage.hpp"

namespace xav
{
	struct SwapChainDetails {
		vk::SurfaceCapabilitiesKHR surfaceCapabilities;		// Surface properties, e.g. image size/extent
		std::vector<vk::SurfaceFormatKHR> formats;			// Surface image formats, e.g. RGBA and size of each component
		std::vector<vk::PresentModeKHR> presentationModes;	// How images should be presented to screen
	};

	class SwapChain
	{
	public:
		SwapChain(Context& context);
		
		vk::Format getSurfaceFormat() const { return surfaceFormat.format; }
		vk::Extent2D getExtent() const { return extent; }
		vk::SwapchainKHR& getSwapChain() { return swapChain.get(); }
		const vk::SwapchainKHR* getSwapChainPtr() const { return &swapChain.get(); }
		FullImage* getDepthStencilFullImage()  { return depthStencilImage.get(); }
		FullImage* getColorMultisampleImage()  { return colorMultisampleImage.get(); }
		//std::vector<SwapChainImage>& getSwapChainImages() const { return swapChainImages; };
		const vk::ImageView& getSwapChainImageView(uint32_t index) {
			return swapChainImages[index]->getView();
		}
		uint32_t getSwapChainSize() const { return static_cast<uint32_t>(swapChainImages.size()); }
		uint32_t getImageCount() const { return imageCount; }
		uint32_t getMinImageCount() const { return minImageCount; }
		//void recreate(Context& context);
	private:
		Context* context;
		//void destroy();
		uint32_t imageCount;
		uint32_t minImageCount;
		std::unique_ptr<FullImage> depthStencilImage;
		std::unique_ptr<FullImage> colorMultisampleImage;
		vk::Extent2D extent;
		vk::UniqueSwapchainKHR		swapChain;
		vk::SurfaceFormatKHR		surfaceFormat;
		std::vector<std::unique_ptr<SwapChainImage>> swapChainImages;
		
		SwapChainDetails			getSwapChainDetails				(Context& context);
		vk::SurfaceFormatKHR		chooseBestSurfaceFormat			(const std::vector<vk::SurfaceFormatKHR>& formats);
		vk::PresentModeKHR			chooseBestPresentationMode		(Context& context);
		vk::Extent2D				chooseSwapExtent				(const VkSurfaceCapabilitiesKHR& surfaceCapability, Context& context);
		void						createSwapChainImages			(Context& context);

		void							createSurface				(const Context& context, GLFWwindow* window);
		vk::UniqueSwapchainKHR			createSwapchain				(Context& _context, vk::SwapchainKHR oldSwapchain = vk::SwapchainKHR());
		std::vector<vk::Framebuffer>	createFrameBuffers			(Context& context);
		
	};

}