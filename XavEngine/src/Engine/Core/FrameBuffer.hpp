#pragma once
#include "Context.hpp"
#include "SwapChain.hpp"
#include "RenderPass.hpp"

namespace xav
{
	class FrameBuffer
	{
	public:
		FrameBuffer(RenderPass* renderPass,SwapChain* swapChain, Context& context);

		const vk::Framebuffer& getFrameBuffer(int index) const {
			return frameBuffers[index].get();
		}
		
	private:
		std::vector<vk::UniqueFramebuffer> frameBuffers;
	};
}

