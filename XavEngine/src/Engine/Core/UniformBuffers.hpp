#pragma once
#include <Engine/Helpers/BufferHelper.hpp>
#include "XavHeaders.hpp"
#include "Context.hpp"
#include "SwapChain.hpp"

namespace xav {

	class UniformBuffers
	{
	public:
		UniformBuffers(size_t bufferSize, SwapChain* swapChain, Context& _context,
		vk::BufferUsageFlags bufferUsageBits = vk::BufferUsageFlagBits::eUniformBuffer | vk::BufferUsageFlagBits::eUniformTexelBuffer,
		vk::MemoryPropertyFlags memoryPropertyBits = vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent, 
		bool memoryMapped = true);
		~UniformBuffers(){ unmapMemory(); }
		uint32_t				getBufferCount() const { return static_cast<uint32_t>(uniformBuffers.size()); }
		const vk::Buffer& 		getBuffer(uint32_t index) const { return uniformBuffers[index].get(); }
		const vk::DeviceMemory& getMemory(uint32_t index) const { return uniformBufferMemories[index].get(); }
		void* 					getMappedMemory(uint32_t index) { return mappedMemory[index]; };
	private:
	 	Context* context;
		SwapChain* swapChain;
		size_t bufferSize;
		const void mapMemory();
		const void unmapMemory();
		std::vector<vk::UniqueBuffer> uniformBuffers;
		std::vector<vk::UniqueDeviceMemory> uniformBufferMemories;
		std::vector<void*> mappedMemory;
	};

}

