#include "GraphicsPipeline.hpp"
#include <Engine/Geometry/Vertex.hpp>
#include <Engine/Geometry/ModelData.hpp>
#include <Engine/Helpers/FileHelper.hpp>
#include <Engine/Helpers/ShaderHelper.hpp>

namespace xav
{
	GraphicsPipeline::GraphicsPipeline(RenderPass* renderPass,  DescriptorSetLayout* descriptorSet,  SwapChain* swapChain, Context& context)
	{
		createPushConstantRange();
		createShaderStages(context);
		createVertexBindings();
		createInputAssembly();
		createViewportAndScissor(swapChain);
		createDynamicStates();
		createRasterizationState();
		createMultisampling(context);
		createBlendStates();
		createPipelineLayout(descriptorSet, context);
		createDepthStencilInfo();
		createGraphicsPipeline(renderPass, context);
	}

	void GraphicsPipeline::createPipelineLayout(DescriptorSetLayout* descriptorSet, Context& context){
		// -- PIPELINE LAYOUT --
		std::array<vk::DescriptorSetLayout, 2> descriptorSetLayouts = { descriptorSet->getDescriptorSetLayout(), descriptorSet->getSamplerDescriptorSetLayout() };
		vk::PipelineLayoutCreateInfo pipelineLayoutCreateInfo;
		pipelineLayoutCreateInfo.setLayoutCount = static_cast<uint32_t>(descriptorSetLayouts.size());;
		pipelineLayoutCreateInfo.pSetLayouts = descriptorSetLayouts.data();
		pipelineLayoutCreateInfo.pushConstantRangeCount = static_cast<uint32_t>(pushConstantRanges.size());
		pipelineLayoutCreateInfo.pPushConstantRanges = pushConstantRanges.data();
		pipelineLayout = context.device.createPipelineLayoutUnique(pipelineLayoutCreateInfo);		
	}

	void GraphicsPipeline::createPushConstantRange()
	{
		pushConstantRanges.clear();
		vk::PushConstantRange pushConstantRangeV;
		pushConstantRangeV.stageFlags = vk::ShaderStageFlagBits::eVertex; 	// Shader stage push constant will go to
		pushConstantRangeV.offset = 0;										// OFfset into given data to pass to push constant
		pushConstantRangeV.size = 112;//sizeof(ModelData);

		pushConstantRanges.push_back(pushConstantRangeV);

		vk::PushConstantRange pushConstantRangeF;
		pushConstantRangeF.stageFlags = vk::ShaderStageFlagBits::eFragment; // Shader stage push constant will go to
		pushConstantRangeF.offset = 112;//offsetof(ModelData,textureOffset);										// OFfset into given data to pass to push constant
		pushConstantRangeF.size = sizeof(glm::vec4);							// Size of data being passed

		pushConstantRanges.push_back(pushConstantRangeF);
	}

	void GraphicsPipeline::createBlendStates(){
		colorBlendState.colorWriteMask = vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA; // Colors to apply blending to
		colorBlendState.blendEnable = VK_TRUE;													// Enable blending
		//Blending equation: (srcColorBlendFactor * new color) colorBlendOp (dstColorBlendFactor * old color)
		colorBlendState.srcColorBlendFactor = vk::BlendFactor::eSrcAlpha;
		colorBlendState.dstColorBlendFactor = vk::BlendFactor::eOneMinusSrcAlpha;
		colorBlendState.colorBlendOp = vk::BlendOp::eAdd; //summarized: (newCol.a * newCol) + ((1-newCol.a) * oldCol)
		colorBlendState.srcAlphaBlendFactor = vk::BlendFactor::eOne;
		colorBlendState.dstAlphaBlendFactor = vk::BlendFactor::eZero;
		colorBlendState.alphaBlendOp = vk::BlendOp::eAdd; // summarized: (1 * new alpha) + (0 * old alpha) = new alpha
		
		colorBlendingCreateInfo.logicOpEnable = VK_FALSE;				// Alternative to calculations, is to use logical operations
		colorBlendingCreateInfo.attachmentCount = 1;
		colorBlendingCreateInfo.pAttachments = &colorBlendState;		
	}

	void GraphicsPipeline::createRasterizationState(){
		rasterizerCreateInfo.depthClampEnable = VK_FALSE;			// Change if fragments beyond near/far planes are clipped(default) or clamped to planes
		rasterizerCreateInfo.rasterizerDiscardEnable = VK_FALSE;	// Wether to discard data and skip rasterizer. Never creates fragments, only suitable for pipeline without framebuffer output.
		rasterizerCreateInfo.polygonMode = vk::PolygonMode::eFill;	// How to handle filling points between vertices
		rasterizerCreateInfo.lineWidth = 1.0f;						// How thick lines should be when drawn
		rasterizerCreateInfo.cullMode = vk::CullModeFlagBits::eBack;// Backface culling
		rasterizerCreateInfo.frontFace = vk::FrontFace::eCounterClockwise; // Front face winding order
		rasterizerCreateInfo.depthBiasEnable = VK_FALSE;			// Wether to add depth bias to fragments (good for stopping "shadow acne" in shadow mapping)
	}

	void GraphicsPipeline::createShaderStages(Context& context){
		// Read SPIR-V code
		auto vertexShaderCode = FileHelper::readFile("Shaders/vert.spv");
		auto fragmentShaderCode = FileHelper::readFile("Shaders/frag.spv");
		// Create shader modules to link with graphics pipeline
		vertexShaderModule = ShaderHelper::createShaderModuleUnique(vertexShaderCode, context);
		fragmentShaderModule = ShaderHelper::createShaderModuleUnique(fragmentShaderCode, context);
		// -- SHADER STAGE CREATION INFORMATION --
		// Vertex stage createion information
		vk::PipelineShaderStageCreateInfo vertexShaderStageCreateInfo;
		vertexShaderStageCreateInfo.stage = vk::ShaderStageFlagBits::eVertex;
		vertexShaderStageCreateInfo.module = vertexShaderModule.get();								// Shader module to be used by stage
		vertexShaderStageCreateInfo.pName = "main";													// Entry point into shader
		shaderStages.push_back(vertexShaderStageCreateInfo);
		// Fragment stage createion information
		vk::PipelineShaderStageCreateInfo fragmentShaderStageCreateInfo;
		fragmentShaderStageCreateInfo.stage = vk::ShaderStageFlagBits::eFragment;
		fragmentShaderStageCreateInfo.module = fragmentShaderModule.get();							// Shader module to be used by stage
		fragmentShaderStageCreateInfo.pName = "main";												// Entry point into shader
		shaderStages.push_back(fragmentShaderStageCreateInfo);
	}

	void GraphicsPipeline::createInputAssembly(){
		// -- INPUT ASSEMBLY --
		inputAssembly.topology = vk::PrimitiveTopology::eTriangleList;  // Primitive type to assemble vertices as
		inputAssembly.primitiveRestartEnable = VK_FALSE;				// Allow overriding of "strip" topology to start new primitives		
	}

	void GraphicsPipeline::createMultisampling(Context& context){
		// -- MULTISAMPLING --
		
		multisamplingCreateInfo.sampleShadingEnable = context.getConfig().enableSampleShading;						// Enable multisample shading or not
		multisamplingCreateInfo.rasterizationSamples = context.getConfig().msaaSamples;
			//vk::SampleCountFlagBits::e1; // Number of samples to use per fragment
		//context.msaaSamples;
		multisamplingCreateInfo.minSampleShading = context.getConfig().minSampleShading;		
	}

	void GraphicsPipeline::createDynamicStates(){
		// -- DYNAMIC STATES --
		// Dynamic states to enable
		//std::vector<VkDynamicState> dynamicStateEnables;
		//dynamicStateEnables.push_back(VK_DYNAMIC_STATE_VIEWPORT);	// Dynamic Viewport: Can resize in command buffer with vkCmdSetViewport (commandbuffer, 0, 1, &viewport)
		//dynamicStateEnables.push_back(VK_DYNAMIC_STATE_SCISSOR);	// Dynamic Scissor: Can resize in command buffer with vkCmdSetScissor (commandbuffer, 0, 1, &scissor)

		//// Dynamic state creation info
		//VkPipelineDynamicStateCreateInfo dynamicStateCreateInfo = {};
		//dynamicStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
		//dynamicStateCreateInfo.dynamicStateCount = static_cast<uint32_t>(dynamicStateEnables.size());
		//dynamicStateCreateInfo.pDynamicStates = dynamicStateEnables.data();		
	}

	void GraphicsPipeline::createVertexBindings(){
		// How the data for a single vertex (including info such as poisition, color, texcoords, normals etc) is as a whole
		bindingDescription.binding = 0;									// Can bind multiple streams of data, this defines which one
		bindingDescription.stride = sizeof(Vertex);						// Size of a single vertex object
		bindingDescription.inputRate = vk::VertexInputRate::eVertex;	// How to move between data after each vertex
																		// VK_VERTEX_INPUT_RATE_VERTEX   : Move on to the next vertex
																		// VK_VERTEX_INPUT_RATE_INSTANCE : Move to a vertex for the next instance
		attributeDescriptions.clear();
		// Position attribute
		vk::VertexInputAttributeDescription v1;
		v1.binding = 0;							// Which binding the data is at (should be same as above, unless multiple streams)
		v1.location = 0;							// Location in shader where data will be read from
		v1.format = vk::Format::eR32G32B32Sfloat;// Format the data will take (also helps define size of data)
		v1.offset = offsetof(Vertex, pos);		// Where this attribute is defined in the data for a single vertex
		attributeDescriptions.push_back(v1);
		// Color attribut
		vk::VertexInputAttributeDescription v2;
		v2.binding = 0;
		v2.location = 1;
		v2.format = vk::Format::eR32G32B32Sfloat;
		v2.offset = offsetof(Vertex, normal);
		attributeDescriptions.push_back(v2);
		// Texture attribute
		vk::VertexInputAttributeDescription v3;
		v3.binding = 0;
		v3.location = 2;
		v3.format = vk::Format::eR32G32Sfloat;
		v3.offset = offsetof(Vertex, tex);
		attributeDescriptions.push_back(v3);
		// Tangent attribute
		vk::VertexInputAttributeDescription v4;
		v4.binding = 0;
		v4.location = 3;
		v4.format = vk::Format::eR32G32B32Sfloat;
		v4.offset = offsetof(Vertex, tangent);
		attributeDescriptions.push_back(v4);		
		// Tangent attribute
		vk::VertexInputAttributeDescription v5;
		v5.binding = 0;
		v5.location = 4;
		v5.format = vk::Format::eR32G32B32Sfloat;
		v5.offset = offsetof(Vertex, bitangent);
		attributeDescriptions.push_back(v5);			

		// -- VERTEX INPUT --
		vertexInputCreateInfo.vertexBindingDescriptionCount = 1;
		vertexInputCreateInfo.pVertexBindingDescriptions = &bindingDescription;							// List of Vertex binding descriptions (data spacing / stride info info)
		vertexInputCreateInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDescriptions.size());
		vertexInputCreateInfo.pVertexAttributeDescriptions = attributeDescriptions.data();						// List of attribute descriptions (data format and where to bind to/from)		
	}

	void GraphicsPipeline::createDepthStencilInfo(){
		// -- DEPTH STENCIL TESTING --
		depthStencilCreateInfo.depthTestEnable = VK_TRUE;				// Enable checking depth to determine fragment write
		depthStencilCreateInfo.depthWriteEnable = VK_TRUE;				// Update depth value if writing fragment
		depthStencilCreateInfo.depthCompareOp = vk::CompareOp::eLess;	// Comparison operation that allows an overwrite
		depthStencilCreateInfo.depthBoundsTestEnable = VK_FALSE;		// Depth bounds test: does the depth value exist between two bounds
		depthStencilCreateInfo.stencilTestEnable = VK_FALSE;			// Enable stencil test		
	}

	void GraphicsPipeline::createViewportAndScissor(SwapChain* swapChain){
		// -- VIEWPORT AND SCISSOR --
		// Create a viewport info struct
		viewport.x = 0.0f;												// x start coordinate
		viewport.y = 0.0f;												// y start coordinate
		viewport.width = (float)swapChain->getExtent().width;			// Width of viewport
		viewport.height = (float)swapChain->getExtent().height;			// Height of viewport	
		viewport.minDepth = 0.0f;										// Min framebuffer depth
		viewport.maxDepth = 1.0f;										// Max framebuffer depth

		// Create a scissor info struct
		scissor.offset = vk::Offset2D{ 0, 0 };										// Offset to use region from
		scissor.extent = swapChain->getExtent();								// Extent to desribe region to use, starting at offset.

		viewportStateCreateInfo.viewportCount = 1;
		viewportStateCreateInfo.pViewports = &viewport;
		viewportStateCreateInfo.scissorCount = 1;
		viewportStateCreateInfo.pScissors = &scissor;		
	}

	void GraphicsPipeline::createGraphicsPipeline(RenderPass* renderPass, Context& context){
		// -- GRAPHICS PIPELINE CREATION --
		vk::GraphicsPipelineCreateInfo pipelineCreateInfo ;
		pipelineCreateInfo.stageCount = static_cast<uint32_t>(shaderStages.size());									// Number of shader stages
		pipelineCreateInfo.pStages = shaderStages.data();							// List of shader stages
		pipelineCreateInfo.pVertexInputState = &vertexInputCreateInfo;		// All the fixed function pipeline states
		pipelineCreateInfo.pInputAssemblyState = &inputAssembly;
		pipelineCreateInfo.pViewportState = &viewportStateCreateInfo;
		pipelineCreateInfo.pDynamicState = nullptr;
		pipelineCreateInfo.pRasterizationState = &rasterizerCreateInfo;
		pipelineCreateInfo.pMultisampleState = &multisamplingCreateInfo;
		pipelineCreateInfo.pColorBlendState = &colorBlendingCreateInfo;
		pipelineCreateInfo.pDepthStencilState = &depthStencilCreateInfo;
		pipelineCreateInfo.layout = pipelineLayout.get();							// Pipeline layout pipeline should use
		pipelineCreateInfo.renderPass = renderPass->getRenderPass();							// Render pass description the pipeline is compatible with
		pipelineCreateInfo.subpass = 0;										// Subpass of render pass to use with pipeline
		// Pipeline derivatives, can create multiple pipelines that derive from one another for optimisation
		pipelineCreateInfo.basePipelineHandle = vk::Pipeline();				// Existing pipeline to derive from
		pipelineCreateInfo.basePipelineIndex = -1;							// or index of pipeline being created to derive from (in case creating multiple at once)
		graphicsPipeline = context.device.createGraphicsPipelineUnique(nullptr, pipelineCreateInfo).value;		
	}
}
