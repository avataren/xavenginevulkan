#include "Synchronization.hpp"
#include "Globals.hpp"

namespace xav
{

	Synchronization::Synchronization(uint32_t swapChainSize, Context& context)
	{
		vk::SemaphoreCreateInfo semaphoreCreateInfo;
		// Fence createion information
		vk::FenceCreateInfo fenceCreateInfo;
		fenceCreateInfo.flags = vk::FenceCreateFlagBits::eSignaled;
		//MAX_FRAME_DRAWS
		for (size_t i = 0; i < swapChainSize; i++)
		{
			imageAvailable.push_back (context.device.createSemaphoreUnique(semaphoreCreateInfo));
			renderFinished.push_back (context.device.createSemaphoreUnique(semaphoreCreateInfo));
			drawFences.push_back (context.device.createFenceUnique(fenceCreateInfo));
		}
	}

}
