#pragma once
//#define VK_USE_PLATFORM_WIN32_KHR
#include <vector>
#include <iostream>
#include "Version.hpp"
#include "XavHeaders.hpp"
#include <GLFW/glfw3.h>
#include <Engine/Helpers/DeviceHelper.hpp>
#include "AnsiColors.hpp"
#include <Engine/Config/Config.hpp>
#include <Engine/NetCore/NetCoreHost.hpp>

const std::vector<const char*> validationLayers = {
	"VK_LAYER_KHRONOS_validation"
};

class NetCoreHost;
namespace xav {

	// Indices (locations) of Queue Families (if they exist at all)
	struct QueueFamilyIndices {
		uint32_t graphicsFamily = -1;		// Location of Graphics Queue Family
		uint32_t presentationFamily = -1;	// Location of Presentation Queue Family
		// Check if queue families are valid
		bool isValid() {
			return graphicsFamily >= 0 && presentationFamily >= 0;
		}
	};

	class Context {
	public:
		Context(GLFWwindow* _window, Config& _config);
		~Context();

		GLFWwindow*			getWindow()			{ return window; }
		VkSurfaceKHR		getSurface()		{ return surface;  }
		QueueFamilyIndices	getQueueFamilies();
		uint32_t			getGraphicsQueueIndex()		{ return queueFamilyIndices.graphicsFamily; }
		uint32_t			getPresentationQueueIndex() { return queueFamilyIndices.presentationFamily; }
		vk::CommandPool&	getGraphicsCommandPool()    { return graphicsCommandPool; }

		bool hasDiscretePresentationQueue() {
			auto indices = getQueueFamilies();
			return (indices.graphicsFamily != indices.presentationFamily);
		}

		// Vulkan instance, stores all per-application states
		vk::Instance instance;
		std::vector<vk::PhysicalDevice> physicalDevices;
		// Physical device (GPU) that Vulkan will ise
		vk::PhysicalDevice physicalDevice;
		vk::Device device;
		// Stores physical device properties (for e.g. checking device limits)
		vk::PhysicalDeviceProperties deviceProperties;
		// Stores phyiscal device features (for e.g. checking if a feature is available)
		vk::PhysicalDeviceFeatures deviceFeatures;
		// Stores all available memory (type) properties for the physical device
		vk::PhysicalDeviceMemoryProperties deviceMemoryProperties;
		Version version;
		Version driverVersion;
		
		const vk::Queue getGraphicsQueue(uint32_t queueIndex=0) const;
		void reCreateSurface();
		void createContext(std::string name = "XavEngine");
		void destroyContext();

		Config& getConfig() { return config; }

		std::string toHumanSize(size_t size) {
			static const std::vector<std::string> SUFFIXES{ { "B", "KB", "MB", "GB", "TB", "PB" } };
			size_t suffixIndex = 0;
			while (suffixIndex < SUFFIXES.size() - 1 && size > 1024) {
				size >>= 10;
				++suffixIndex;
			}

			std::stringstream buffer;
			buffer << size << " " << SUFFIXES[suffixIndex];
			return buffer.str();
		}
#ifdef _DEBUG
		bool enableValidationLayers = true;
#else
		bool enableValidationLayers = false;
#endif

	private:
		NetCoreHost netCoreHost;
		QueueFamilyIndices queueFamilyIndices;
		VkDebugUtilsMessengerEXT callback;
		GLFWwindow* window;
		Config& config;
		VkSurfaceKHR surface;
		vk::CommandPool graphicsCommandPool;

		static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
			VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity, 
			VkDebugUtilsMessageTypeFlagsEXT messageType,
			const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
			void* pUserData) {
			if (messageSeverity & VkDebugUtilsMessageSeverityFlagBitsEXT::VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT)
				std::cerr <<ansi::foreground_bright << ansi::foreground_red;
			else if (messageSeverity & VkDebugUtilsMessageSeverityFlagBitsEXT::VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT)
				std::cerr << ansi::foreground_yellow;
			else
				std::cerr << ansi::foreground_white;

			std::cerr << "VL:: " << pCallbackData->pMessage << std::endl;
			std::cerr << ansi::foreground_normal << ansi::foreground_white;
			return VK_FALSE;
		}
		void createSurface(GLFWwindow* window);
		void createCommandPool();

		VkResult CreateDebugUtilsMessengerEXT(VkInstance instance, const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDebugUtilsMessengerEXT* pCallback);
		void DestroyDebugUtilsMessengerEXT(VkInstance instance, VkDebugUtilsMessengerEXT callback, const VkAllocationCallbacks* pAllocator);

		void setupDebugCallback();
		bool checkValidationLayerSupport();
		std::vector<const char*> getRequiredExtensions();
		void PrintDeviceInfo();
	};
}
