#include "DescriptorSetLayout.hpp"
#include <Engine/Core/Globals.hpp>
namespace xav
{
	DescriptorSetLayout::DescriptorSetLayout(Context& context)
	{

		vk::DescriptorSetLayoutBinding vpLayoutBinding;
		// Model binding info
		vpLayoutBinding.binding = 0;												// Binding point in shader (designated by binding in shader)
		vpLayoutBinding.descriptorType = vk::DescriptorType::eUniformBuffer;		// Type of descriptor(uniform, dynamic uniform, image sampler, etc...)
		vpLayoutBinding.descriptorCount = 1;										// Number of descriptors for binding
		vpLayoutBinding.stageFlags = vk::ShaderStageFlagBits::eVertex;
		vpLayoutBinding.pImmutableSamplers = nullptr;								// For texture: can make sampler data immutable by specifying in layout

		vk::DescriptorSetLayoutBinding lightsLayoutBinding;
		lightsLayoutBinding.binding = 1;
		lightsLayoutBinding.descriptorType = vk::DescriptorType::eUniformBuffer;
		lightsLayoutBinding.descriptorCount = 1;
		lightsLayoutBinding.stageFlags = vk::ShaderStageFlagBits::eFragment;
		lightsLayoutBinding.pImmutableSamplers = nullptr;

		std::vector<vk::DescriptorSetLayoutBinding> layoutBindings = { vpLayoutBinding, lightsLayoutBinding };

		// Create descriptor set layout with given bindings
		vk::DescriptorSetLayoutCreateInfo layoutCreateInfo;
		layoutCreateInfo.bindingCount = static_cast<uint32_t>(layoutBindings.size());	// Number of binding infos
		layoutCreateInfo.pBindings = layoutBindings.data();								// Array of binding infos

		// Create descriptor set layout
		descriptorSetLayout = context.device.createDescriptorSetLayoutUnique(layoutCreateInfo);

		// CREATE TEXTURE SAMPLER DESCIPTOR LAYOUT
		// Texture binding info
		std::vector<vk::DescriptorSetLayoutBinding> samplerLayouts;

		vk::DescriptorSetLayoutBinding samplerLayoutBinding;
		samplerLayoutBinding.binding = 0;
		samplerLayoutBinding.descriptorType = vk::DescriptorType::eCombinedImageSampler;
		samplerLayoutBinding.descriptorCount = MAX_TEXTURES;
		samplerLayoutBinding.stageFlags = vk::ShaderStageFlagBits::eFragment;
		samplerLayoutBinding.pImmutableSamplers = nullptr;
		samplerLayouts.push_back(samplerLayoutBinding);

		vk::DescriptorSetLayoutCreateInfo textureLayoutCreateInfo;
		textureLayoutCreateInfo.bindingCount = static_cast<uint32_t>(samplerLayouts.size());
		textureLayoutCreateInfo.pBindings = samplerLayouts.data();

		samplerSetLayout = context.device.createDescriptorSetLayoutUnique(textureLayoutCreateInfo);

		vk::DescriptorSetLayoutBinding vpShaderLayoutBinding;
		// shader uniform binding info
		vpShaderLayoutBinding.binding = 0;												// Binding point in shader (designated by binding in shader)
		vpShaderLayoutBinding.descriptorType = vk::DescriptorType::eUniformBuffer;		// Type of descriptor(uniform, dynamic uniform, image sampler, etc...)
		vpShaderLayoutBinding.descriptorCount = MAX_OBJECTS;								// Number of descriptors for binding
		vpShaderLayoutBinding.stageFlags = vk::ShaderStageFlagBits::eFragment;
		vpShaderLayoutBinding.pImmutableSamplers = nullptr;								// For texture: can make sampler data immutable by specifying in layout
		std::vector<vk::DescriptorSetLayoutBinding> shaderLayoutBindings = { vpShaderLayoutBinding };
		// Create descriptor set layout with given bindings
		vk::DescriptorSetLayoutCreateInfo shaderLayoutCreateInfo;
		shaderLayoutCreateInfo.bindingCount = static_cast<uint32_t>(shaderLayoutBindings.size());	// Number of binding infos
		shaderLayoutCreateInfo.pBindings = shaderLayoutBindings.data();							// Array of binding infos
		// Create descriptor set layout
		shaderUniformSetLayout = context.device.createDescriptorSetLayoutUnique(shaderLayoutCreateInfo);		

	}
}