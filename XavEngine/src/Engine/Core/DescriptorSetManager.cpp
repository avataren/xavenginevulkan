
#include "DescriptorSetManager.hpp"

namespace xav
{
    DescriptorSetManager::DescriptorSetManager() 
    {
        
    }
    
    DescriptorSetInfo DescriptorSetManager::GetDescriptors_PerFrame(uint32_t imageIndex) 
    {
        DescriptorSetInfo info;
        return info;
    }
    
    DescriptorSetInfo DescriptorSetManager::GetDescriptors_PerObject(uint32_t imageIndex, uint32_t objectId) 
    {
        DescriptorSetInfo info;
        return info;
    }
    
    void DescriptorSetManager::AddPerFrameDescriptorSet(vk::DescriptorSet descSet) 
    {
        
    }
    
    void DescriptorSetManager::AddPerFrameDescriptorSet(const std::vector<vk::DescriptorSet>& descSet) 
    {
        
    }
    
    void DescriptorSetManager::AddPerObjectDescriptorSet(const std::vector<vk::DescriptorSet>& descSet) 
    {
        
    }
}