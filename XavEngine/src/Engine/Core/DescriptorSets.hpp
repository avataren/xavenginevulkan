#pragma once
#include "XavHeaders.hpp"

#include "SwapChain.hpp"
#include "Context.hpp"
#include "DescriptorSetLayout.hpp"
#include "DescriptorPool.hpp"
#include <Engine/Core/BufferManager.hpp>

namespace xav
{
	class DescriptorSets
	{
	public:
		DescriptorSets(SwapChain* swapChain, DescriptorSetLayout* descriptorSetLayout, DescriptorPool* descriptorPool, BufferManager* bufferManager, Context& context);
		vk::DescriptorSet& getDescriptorSet(uint32_t index) {
			return *descriptorSets[index];
		}
	private:
		std::vector<vk::UniqueDescriptorSet> descriptorSets;		// one for each swap chain image
	};
}

