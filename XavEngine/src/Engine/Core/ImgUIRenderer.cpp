#include "ImgUIRenderer.hpp"
#include <imgui/imgui.h>
#include <imgui/imgui_impl_glfw.h>
#include <imgui/implot.h>
#include <Engine/Helpers/BufferHelper.hpp>

namespace xav {

	ImgUIRenderer::ImgUIRenderer(SwapChain* swapChain, Context& _context) : context(&_context)
	{
		// Setup Dear ImGui context
		IMGUI_CHECKVERSION();
		auto ctx = ImGui::CreateContext();
		//ImGui::SetCurrentContext(ctx);
		auto pltCtx = ImPlot::CreateContext();
		//ImGui::GetIO().BackendFlags |= ImGuiBackendFlags_PlatformHasViewports | ImGuiBackendFlags_RendererHasViewports;
		ImGui::GetIO().ConfigFlags |= ImGuiConfigFlags_DockingEnable;// | ImGuiConfigFlags_ViewportsEnable;
		//ImGui::GetIO().ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;

		//ImPlot::SetCurrentContext(pltCtx);
		setStyle();
		//ImGui::StyleColorsClassic();
		createDescriptorPool();
		createRenderPass(swapChain);
		createCommandPoolsAndBuffers(swapChain);
		createFramebuffers(swapChain);
		initializeImgui(swapChain);

		auto cmdBuffer = BufferHelper::beginCommandBuffer(_context);
		ImGui_ImplVulkan_CreateFontsTexture((VkCommandBuffer)cmdBuffer.get());//, VK_FORMAT_R8G8B8A8_SRGB);
		BufferHelper::endAndSubmitCommandBuffer(_context, cmdBuffer.get());
	}

	ImgUIRenderer::~ImgUIRenderer()
	{
		// Resources to destroy when the program ends
		ImGui_ImplVulkan_Shutdown();
		//ImGui_ImplGlfw_Shutdown();
		ImPlot::DestroyContext();
		ImGui::DestroyContext();
		for (int i = 0; i < frameBuffers.size(); i++)
		{
			vkDestroyFramebuffer(context->device, frameBuffers[i], nullptr);
		}
		//vkDestroyDescriptorPool(device, imguiDescriptorPool, nullptr);
		vkDestroyDescriptorPool(context->device, descriptorPool, nullptr/* g_Allocator*/);
		vkDestroyRenderPass(context->device, imGuiRenderPass, nullptr);

		//		for (int i=0;i< imGuiCommandPools.size();i++)
				//{
		//}
		vkFreeCommandBuffers(context->device, commandPool, static_cast<uint32_t>(commandBuffers.size()), commandBuffers.data());
		vkDestroyCommandPool(context->device, commandPool, nullptr);
	}

	void ImgUIRenderer::recreate(SwapChain* swapChain) {
		for (int i = 0; i < frameBuffers.size(); i++)
		{
			vkDestroyFramebuffer(context->device, frameBuffers[i], nullptr);
		}
		vkDestroyRenderPass(context->device, imGuiRenderPass, nullptr);
		createRenderPass(swapChain);
		createFramebuffers(swapChain);
	}

	void ImgUIRenderer::recordCommands(uint32_t imageIndex)
	{
	}

	void ImgUIRenderer::initializeImgui(SwapChain* swapChain) {
		// Setup Platform/Renderer bindings
		ImGui_ImplGlfw_InitForVulkan(context->getWindow(), true);
		ImGui_ImplVulkan_InitInfo init_info = {};
		init_info.Instance = context->instance;
		init_info.PhysicalDevice = context->physicalDevice;
		init_info.Device = context->device;
		init_info.QueueFamily = context->getQueueFamilies().graphicsFamily;
		//g_QueueFamily;
		init_info.Queue = context->getGraphicsQueue();
		init_info.PipelineCache = VK_NULL_HANDLE;
		//g_PipelineCache;
		init_info.DescriptorPool = descriptorPool;
		init_info.Allocator = nullptr;// g_Allocator;
		init_info.MinImageCount = swapChain->getMinImageCount();
		//g_MinImageCount;
		init_info.ImageCount = swapChain->getImageCount();
		init_info.CheckVkResultFn = check_vk_result;

		ImGui_ImplVulkan_Init(&init_info, imGuiRenderPass);
	}

	void ImgUIRenderer::createFramebuffers(SwapChain* swapChain) {
		frameBuffers.resize(swapChain->getImageCount());
		VkImageView attachment[1];
		VkFramebufferCreateInfo info = {};
		info.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
		info.renderPass = imGuiRenderPass;
		info.attachmentCount = 1;
		info.pAttachments = attachment;
		info.width = swapChain->getExtent().width;
		//wd->Width;
		info.height = swapChain->getExtent().height;
		//wd->Height;
		info.layers = 1;
		for (uint32_t i = 0; i < swapChain->getImageCount(); i++)
		{
			//ImGui_ImplVulkanH_Frame* fd = &wd->Frames[i];
			attachment[0] = swapChain->getSwapChainImageView(i);
			//fd->BackbufferView;
			auto err = vkCreateFramebuffer(context->device, &info, nullptr/*allocator*/, &frameBuffers[i]);
			check_vk_result(err);
		}
	}

	void ImgUIRenderer::createRenderPass(SwapChain* swapChain)
	{
		VkAttachmentDescription attachment = {};
		attachment.format = (VkFormat)swapChain->getSurfaceFormat();
		attachment.samples = VK_SAMPLE_COUNT_1_BIT;
		attachment.loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
		attachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
		attachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		attachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		attachment.initialLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
		attachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

		VkAttachmentReference color_attachment = {};
		color_attachment.attachment = 0;
		color_attachment.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

		VkSubpassDescription subpass = {};
		subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
		subpass.colorAttachmentCount = 1;
		subpass.pColorAttachments = &color_attachment;

		VkSubpassDependency dependency = {};
		dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
		dependency.dstSubpass = 0;
		dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		dependency.srcAccessMask = 0;  // or VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
		dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;






		VkRenderPassCreateInfo info = {};
		info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
		info.attachmentCount = 1;
		info.pAttachments = &attachment;
		info.subpassCount = 1;
		info.pSubpasses = &subpass;
		info.dependencyCount = 1;
		info.pDependencies = &dependency;
		if (vkCreateRenderPass(context->device, &info, nullptr, &imGuiRenderPass) != VK_SUCCESS) {
			throw std::runtime_error("Could not create Dear ImGui's render pass");
		}
	}

	void ImgUIRenderer::createDescriptorPool() {
		// Create Descriptor Pool
		VkDescriptorPoolSize pool_sizes[] =
		{
			{ VK_DESCRIPTOR_TYPE_SAMPLER, 1000 },
			{ VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1000 },
			{ VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, 1000 },
			{ VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 1000 },
			{ VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER, 1000 },
			{ VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER, 1000 },
			{ VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1000 },
			{ VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1000 },
			{ VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC, 1000 },
			{ VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC, 1000 },
			{ VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT, 1000 }
		};
		VkDescriptorPoolCreateInfo pool_info = {};
		pool_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
		pool_info.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
		pool_info.maxSets = 1000 * IM_ARRAYSIZE(pool_sizes);
		pool_info.poolSizeCount = (uint32_t)IM_ARRAYSIZE(pool_sizes);
		pool_info.pPoolSizes = pool_sizes;
		auto err = vkCreateDescriptorPool(context->device, &pool_info, nullptr/* g_Allocator*/, &descriptorPool);
		check_vk_result(err);
	}

	void ImgUIRenderer::createCommandPoolsAndBuffers(SwapChain* swapChain)
	{
		createCommandPool(&commandPool, VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT);
		commandBuffers.resize(swapChain->getImageCount());
		createCommandBuffers(commandBuffers.data(), static_cast<uint32_t>(commandBuffers.size()), commandPool);
	}

	void ImgUIRenderer::createCommandPool(VkCommandPool* commandPool, VkCommandPoolCreateFlags flags) {
		VkCommandPoolCreateInfo commandPoolCreateInfo = {};
		commandPoolCreateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
		commandPoolCreateInfo.queueFamilyIndex = context->getQueueFamilies().graphicsFamily;
		commandPoolCreateInfo.flags = flags;

		if (vkCreateCommandPool(context->device, &commandPoolCreateInfo, nullptr, commandPool) != VK_SUCCESS) {
			throw std::runtime_error("Could not create graphics command pool");
		}
	}

	void ImgUIRenderer::createCommandBuffers(VkCommandBuffer* commandBuffer, uint32_t commandBufferCount, VkCommandPool& commandPool) {
		VkCommandBufferAllocateInfo commandBufferAllocateInfo = {};
		commandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
		commandBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
		commandBufferAllocateInfo.commandPool = commandPool;
		commandBufferAllocateInfo.commandBufferCount = commandBufferCount;
		vkAllocateCommandBuffers(context->device, &commandBufferAllocateInfo, commandBuffer);
	}

	void ImgUIRenderer::recordCommands(SwapChain* swapChain, uint32_t imageIndex) {
		auto imgDraw = ImGui::GetDrawData();
		if (imgDraw != nullptr)
		{
			VkCommandBufferBeginInfo info = {};
			info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
			info.flags |= VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

			check_vk_result(vkBeginCommandBuffer(commandBuffers[imageIndex], &info));
			{
				std::array<VkClearValue, 1> clearValues = {};
				clearValues[0].color = vk::ClearColorValue(std::array<float, 4>{0.0f, 0.0f, 0.0f, 1.0f});
				VkRenderPassBeginInfo info = {};
				info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
				info.renderPass = imGuiRenderPass;
				info.framebuffer = frameBuffers[imageIndex];
				info.renderArea.extent.width = swapChain->getExtent().width;
				info.renderArea.extent.height = swapChain->getExtent().height;
				info.clearValueCount = 1;
				info.pClearValues = clearValues.data();

				vkCmdBeginRenderPass(commandBuffers[imageIndex], &info, VK_SUBPASS_CONTENTS_INLINE);
				ImGui_ImplVulkan_RenderDrawData(ImGui::GetDrawData(), commandBuffers[imageIndex]);
				vkCmdEndRenderPass(commandBuffers[imageIndex]);
				check_vk_result(vkEndCommandBuffer(commandBuffers[imageIndex]));
			}
		}
	}

	void ImgUIRenderer::setStyle()
	{
		ImGuiIO& io = ImGui::GetIO(); (void)io;
		ImFontConfig config;
		config.OversampleH = 2;
		config.OversampleV = 2;
		io.Fonts->AddFontFromFileTTF("fonts/Roboto-Regular.ttf", 20, &config);
		//io.Fonts->AddFontFromFileTTF("fonts/CodeSaver-Regular.otf", 24, &config);
		//io.Fonts->AddFontFromFileTTF("fonts/Play-Regular.ttf", 24, &config);
		io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
		io.KeyMap[ImGuiKey_Delete] = GLFW_KEY_DELETE;
		io.KeyMap[ImGuiKey_Backspace] = GLFW_KEY_BACKSPACE;
		io.ConfigDragClickToInputText = true;
		//io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls
		ImGui::GetStyle().WindowBorderSize = 0;
		// Setup Dear ImGui style
		//ImGui::StyleColorsDark();
		ImVec4* colors = ImGui::GetStyle().Colors;
		colors[ImGuiCol_Text] = ImVec4(1.00f, 1.00f, 1.00f, 1.00f);
		colors[ImGuiCol_TextDisabled] = ImVec4(0.50f, 0.50f, 0.50f, 1.00f);
		colors[ImGuiCol_WindowBg] = ImVec4(0.01f, 0.01f, 0.01f, 0.92f);
		colors[ImGuiCol_ChildBg] = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
		colors[ImGuiCol_PopupBg] = ImVec4(0.08f, 0.08f, 0.08f, 0.94f);
		colors[ImGuiCol_Border] = ImVec4(0.43f, 0.43f, 0.50f, 0.50f);
		colors[ImGuiCol_BorderShadow] = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
		colors[ImGuiCol_FrameBg] = ImVec4(0.08f, 0.10f, 0.12f, 0.76f);
		colors[ImGuiCol_FrameBgHovered] = ImVec4(0.44f, 0.57f, 0.72f, 0.40f);
		colors[ImGuiCol_FrameBgActive] = ImVec4(0.29f, 0.47f, 0.69f, 0.67f);
		colors[ImGuiCol_TitleBg] = ImVec4(0.03f, 0.03f, 0.05f, 1.00f);
		colors[ImGuiCol_TitleBgActive] = ImVec4(0.17f, 0.20f, 0.27f, 1.00f);
		colors[ImGuiCol_TitleBgCollapsed] = ImVec4(0.00f, 0.00f, 0.00f, 0.51f);
		colors[ImGuiCol_MenuBarBg] = ImVec4(0.09f, 0.10f, 0.15f, 1.00f);
		colors[ImGuiCol_ScrollbarBg] = ImVec4(0.02f, 0.02f, 0.02f, 0.53f);
		colors[ImGuiCol_ScrollbarGrab] = ImVec4(0.31f, 0.31f, 0.31f, 1.00f);
		colors[ImGuiCol_ScrollbarGrabHovered] = ImVec4(0.41f, 0.41f, 0.41f, 1.00f);
		colors[ImGuiCol_ScrollbarGrabActive] = ImVec4(0.51f, 0.51f, 0.51f, 1.00f);
		colors[ImGuiCol_CheckMark] = ImVec4(1.00f, 1.00f, 1.00f, 1.00f);
		colors[ImGuiCol_SliderGrab] = ImVec4(0.63f, 0.76f, 0.93f, 1.00f);
		colors[ImGuiCol_SliderGrabActive] = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);
		colors[ImGuiCol_Button] = ImVec4(0.54f, 0.66f, 0.81f, 0.40f);
		colors[ImGuiCol_ButtonHovered] = ImVec4(0.40f, 0.57f, 0.76f, 1.00f);
		colors[ImGuiCol_ButtonActive] = ImVec4(0.21f, 0.35f, 0.49f, 1.00f);
		colors[ImGuiCol_Header] = ImVec4(0.37f, 0.54f, 0.74f, 0.31f);
		colors[ImGuiCol_HeaderHovered] = ImVec4(0.23f, 0.40f, 0.69f, 0.80f);
		colors[ImGuiCol_HeaderActive] = ImVec4(0.24f, 0.37f, 0.53f, 1.00f);
		colors[ImGuiCol_Separator] = ImVec4(0.43f, 0.43f, 0.50f, 0.50f);
		colors[ImGuiCol_SeparatorHovered] = ImVec4(0.10f, 0.40f, 0.75f, 0.78f);
		colors[ImGuiCol_SeparatorActive] = ImVec4(0.10f, 0.40f, 0.75f, 1.00f);
		colors[ImGuiCol_ResizeGrip] = ImVec4(0.32f, 0.42f, 0.54f, 0.20f);
		colors[ImGuiCol_ResizeGripHovered] = ImVec4(0.61f, 0.73f, 0.88f, 0.67f);
		colors[ImGuiCol_ResizeGripActive] = ImVec4(0.26f, 0.59f, 0.98f, 0.95f);
		colors[ImGuiCol_Tab] = ImVec4(0.12f, 0.17f, 0.25f, 0.86f);
		colors[ImGuiCol_TabHovered] = ImVec4(0.27f, 0.47f, 0.71f, 0.80f);
		colors[ImGuiCol_TabActive] = ImVec4(0.31f, 0.39f, 0.48f, 1.00f);
		colors[ImGuiCol_TabUnfocused] = ImVec4(0.07f, 0.10f, 0.15f, 0.97f);
		colors[ImGuiCol_TabUnfocusedActive] = ImVec4(0.11f, 0.16f, 0.24f, 1.00f);
		colors[ImGuiCol_DockingPreview] = ImVec4(0.38f, 0.54f, 0.72f, 0.70f);
		colors[ImGuiCol_DockingEmptyBg] = ImVec4(0.20f, 0.20f, 0.20f, 1.00f);
		colors[ImGuiCol_PlotLines] = ImVec4(0.61f, 0.61f, 0.61f, 1.00f);
		colors[ImGuiCol_PlotLinesHovered] = ImVec4(1.00f, 0.43f, 0.35f, 1.00f);
		colors[ImGuiCol_PlotHistogram] = ImVec4(0.10f, 0.25f, 0.38f, 1.00f);
		colors[ImGuiCol_PlotHistogramHovered] = ImVec4(0.30f, 0.33f, 0.68f, 1.00f);
		colors[ImGuiCol_TableHeaderBg] = ImVec4(0.19f, 0.19f, 0.20f, 1.00f);
		colors[ImGuiCol_TableBorderStrong] = ImVec4(0.31f, 0.31f, 0.35f, 1.00f);
		colors[ImGuiCol_TableBorderLight] = ImVec4(0.23f, 0.23f, 0.25f, 1.00f);
		colors[ImGuiCol_TableRowBg] = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
		colors[ImGuiCol_TableRowBgAlt] = ImVec4(1.00f, 1.00f, 1.00f, 0.06f);
		colors[ImGuiCol_TextSelectedBg] = ImVec4(0.26f, 0.59f, 0.98f, 0.35f);
		colors[ImGuiCol_DragDropTarget] = ImVec4(1.00f, 1.00f, 0.00f, 0.90f);
		colors[ImGuiCol_NavHighlight] = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);
		colors[ImGuiCol_NavWindowingHighlight] = ImVec4(1.00f, 1.00f, 1.00f, 0.70f);
		colors[ImGuiCol_NavWindowingDimBg] = ImVec4(0.80f, 0.80f, 0.80f, 0.20f);
		colors[ImGuiCol_ModalWindowDimBg] = ImVec4(0.02f, 0.02f, 0.02f, 0.96f);
	}
}
