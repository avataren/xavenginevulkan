#pragma once

#include "Context.hpp"
#include "UniformBuffers.hpp"
#include "SwapChain.hpp"

namespace xav
{
	class DescriptorPool
	{
	public:
		DescriptorPool(	SwapChain* swapChain,
						Context& _context);

		vk::DescriptorPool getUniformDescriptorPool()  { return descriptorPool.get(); }
		vk::DescriptorPool getSamplerDescriptorPool()  { return samplerDescriptorPool.get(); }

	private:
		vk::UniqueDescriptorPool descriptorPool;
		vk::UniqueDescriptorPool samplerDescriptorPool;

	};

}

