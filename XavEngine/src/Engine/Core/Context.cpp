#include "Context.hpp"
#include "XavHeaders.hpp"
VULKAN_HPP_DEFAULT_DISPATCH_LOADER_DYNAMIC_STORAGE;

//#include "Debug.hpp"


namespace xav {
	Context::Context(GLFWwindow* _window, Config& _config) :  window(_window), config(_config)
	{
		createContext();

		if (config.msaaSamples > DeviceHelper::getMaxUsableSampleCount(physicalDevice))
		{
			config.msaaSamples = DeviceHelper::getMaxUsableSampleCount(physicalDevice);
			std::cerr << "Unsupported msaa count, setting count to max allowable" << std::endl;
		}

		createSurface(window);
		queueFamilyIndices = getQueueFamilies();
		createCommandPool();
#ifdef _DEBUG
		PrintDeviceInfo();
#endif
	}

	Context::~Context()
	{
		destroyContext();
	}

	const vk::Queue Context::getGraphicsQueue(uint32_t queueIndex) const
	{
		return device.getQueue(queueFamilyIndices.graphicsFamily, queueIndex);
	}

	void Context::createContext(std::string name) {

		vk::DynamicLoader dl;
		PFN_vkGetInstanceProcAddr vkGetInstanceProcAddr =
		dl.getProcAddress<PFN_vkGetInstanceProcAddr>("vkGetInstanceProcAddr");
		VULKAN_HPP_DEFAULT_DISPATCHER.init(vkGetInstanceProcAddr);

		// Vulkan instance
		vk::ApplicationInfo appInfo;
		appInfo.pApplicationName = name.c_str();
		appInfo.pEngineName = name.c_str();
		appInfo.apiVersion = VK_API_VERSION_1_2;
		appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
		appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);

		auto extensions = getRequiredExtensions();

		auto instanceCreateInfo = vk::InstanceCreateInfo(
			vk::InstanceCreateFlags(),
			&appInfo,
			0, nullptr, // enabled layers
			static_cast<uint32_t>(extensions.size()), extensions.data() // enabled extensions
		);

		if (enableValidationLayers && checkValidationLayerSupport())
		{
			instanceCreateInfo.setEnabledLayerCount(static_cast<uint32_t>(validationLayers.size()));
			instanceCreateInfo.setPEnabledLayerNames(validationLayers);
		}

		instance = vk::createInstance(instanceCreateInfo);
		VULKAN_HPP_DEFAULT_DISPATCHER.init(instance);
		// Physical device
		physicalDevices = instance.enumeratePhysicalDevices();
		physicalDevice = DeviceHelper::FindSuitableDevice(physicalDevices);
		
		// Gather physical device properties
		deviceProperties = physicalDevice.getProperties();
		version = deviceProperties.apiVersion;
		driverVersion = deviceProperties.driverVersion;
		deviceFeatures = physicalDevice.getFeatures();
		// Gather physical device memory properties
		deviceMemoryProperties = physicalDevice.getMemoryProperties();
		// set up the logical device

		device = DeviceHelper::CreateLogicalDevice(physicalDevice);
		VULKAN_HPP_DEFAULT_DISPATCHER.init(device);

		/*
			// TODO: Enable raytracing if available
			auto properties =
				physicalDevice.getProperties2<vk::PhysicalDeviceProperties2,
				vk::PhysicalDeviceRayTracingPipelinePropertiesKHR>();
			auto rtProperties = properties.get<vk::PhysicalDeviceRayTracingPipelinePropertiesKHR>();
		*/
		// Set up validation layers and debug callback if in debug mode
		setupDebugCallback();
	}

	void Context::destroyContext()
	{
		device.destroyCommandPool(graphicsCommandPool);
		vkDestroySurfaceKHR(instance, surface, nullptr);
		device.destroy();
		if (enableValidationLayers) {
			instance.destroyDebugUtilsMessengerEXT(callback);
		}
		instance.destroy();
	}

	QueueFamilyIndices Context::getQueueFamilies()
	{
		QueueFamilyIndices indices;
		uint32_t queueFamilyCount = 0;
		vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, nullptr);
		if (queueFamilyCount == 0)
			throw std::runtime_error("No queue families found!");

		std::vector<VkQueueFamilyProperties> queueFamilyList(queueFamilyCount);
		vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, queueFamilyList.data());

		// Go through each qeueue family and check if it has at least 1 of the required types of queue
		int i = 0;
		for (const auto& queueFamily : queueFamilyList) {
			// First check if queue family has at least 1 qeueue in that family (could have no queues)
			// Queue can be multiple types defined through bitfield. Need to bitwise AND with VK_QUEUE_*_BIT to check it.
			if (queueFamily.queueCount > 0 && queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT) {
				indices.graphicsFamily = i; // If queue family is valid, then get index
			}

			VkBool32 presentationSupport = false;
			vkGetPhysicalDeviceSurfaceSupportKHR(physicalDevice, i, surface, &presentationSupport);
			// Check if queue is presentation queue, can be both graphics and presentation
			if (queueFamily.queueCount > 0 && presentationSupport) {
				indices.presentationFamily = i;
			}
			// Check if queue family indices are in a valid state, stop searching if so
			if (indices.isValid()) {
				break;
			}
			i++;
		}
		return indices;
	}

	void Context::createCommandPool()
	{
		vk::CommandPoolCreateInfo poolInfo;
		poolInfo.flags = vk::CommandPoolCreateFlagBits::eResetCommandBuffer;
		poolInfo.queueFamilyIndex = queueFamilyIndices.graphicsFamily;//  Queue family type that buffers from this command pool will use
		// Create a graphics queue family command pool
		graphicsCommandPool = device.createCommandPool(poolInfo);
	}

	void Context::reCreateSurface() {
		vkDestroySurfaceKHR(instance, surface, nullptr);
		createSurface(window);
	}
	
	void Context::createSurface(GLFWwindow* window)
	{
		// Create Surface (creates a surface create info struct, runs the create surface function, returning a VkResult!
		auto result = glfwCreateWindowSurface(instance, window, nullptr, &surface);
		if (result != VK_SUCCESS)
			throw std::runtime_error("Failed to create a surface");
	}

	VkResult Context::CreateDebugUtilsMessengerEXT(VkInstance instance, const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDebugUtilsMessengerEXT* pCallback) {
		auto func = (PFN_vkCreateDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT");
		if (func != nullptr) {
			return func(instance, pCreateInfo, pAllocator, pCallback);
		}
		else {
			return VK_ERROR_EXTENSION_NOT_PRESENT;
		}
	}

	void Context::DestroyDebugUtilsMessengerEXT(VkInstance instance, VkDebugUtilsMessengerEXT callback, const VkAllocationCallbacks* pAllocator) {
		auto func = (PFN_vkDestroyDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT");
		if (func != nullptr) {
			func(instance, callback, pAllocator);
		}
	}

	void Context::setupDebugCallback() {
		if (!enableValidationLayers) return;

		auto createInfo = vk::DebugUtilsMessengerCreateInfoEXT(
			vk::DebugUtilsMessengerCreateFlagsEXT(),
			vk::DebugUtilsMessageSeverityFlagBitsEXT::eVerbose | 
			vk::DebugUtilsMessageSeverityFlagBitsEXT::eWarning | 
			vk::DebugUtilsMessageSeverityFlagBitsEXT::eError,
			vk::DebugUtilsMessageTypeFlagBitsEXT::eGeneral | 
			vk::DebugUtilsMessageTypeFlagBitsEXT::eValidation | 
			vk::DebugUtilsMessageTypeFlagBitsEXT::ePerformance,
			debugCallback,
			nullptr
		);
		callback = instance.createDebugUtilsMessengerEXT(createInfo);
	}

	std::vector<const char*> Context::getRequiredExtensions() {
		uint32_t glfwExtensionCount = 0;
		const char** glfwExtensions;
		glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

		std::vector<const char*> extensions(glfwExtensions, glfwExtensions + glfwExtensionCount);

		if (enableValidationLayers) {
			extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
		}

		return extensions;
	}

	bool Context::checkValidationLayerSupport() {
		auto availableLayers = vk::enumerateInstanceLayerProperties();
		for (const char* layerName : validationLayers) {
			bool layerFound = false;

			for (const auto& layerProperties : availableLayers) {
				if (strcmp(layerName, layerProperties.layerName) == 0) {
					layerFound = true;
					break;
				}
			}

			if (!layerFound) {
				return false;
			}
		}
		return true;
	}

	void Context::PrintDeviceInfo() {
		std::cout << ansi::foreground_bright << ansi::foreground_green;
		std::cout << "============================================CONTEXT CREATED============================================" << std::endl;
		std::cout << "Device Name:    " << deviceProperties.deviceName << std::endl;
		std::cout << "Device Type:    " << vk::to_string(deviceProperties.deviceType) << std::endl;
		std::cout << "API Version:    " << version.toString() << std::endl;
		std::cout << "Driver Version: " << driverVersion.toString() << std::endl;
		std::cout << "maxDescriptorSetUniformBuffers: " << deviceProperties.limits.maxDescriptorSetUniformBuffers << std::endl;
		std::cout << "maxDescriptorSetUniformBuffersDynamic: " << deviceProperties.limits.maxDescriptorSetUniformBuffersDynamic << std::endl;
		std::cout << "minUniformBufferOffsetAlignment: " << deviceProperties.limits.minUniformBufferOffsetAlignment << std::endl;
		std::cout << "maxPerStageDescriptorUniformBuffers: " << deviceProperties.limits.maxPerStageDescriptorUniformBuffers << std::endl;
		std::cout << "============================================STARTING ENGINE============================================" << std::endl;
		std::cout << ansi::foreground_normal << ansi::foreground_white;
	}
}
