#pragma once
#include <glm/glm.hpp>

namespace xav
{
	struct UboViewProjection {
		glm::mat4 projection;
	};
}
