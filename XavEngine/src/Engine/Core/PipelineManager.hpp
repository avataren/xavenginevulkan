#pragma once

#include <Engine/Core/GraphicsPipeline.hpp>
#include <Engine/Scene/SceneComponents.hpp>

namespace xav
{
    
    class PipelineManager
    {
        public:
        PipelineManager();
        
        GraphicsPipeline& getPipelineForMaterial(const MaterialComponent& material);

        private:

    };

} // namespace xav
