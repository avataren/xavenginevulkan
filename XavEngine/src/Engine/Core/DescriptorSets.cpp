#include "UboViewProjection.hpp"
#include "DescriptorSets.hpp"
#include <Engine/Core/Globals.hpp>
#include <Engine/Components/LightComponent.hpp>

xav::DescriptorSets::DescriptorSets(SwapChain* swapChain, DescriptorSetLayout* descriptorSetLayout, DescriptorPool* descriptorPool, BufferManager* bufferManager, Context& context)
{
	auto swapChainSize = swapChain->getSwapChainSize();
	
	std::vector<vk::DescriptorSetLayout> setLayouts(swapChainSize, descriptorSetLayout->getDescriptorSetLayout());
	
	vk::DescriptorSetAllocateInfo setAllocInfo;
	setAllocInfo.descriptorPool = descriptorPool->getUniformDescriptorPool();	// Pool to allocate descriptor sets from
	setAllocInfo.descriptorSetCount = static_cast<uint32_t>(setLayouts.size());
	setAllocInfo.pSetLayouts = setLayouts.data();								// Layouts to use to allocate sets (1:1 relationship)
	// Allocate descriptor sets (multiple)
	descriptorSets = context.device.allocateDescriptorSetsUnique(setAllocInfo);
	
	auto vertexPrFrame = &bufferManager->createUniformBuffer("VertexPerFrame",sizeof(UboViewProjection));
	auto NumLights =  MAX_LIGHTS;// (65536/sizeof(LightSourceComponent));
	auto lightsPrFrame = &bufferManager->createUniformBuffer("LightsPerFrame",sizeof(LightSourceComponent) * NumLights);
	//auto materialUBO = &bufferManager->createUniformBuffer("Materials",MAX_OBJECTS);

	// Update all descriptor set bindings
	for (size_t i = 0; i < swapChainSize; i++) {
		//VIEW PROJECTION DESCRIPTOR
		// Buffer info and data offset info
		vk::DescriptorBufferInfo vpBufferInfo;
		vpBufferInfo.buffer = vertexPrFrame->getBuffer(static_cast<uint32_t>(i));	// Buffer to get data from
		vpBufferInfo.offset = 0;													// Position of start of data
		vpBufferInfo.range = sizeof(UboViewProjection);								// size of data

		vk::DescriptorBufferInfo vpBufferInfoLights;
		vpBufferInfoLights.buffer = lightsPrFrame->getBuffer(static_cast<uint32_t>(i));	// Buffer to get data from
		vpBufferInfoLights.offset = 0;													// Position of start of data
		vpBufferInfoLights.range = sizeof(LightSourceComponent) * NumLights;



		// Data about connection between binding and buffer
		vk::WriteDescriptorSet vpSetWrite;
		vpSetWrite.dstSet = *descriptorSets[i];								// Descriptor set to update
		vpSetWrite.dstBinding = 0;											// Binding to update, matches shader layout
		vpSetWrite.dstArrayElement = 0;										// Index in array to update
		vpSetWrite.descriptorType = vk::DescriptorType::eUniformBuffer;		// Type of descriptor
		vpSetWrite.descriptorCount = 1;										// Amount to update
		vpSetWrite.pBufferInfo = &vpBufferInfo;								// Information about buffer data to bind

		vk::WriteDescriptorSet vpSetWriteLights;
		vpSetWriteLights.dstSet = *descriptorSets[i];								// Descriptor set to update
		vpSetWriteLights.dstBinding = 1;											// Binding to update, matches shader layout
		vpSetWriteLights.dstArrayElement = 0;										// Index in array to update
		vpSetWriteLights.descriptorType = vk::DescriptorType::eUniformBuffer;		// Type of descriptor
		vpSetWriteLights.descriptorCount = 1;										// Amount to update
		vpSetWriteLights.pBufferInfo = &vpBufferInfoLights;		

		//MODEL DESCRIPTOR
		//Model buffer binding info
		//VkDescriptorBufferInfo modelBufferInfo = {};
		//modelBufferInfo.buffer = modelDUniformBuffer[i];
		//modelBufferInfo.offset = 0;
		//modelBufferInfo.range = modelUniformAlignment;

		//VkWriteDescriptorSet modelSetWrite = {};
		//modelSetWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		//modelSetWrite.dstSet = descriptorSets[i];
		//modelSetWrite.dstBinding = 1;
		//modelSetWrite.dstArrayElement = 0;
		//modelSetWrite.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
		//modelSetWrite.descriptorCount = 1;
		//modelSetWrite.pBufferInfo = &modelBufferInfo;

		std::vector<vk::WriteDescriptorSet> setWrites = { vpSetWrite, vpSetWriteLights };
		context.device.updateDescriptorSets(setWrites, 0);
		// Update the descriptor sets with new buffer/binding info
		//vkUpdateDescriptorSets(mainDevice.logicalDevice, static_cast<uint32_t>(setWrites.size()), setWrites.data(), 0, nullptr);
	}
}
