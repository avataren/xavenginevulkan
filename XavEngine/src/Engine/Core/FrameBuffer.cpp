#include "FrameBuffer.hpp"
namespace xav
{
	FrameBuffer::FrameBuffer(RenderPass* renderPass, SwapChain* swapChain, Context& context)
	{
		bool noAntialiasing = context.getConfig().msaaSamples == vk::SampleCountFlagBits::e1;
		// Create a framebuffer for each swap chain image
		for (size_t i = 0; i < swapChain->getSwapChainSize(); i++) {

			std::array<vk::ImageView, 3> attachments = {
				swapChain->getColorMultisampleImage()->getView(),
				swapChain->getDepthStencilFullImage()->getView(),
				swapChain->getSwapChainImageView(static_cast<uint32_t>(i))
			};

			std::array<vk::ImageView, 2> attachments_noAA = {
				swapChain->getSwapChainImageView(static_cast<uint32_t>(i)),
				swapChain->getDepthStencilFullImage()->getView()
			};

			uint32_t attachmentCount = static_cast<uint32_t>(noAntialiasing ? attachments_noAA.size() : attachments.size());

			vk::FramebufferCreateInfo framebufferCreateInfo;
			framebufferCreateInfo.renderPass = renderPass->getRenderPass();											// Render pass layout the framebuffer will be used with
			framebufferCreateInfo.attachmentCount = attachmentCount;												// 
			framebufferCreateInfo.pAttachments = noAntialiasing ? attachments_noAA.data()  : attachments.data();	// List of attachments (1:1 with Render Pass)
			framebufferCreateInfo.width = swapChain->getExtent().width;												// Framebuffer width
			framebufferCreateInfo.height = swapChain->getExtent().height;											// Framebuffer height
			framebufferCreateInfo.layers = 1;																		// Framebuffer layers
			
			frameBuffers.push_back(context.device.createFramebufferUnique(framebufferCreateInfo));
		}
	}
}