#pragma once
#include "XavHeaders.hpp"
#include "Context.hpp"
#include "SwapChain.hpp"
namespace xav{

	class RenderPass
	{
	public:
		RenderPass(SwapChain* swapChain, Context& context);
		vk::RenderPass& getRenderPass()  { return renderPass.get(); }

	private:
		vk::UniqueRenderPass renderPass;
	};
}

