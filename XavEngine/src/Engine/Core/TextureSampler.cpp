#include "TextureSampler.hpp"
namespace xav {
	TextureSampler::TextureSampler(Context& _context) : context(&_context)
	{
		create(_context);
	}

	TextureSampler::~TextureSampler()
	{
		destroy();
	}
}