#pragma once
#include "XavHeaders.hpp"
#include "Context.hpp"
#include "SwapChain.hpp"

namespace xav
{
	class Synchronization
	{
	public:
		Synchronization(uint32_t swapChainSize,Context& context);
		
		std::vector<vk::UniqueSemaphore> imageAvailable;
		std::vector<vk::UniqueSemaphore> renderFinished;
		std::vector<vk::UniqueFence>	 drawFences;
	};
}

