#pragma once
#include <Engine/Core/DescriptorSets.hpp>
#include <Engine/Core/XavHeaders.hpp>

namespace xav
{
    struct DescriptorSetInfo{
        vk::DescriptorSetLayout layout;
        std::list<vk::DescriptorSet> descriptorSet;  
    };


    // TODO: add texture-descritor set
    class DescriptorSetManager
    {

        public:
        DescriptorSetManager();
        
        DescriptorSetInfo GetDescriptors_PerFrame   (uint32_t imageIndex);
        DescriptorSetInfo GetDescriptors_PerObject  (uint32_t imageIndex, uint32_t objectId);

        void AddPerFrameDescriptorSet(vk::DescriptorSet descSet);
        void AddPerFrameDescriptorSet(const std::vector<vk::DescriptorSet>& descSet);
        void AddPerObjectDescriptorSet(const std::vector<vk::DescriptorSet>& descSet);

        private:
        virtual void SetupDescriptorSetLayout() = 0;
        
        Ref<DescriptorSets> perObjectSets;
    };
}