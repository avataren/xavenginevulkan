#pragma once
#include <vector>

#include "Context.hpp"
#include "XavHeaders.hpp"
#include "SwapChain.hpp"

namespace xav
{
	class CommandBuffers
	{
	public:
		CommandBuffers(SwapChain* swapChain, Context& context);
		
		const vk::CommandBuffer& getGraphicsCommandBuffer(uint32_t index) {
			return commandBuffers[index].get();
		};

	private:
		std::vector<vk::UniqueCommandBuffer> commandBuffers;
		//void createCommandBuffers(const SwapChain& swapChain, Context& context);
	};
}

