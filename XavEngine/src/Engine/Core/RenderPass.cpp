#include "RenderPass.hpp"

xav::RenderPass::RenderPass(SwapChain* swapChain, Context& context)
{
	auto format = swapChain->getSurfaceFormat();
	//vk::Format::eR32G32B32A32Sfloat;
	//swapChain->getSurfaceFormat();

	bool noAntialiasing = context.getConfig().msaaSamples == vk::SampleCountFlagBits::e1;

	vk::AttachmentDescription colorAttachment{
		vk::AttachmentDescriptionFlags(),
		format,
		///vk::SampleCountFlagBits::e1,
		context.getConfig().msaaSamples,
		vk::AttachmentLoadOp::eClear,
		vk::AttachmentStoreOp::eStore,
		vk::AttachmentLoadOp::eDontCare,
		vk::AttachmentStoreOp::eDontCare,
		vk::ImageLayout::eUndefined,
		//vk::ImageLayout::ePresentSrcKHR
		vk::ImageLayout::eColorAttachmentOptimal
	};

	if (noAntialiasing)
	{
		//colorAttachment.finalLayout = vk::ImageLayout::ePresentSrcKHR;
		colorAttachment.finalLayout = vk::ImageLayout::eColorAttachmentOptimal;
		//VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
	}

	// REFERENCES
	
	// Depth attachment of render pass
	vk::AttachmentDescription depthAttachment = {
		vk::AttachmentDescriptionFlags(),
		swapChain->getDepthStencilFullImage()->format,
		context.getConfig().msaaSamples,
		vk::AttachmentLoadOp::eClear,
		vk::AttachmentStoreOp::eDontCare,
		vk::AttachmentLoadOp::eDontCare,
		vk::AttachmentStoreOp::eDontCare,
		vk::ImageLayout::eUndefined,
		vk::ImageLayout::eDepthStencilAttachmentOptimal
	};

	vk::AttachmentDescription colorAttachmentResolve;
	colorAttachmentResolve.samples = vk::SampleCountFlagBits::e1,
	colorAttachmentResolve.loadOp = vk::AttachmentLoadOp::eDontCare;
	colorAttachmentResolve.storeOp = vk::AttachmentStoreOp::eStore;
	colorAttachmentResolve.stencilLoadOp = vk::AttachmentLoadOp::eDontCare;
	colorAttachmentResolve.stencilStoreOp = vk::AttachmentStoreOp::eDontCare;
	colorAttachmentResolve.initialLayout = vk::ImageLayout::eUndefined;
	//colorAttachmentResolve.finalLayout = vk::ImageLayout::ePresentSrcKHR;
	colorAttachmentResolve.finalLayout = vk::ImageLayout::eColorAttachmentOptimal;
	colorAttachmentResolve.format = format;
	//swapChain->getSurfaceFormat();


	// Attachment reference uses an attachment index that refers to index in the attachment list passte to renderPassCreateInfo
	vk::AttachmentReference colorAttachmentReference{0,	vk::ImageLayout::eColorAttachmentOptimal }; // Layout
	// Depth attachment reference
	vk::AttachmentReference depthAttachmentReference{1,	vk::ImageLayout::eDepthStencilAttachmentOptimal };	// Layout
	// Resolve attachment (for multisampling) reference
	vk::AttachmentReference colorAttachmentResolveRef{ 2, vk::ImageLayout::eColorAttachmentOptimal };


	// - Information about a particular subass that the render pass is using
	vk::SubpassDescription subpass{
		vk::SubpassDescriptionFlags(),		// Flags
		vk::PipelineBindPoint::eGraphics,	// Pipeline bind point
		0,									// Input attachment count
		nullptr,							// Input attachments
		1,									// Color attachments count
		&colorAttachmentReference,			// Color attachments
		&colorAttachmentResolveRef,			//&multiSamplingAttachmentReference (Resolve attachments)
		&depthAttachmentReference,			// Depth stencil attachments
		0,									// Preserve attachments count
		nullptr 
	};
	if (noAntialiasing)
		subpass.pResolveAttachments = nullptr;

	// Need to determine when layout transitions occur using subpass depencies
	std::array<vk::SubpassDependency, 2> subPassDependencies;
	// Conversion from VK_IMAGE_LAYOUT_UNDEFINED to VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
	// Transition must happen after ...
	subPassDependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;		
	subPassDependencies[0].srcStageMask = vk::PipelineStageFlagBits::eBottomOfPipe;
	subPassDependencies[0].srcAccessMask = vk::AccessFlagBits::eMemoryRead;
	// But must happen before...
	subPassDependencies[0].dstSubpass = 0;
	subPassDependencies[0].dstStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput;
	subPassDependencies[0].dstAccessMask = vk::AccessFlagBits::eColorAttachmentRead | vk::AccessFlagBits::eColorAttachmentWrite;
	subPassDependencies[0].dependencyFlags = vk::DependencyFlags();

	// Conversion from VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL to VK_IMAGE_LAYOUT_PRESENT_SRC_KHR
	// Transition must happen after ...
	subPassDependencies[1].srcSubpass = 0;
	subPassDependencies[1].srcStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput;
	subPassDependencies[1].srcAccessMask = vk::AccessFlagBits::eColorAttachmentRead | vk::AccessFlagBits::eColorAttachmentWrite;
	// But must happen before...
	subPassDependencies[1].dstSubpass = VK_SUBPASS_EXTERNAL;
	subPassDependencies[1].dstStageMask = vk::PipelineStageFlagBits::eBottomOfPipe;
	subPassDependencies[1].dstAccessMask = vk::AccessFlagBits::eMemoryRead;
	subPassDependencies[1].dependencyFlags = vk::DependencyFlags();

	std::array<vk::AttachmentDescription, 3>  renderPassAttachments = { colorAttachment, depthAttachment, colorAttachmentResolve }; // ORDER IS IMPORTANT!
	std::array<vk::AttachmentDescription, 2>  renderPassAttachments_noAA = { colorAttachment, depthAttachment };

	vk::RenderPassCreateInfo renderPassCreateInfo{
		vk::RenderPassCreateFlags(),
		renderPassAttachments,
		subpass,
		subPassDependencies,
	};

	vk::RenderPassCreateInfo renderPassCreateInfo_noAA{
		vk::RenderPassCreateFlags(),
		renderPassAttachments_noAA,
		subpass,
		subPassDependencies,
	};

	// Create info for render pass
	//VkRenderPassCreateInfo renderPassCreateInfo = {};
	//renderPassCreateInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	//renderPassCreateInfo.attachmentCount = static_cast<uint32_t>(renderPassAttachments.size());
	//renderPassCreateInfo.pAttachments = renderPassAttachments.data();
	//renderPassCreateInfo.subpassCount = 1;
	//renderPassCreateInfo.pSubpasses = &subpass;
	//renderPassCreateInfo.dependencyCount = static_cast<uint32_t>(subPassDependencies.size());
	//renderPassCreateInfo.pDependencies = subPassDependencies.data();
	if (noAntialiasing)
		renderPass = context.device.createRenderPassUnique(renderPassCreateInfo_noAA);
	else
		renderPass = context.device.createRenderPassUnique(renderPassCreateInfo);
	//auto result = vkCreateRenderPass(context.device, &renderPassCreateInfo, nullptr, &renderPass);
	//if (result != VK_SUCCESS)
//		throw std::runtime_error("Failed to create render pass");
}


