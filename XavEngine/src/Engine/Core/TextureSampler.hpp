#pragma once

#include "XavHeaders.hpp"

#include "Context.hpp"

namespace xav {

	class TextureSampler
	{
	public:
		TextureSampler(Context& _context);
		~TextureSampler();

		vk::Sampler getSampler() const { return sampler; }

		void create(Context& _context) {
			vk::SamplerCreateInfo samplerCreateInfo = {};
			samplerCreateInfo.magFilter = vk::Filter::eLinear;
			samplerCreateInfo.minFilter = vk::Filter::eLinear;
			samplerCreateInfo.addressModeU = vk::SamplerAddressMode::eRepeat;
			samplerCreateInfo.addressModeV = vk::SamplerAddressMode::eRepeat;
			samplerCreateInfo.addressModeW = vk::SamplerAddressMode::eRepeat;
			samplerCreateInfo.borderColor = vk::BorderColor::eIntOpaqueBlack;
			samplerCreateInfo.unnormalizedCoordinates = VK_FALSE;				// Whether coords should be normalized between 0 and 1, or use pixel index
			samplerCreateInfo.mipmapMode = ::vk::SamplerMipmapMode::eLinear;
			samplerCreateInfo.mipLodBias = 0.0f;								// Level of detail bias for mip level
			samplerCreateInfo.minLod = 0.0f;									// Minimum lod to pick mip level
			samplerCreateInfo.maxLod = 128.0f;									// Maximum ... (usually equal to mipLevels for texture)
			samplerCreateInfo.anisotropyEnable = VK_TRUE;						// Perspective correct mapping
			samplerCreateInfo.maxAnisotropy = context->getConfig().maxAnisotropy;

			sampler = _context.device.createSampler(samplerCreateInfo);
		}

		void destroy() {
			context->device.destroySampler(sampler);
		}
	private:
		Context* context;

		vk::Sampler sampler;
	};

}

