#pragma once
#include <Engine/Core/Context.hpp>
#include <Engine/Core/SwapChain.hpp>
#include <Engine/Core/UniformBuffers.hpp>
#include <unordered_map>
#include <Engine/Core/XavHeaders.hpp>

namespace xav
{
    template< typename T >
    struct uniform_buffer_deleter
    {
        void operator ()(T const* p)
        {
            //p->unmapMemory();
            delete p;
        }
    };

    class BufferManager {
    public:
        BufferManager(Context& _context, SwapChain* _swapChain);
        UniformBuffers& getUniformBuffer(const std::string& name);
        UniformBuffers* getUniformBufferPtr(const std::string& name);
        UniformBuffers& createUniformBuffer(const std::string& name, size_t bufferItemSize);
    private:
        

        std::unordered_map <std::string, std::unique_ptr<UniformBuffers> > uniformBuffers;


        Context* context;
        SwapChain* swapChain;
    };
}