#include "CommandBuffers.hpp"
namespace xav
{
	CommandBuffers::CommandBuffers(SwapChain* swapChain,Context& context){
		vk::CommandBufferAllocateInfo cbAllocInfo;
		cbAllocInfo.commandPool = context.getGraphicsCommandPool();
		//graphicsCommandPool;
		cbAllocInfo.level = vk::CommandBufferLevel::ePrimary;
		//VK_COMMAND_BUFFER_LEVEL_PRIMARY;	// secondary would be executed by another command buffer (vkCmdExecuteCommands(buffer)), primary goes straight to queue
		cbAllocInfo.commandBufferCount = static_cast<uint32_t>(swapChain->getSwapChainSize());
		// Allocate command buffers and place handles in array of buffers
		commandBuffers = context.device.allocateCommandBuffersUnique(cbAllocInfo);
	}
}