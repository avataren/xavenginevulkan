#include "FullImage.hpp"
#include <Engine/Helpers/MemoryHelper.hpp>

namespace xav {

	FullImage::FullImage(const vk::Extent2D& extent,
		vk::Format _format,
		vk::ImageTiling tiling,
		vk::ImageUsageFlags usageFlags,
		vk::MemoryPropertyFlags propFlags,
		Context& context,
		vk::ImageAspectFlags aspectFlags,
		uint32_t mipLevels,
		vk::SampleCountFlagBits sampleCount,
		vk::ImageLayout imageLayout) : format(_format)
	{
		// CREATE IMAGE
		vk::ImageCreateInfo imageInfo{
			vk::ImageCreateFlags(),				// Flags
			vk::ImageType::e2D,					// Image type
			format,								// Format
			{extent.width, extent.height, 1},	// Extent
			mipLevels,							// Mip levels
			1,									// Array layers
			sampleCount,						// Sample count
			tiling,								// Tiling
			usageFlags,							// Usage flags
			vk::SharingMode::eExclusive,		// Sharing mode
			0,									// Queue family index count
			nullptr,							// Queue family indices,
			imageLayout
		}; // Initial layout

		image = context.device.createImageUnique(imageInfo);
		auto memoryRequirements = context.device.getImageMemoryRequirements(image.get());
		vk::MemoryAllocateInfo memoryAllocInfo;
		memoryAllocInfo.memoryTypeIndex = MemoryHelper::findMemoryTypeIndex(context.physicalDevice, memoryRequirements.memoryTypeBits, propFlags);
		memoryAllocInfo.allocationSize = memoryRequirements.size;
		memory = context.device.allocateMemoryUnique(memoryAllocInfo);
		context.device.bindImageMemory(image.get(), memory.get(), 0);
		
		view = ImageHelper::createImageViewUnique(
			image.get(),
			format,
			aspectFlags,
			context,
			mipLevels);
	};

}