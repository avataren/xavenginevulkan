#include "BufferManager.hpp"

namespace xav
{
    BufferManager::BufferManager(Context& _context, SwapChain* _swapChain) : context(&_context), swapChain(_swapChain)
    {
        //deleteBufferFunc =  [](UniformBuffers *p) {p->unmapMemory(); delete p; };
    }
    
    UniformBuffers& BufferManager::createUniformBuffer(const std::string& name, size_t bufferItemSize) {
            auto deleter = [](UniformBuffers *p) { delete p; };
         uniformBuffers[name] = std::unique_ptr<UniformBuffers>(new UniformBuffers(bufferItemSize, swapChain, *context));
         //std::make_unique<UniformBuffers>(UniformBuffers(bufferItemSize, swapChain, *context), [](UniformBuffers *p) {p->unmapMemory(); delete p; });

         return *uniformBuffers[name].get();
    }

    UniformBuffers& BufferManager::getUniformBuffer(const std::string& name) 
    {
        if (uniformBuffers.find(name) == uniformBuffers.end())
        {
            throw std::runtime_error("Buffer does not exist");
        }
        return *uniformBuffers[name].get();
    }
    
    UniformBuffers* BufferManager::getUniformBufferPtr(const std::string& name) 
    {
        if (uniformBuffers.find(name) == uniformBuffers.end())
        {
            throw std::runtime_error("Buffer does not exist");
        }
        return uniformBuffers[name].get();        
    }

}