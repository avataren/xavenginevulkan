#pragma once
#include "Context.hpp"
#include <Engine/Geometry/Vertex.hpp>
#include <mutex>

namespace xav
{

	class GeometryBuffers
	{
	public:
		GeometryBuffers(Context& context);

		size_t const currentVertexOffset() {
			return vertexBufferOffset;
		}

		size_t const currentIndexOffset() {
			return indexBufferOffset;
		}

		void copyToVertexBuffer(
			Context& context,
			std::vector<Vertex>* vertices,
			size_t* vertexOffset,
			vk::CommandPool* commandPool);

		void copyToIndexBuffer(
			Context& context,
			std::vector<uint32_t>* indices,
			size_t* indexOffset,
			vk::CommandPool* commandPool);

		vk::Buffer getVertexBuffer()
		{
			return vertexBuffer.get();
		}

		vk::Buffer getIndexBuffer()
		{
			return indexBuffer.get();
		}
	private:
		std::mutex vertexMtx;
		std::mutex indexMtx;
		static constexpr vk::DeviceSize MAX_VERTEX_COUNT = 10000000;
		static constexpr vk::DeviceSize MAX_INDEX_COUNT = MAX_VERTEX_COUNT*8;

		void createVertexBuffer(Context& context);
		void createIndexBuffer(Context& context);

		vk::DeviceSize vertexBufferOffset;
		vk::DeviceSize indexBufferOffset;

		vk::UniqueBuffer vertexBuffer;
		vk::UniqueDeviceMemory vertexBufferMemory;

		vk::UniqueBuffer indexBuffer;
		vk::UniqueDeviceMemory indexBufferMemory;
	};

}

