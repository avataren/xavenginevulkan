#pragma once
#include "XavHeaders.hpp"
#include "Context.hpp"
#include "SwapChain.hpp"
#include <imgui/imgui_impl_vulkan.h>

namespace xav
{
	class ImgUIRenderer
	{
	public:
		ImgUIRenderer(SwapChain* swapChain, Context& context);
		~ImgUIRenderer();
		void recordCommands(uint32_t imageIndex);
		std::vector<VkCommandBuffer> commandBuffers;
		VkCommandPool commandPool;
		void recordCommands(SwapChain* swapChain, uint32_t imageIndex);
		void recreate(SwapChain* swapChain);
	private:
		Context* context;
		VkDescriptorPool descriptorPool = VK_NULL_HANDLE;
		VkRenderPass imGuiRenderPass = VK_NULL_HANDLE;

		//std::vector<VkCommandPool> 
		
		std::vector<VkFramebuffer> frameBuffers;
		void setStyle();
		void createCommandPool(VkCommandPool* commandPool, VkCommandPoolCreateFlags flags);
		void createCommandBuffers(VkCommandBuffer* commandBuffer, uint32_t commandBufferCount, VkCommandPool& commandPool);
		void createFramebuffers(SwapChain* swapChain);
		void createDescriptorPool();
		void createCommandPoolsAndBuffers(SwapChain* swapChain);
		void initializeImgui(SwapChain* swapChain);
		void createRenderPass(SwapChain* swapChain);

		static void check_vk_result(VkResult err) {
			if (err != VK_SUCCESS)
			{
				throw std::runtime_error("Imgui encountered a problem.");
			}
		}
	};
}

