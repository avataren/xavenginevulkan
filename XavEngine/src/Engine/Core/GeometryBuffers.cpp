#include "GeometryBuffers.hpp"
#include <Engine/Helpers/BufferHelper.hpp>

namespace xav
{
	GeometryBuffers::GeometryBuffers(Context& context)
	{
		vertexBufferOffset = 0;
		indexBufferOffset = 0;
		createVertexBuffer(context);
		createIndexBuffer(context);
	}

	void GeometryBuffers::createVertexBuffer(Context& context)
	{
		vk::DeviceSize bufferSize =  sizeof(Vertex) * MAX_VERTEX_COUNT;
		
		BufferHelper::createBufferUnique(bufferSize,
			vk::BufferUsageFlagBits::eTransferDst | vk::BufferUsageFlagBits::eVertexBuffer,
			vk::MemoryPropertyFlagBits::eDeviceLocal,
			vertexBuffer, vertexBufferMemory, context);
	}

	void GeometryBuffers::createIndexBuffer(Context& context)
	{
		vk::DeviceSize bufferSize = sizeof(uint32_t) * MAX_INDEX_COUNT;

		BufferHelper::createBufferUnique(bufferSize,
			vk::BufferUsageFlagBits::eTransferDst | vk::BufferUsageFlagBits::eIndexBuffer,
			vk::MemoryPropertyFlagBits::eDeviceLocal,
			indexBuffer, indexBufferMemory, context);
	}
	
	void GeometryBuffers::copyToVertexBuffer(
		Context& context, 
		std::vector<Vertex>* vertices, 
		vk::DeviceSize* vertexOffset,
		vk::CommandPool* commandPool)
	{
		std::lock_guard<std::mutex> lock(vertexMtx);
		//size of buffer needed to hold all vertices
		vk::DeviceSize bufferCapacity= sizeof(Vertex) * MAX_VERTEX_COUNT;
		vk::DeviceSize bufferSize = sizeof(Vertex) * vertices->size();

		//std::cout << "Capacity: " << bufferCapacity << ", trying to use " << vertexBufferOffset + bufferSize << std::endl;
		auto sizeAfterAlloc = vertexBufferOffset + bufferSize;
		if (sizeAfterAlloc > bufferCapacity)
		{
			std::cout << "No more room on vertex buffer (" << sizeAfterAlloc << " / " << bufferCapacity << ")" << std::endl;
			throw std::runtime_error("No more room on vertex buffer, please increase pool size");
		}

		// create temporary buffer to "stage" vertex data before transferring to GPU
		vk::UniqueBuffer stagingBuffer;
		vk::UniqueDeviceMemory stagingBufferMemory;
		// create staging buffer and allocate memory to it
		BufferHelper::createBufferUnique(bufferSize,
			vk::BufferUsageFlagBits::eTransferSrc,
			vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent,
			stagingBuffer, stagingBufferMemory, context);
		// MAP MEMORY TO VERTEX BUFFER
		auto data = context.device.mapMemory(stagingBufferMemory.get(), 0, bufferSize);
		memcpy(data, vertices->data(), (size_t)bufferSize);						// 3. Copy memory from vertices vector to point
		context.device.unmapMemory(stagingBufferMemory.get());					// 4. Unmap vertex buffer memory
		// copy staging buffer to vertex buffer on gpu
		//*vertexOffset = currentVertexOffset();
		*vertexOffset = currentVertexOffset() / sizeof(Vertex);
		BufferHelper::copyBufferWithDstOffset(stagingBuffer.get(),
			vertexBuffer.get(),
			bufferSize,
			context,
			currentVertexOffset(),
			commandPool);
		vertexBufferOffset += bufferSize;
	}

	void GeometryBuffers::copyToIndexBuffer(
		Context& context,
		std::vector<uint32_t>* indices,
		vk::DeviceSize* indexOffset,
		vk::CommandPool* commandPool)
	{
		std::lock_guard<std::mutex> lock(indexMtx);
		//size of buffer needed to hold all vertices
		vk::DeviceSize bufferSize = sizeof(uint32_t) * indices->size();
		vk::DeviceSize bufferCapacity = sizeof(uint32_t) * MAX_INDEX_COUNT;
		auto sizeAfterAlloc = indexBufferOffset + bufferSize;
		//std::cout << "Capacity: " << bufferCapacity << ", trying to use " << vertexBufferOffset + bufferSize << std::endl;
		if (sizeAfterAlloc  > bufferCapacity)
		{
			std::cout << "No more room on index buffer (" << sizeAfterAlloc << " / " << bufferCapacity << ")" << std::endl;
			throw std::runtime_error("No more room on index buffer, please increase pool size");
		}

		// create temporary buffer to "stage" vertex data before transferring to GPU
		vk::UniqueBuffer stagingBuffer;
		vk::UniqueDeviceMemory stagingBufferMemory;

		// create staging buffer and allocate memory to it
		BufferHelper::createBufferUnique(bufferSize,
			vk::BufferUsageFlagBits::eTransferSrc,
			vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent,
			stagingBuffer, stagingBufferMemory, context);

		// MAP MEMORY TO VERTEX BUFFER
		auto data = context.device.mapMemory(stagingBufferMemory.get(), 0, bufferSize);
		memcpy(data, indices->data(), (size_t)bufferSize);
		context.device.unmapMemory(stagingBufferMemory.get());

		// copy staging buffer to vertex buffer on gpu
		*indexOffset = currentIndexOffset();
		
		BufferHelper::copyBufferWithDstOffset(stagingBuffer.get(),
			indexBuffer.get(),
			bufferSize,
			context,
			currentIndexOffset(),
			commandPool);
		indexBufferOffset += bufferSize;
	}
}