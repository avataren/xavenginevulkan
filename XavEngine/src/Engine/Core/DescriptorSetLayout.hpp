#pragma once

#include "XavHeaders.hpp"
#include "Context.hpp"

namespace xav
{
	class DescriptorSetLayout
	{
	public:
		DescriptorSetLayout(Context& context);
		vk::DescriptorSetLayout& getDescriptorSetLayout() {
			return descriptorSetLayout.get();
		}

		vk::DescriptorSetLayout& getSamplerDescriptorSetLayout() {
			return samplerSetLayout.get();
		}

		vk::DescriptorSetLayout& getShaderUniformDescriptorSetLayout() {
			return shaderUniformSetLayout.get();
		}		
	
	private:
		vk::UniqueDescriptorSetLayout descriptorSetLayout; 		// per draw (view, projection, lights etc)
		vk::UniqueDescriptorSetLayout samplerSetLayout;			// per draw (samplers and texures)
		vk::UniqueDescriptorSetLayout shaderUniformSetLayout; 	// potentially per object (materials)
	};
}

