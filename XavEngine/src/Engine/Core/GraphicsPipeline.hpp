#pragma once

#include "Context.hpp"
#include "XavHeaders.hpp"
#include "SwapChain.hpp"
#include "DescriptorSetLayout.hpp"
#include "RenderPass.hpp"

namespace xav {

	class GraphicsPipeline
	{
	public:
		GraphicsPipeline(RenderPass* renderPass, DescriptorSetLayout* descriptorSet, SwapChain* swapChain, Context& context);
		//~GraphicsPipeline();

		vk::Pipeline getPipeline() { return graphicsPipeline.get(); }
		vk::PipelineLayout getPipelineLayout() { return pipelineLayout.get(); }

		//void init(vk::RenderPass renderPass, const DescriptorSetLayout& descriptorSet, const SwapChain& swapChain, Context& context);
	private:
		
		virtual void createPushConstantRange();
		virtual void createVertexBindings();
		virtual void createInputAssembly();
		virtual void createRasterizationState();
		virtual void createPipelineLayout(DescriptorSetLayout* descriptorSet, Context& context);
		virtual void createDepthStencilInfo();
		virtual void createBlendStates();
		virtual void createDynamicStates();
		virtual void createMultisampling(Context& context);
		virtual void createViewportAndScissor(SwapChain* swapChain);
		virtual void createShaderStages(Context& context);
		virtual void createGraphicsPipeline(RenderPass* renderPass, Context& context);

		vk::UniqueShaderModule vertexShaderModule;
		vk::UniqueShaderModule fragmentShaderModule;

		std::vector<vk::PushConstantRange>					pushConstantRanges;
		std::vector<vk::PipelineShaderStageCreateInfo> 		shaderStages;
		vk::PipelineRasterizationStateCreateInfo 			rasterizerCreateInfo;
		vk::PipelineColorBlendAttachmentState 				colorBlendState;
		vk::PipelineColorBlendStateCreateInfo 				colorBlendingCreateInfo;
		vk::PipelineMultisampleStateCreateInfo 				multisamplingCreateInfo;
		vk::VertexInputBindingDescription 					bindingDescription;
		vk::PipelineVertexInputStateCreateInfo 				vertexInputCreateInfo;
		vk::PipelineInputAssemblyStateCreateInfo 			inputAssembly;
		std::vector<vk::VertexInputAttributeDescription> 	attributeDescriptions;
		vk::UniquePipeline									graphicsPipeline;
		vk::UniquePipelineLayout							pipelineLayout;
		vk::PipelineViewportStateCreateInfo 				viewportStateCreateInfo;
		vk::PipelineDepthStencilStateCreateInfo 			depthStencilCreateInfo;
		vk::Rect2D 											scissor;
		vk::Viewport	 									viewport = {};

	};
}

