#pragma once
constexpr uint32_t MAX_LIGHTS = 1024;
constexpr uint32_t MAX_TEXTURES = 4096;
constexpr uint32_t MAX_OBJECTS = 1000;
#define MAX_FRAME_DRAWS = swapChain.getSwapChainSize()
#ifdef LOCAL_ASSETS
constexpr auto TEXTURE_FOLDER = "c:/src/data/Textures/";
#else
constexpr auto TEXTURE_FOLDER = "Textures/";
#endif