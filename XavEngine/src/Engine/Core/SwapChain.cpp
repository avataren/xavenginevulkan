#include <stack>
#include "SwapChain.hpp"
#include "XavHeaders.hpp"
#include <Engine/Helpers/ImageHelper.hpp>
namespace xav
{
	SwapChain::SwapChain(Context& _context) : context(&_context), imageCount(0)
	{
		swapChain = createSwapchain(_context);
		
		createSwapChainImages(_context);

		depthStencilImage = std::make_unique<FullImage>(FullImage::depthBufferImage(getExtent(), _context));

		//auto attachFormat = getSurfaceFormat();

		colorMultisampleImage = std::make_unique<FullImage>(FullImage(
			getExtent(),
			getSurfaceFormat(),
			vk::ImageTiling::eOptimal,
			vk::ImageUsageFlagBits::eTransientAttachment | vk::ImageUsageFlagBits::eColorAttachment,
			vk::MemoryPropertyFlagBits::eDeviceLocal,
			_context,
			vk::ImageAspectFlagBits::eColor,
			1,
			_context.getConfig().msaaSamples
		));
	}

	void SwapChain::createSwapChainImages(Context& context) {
		auto images = context.device.getSwapchainImagesKHR(swapChain.get());
		//swapChainImages = std::vector<std::unique_ptr<SwapChainImage>>();
		for (auto image : images) {
			// add to swap chain image list
			swapChainImages.push_back(std::make_unique<SwapChainImage>(SwapChainImage(image, getSurfaceFormat(), context)));
		}
	}

	vk::UniqueSwapchainKHR SwapChain::createSwapchain(Context& context, vk::SwapchainKHR oldSwapchain) {
		// Get Swap Chain details so we can pick the best settings
		auto swapChainDetails = getSwapChainDetails(context);
		// 1. Choose best surface format
		surfaceFormat = chooseBestSurfaceFormat(swapChainDetails.formats);
		// 2. Choose best presentation mode
		auto presentationMode = chooseBestPresentationMode(context);
		// 3. Choose swap chain image resolution
		extent = chooseSwapExtent(swapChainDetails.surfaceCapabilities, context);
		//// Determine how many images are in swap chain
		imageCount = swapChainDetails.surfaceCapabilities.minImageCount; // triple buffering
		if (presentationMode == vk::PresentModeKHR::eMailbox)
			imageCount += 4;

		minImageCount = swapChainDetails.surfaceCapabilities.minImageCount;
		// If maxImageCount is 0, there is no upper limit
		if (swapChainDetails.surfaceCapabilities.maxImageCount > 0)
			imageCount = std::min(imageCount, swapChainDetails.surfaceCapabilities.maxImageCount);

		vk::SwapchainCreateInfoKHR swapChainCreateInfo
		{
			vk::SwapchainCreateFlagsKHR(),							// Flags
			context.getSurface(),									// Surface
			imageCount,												// Minimum image count
			surfaceFormat.format,									// Image format
			surfaceFormat.colorSpace,								// Image color space
			extent,													// Image extent
			1,														// Image array layers
			vk::ImageUsageFlagBits::eColorAttachment,				// Image usage
			vk::SharingMode::eExclusive,							// Image sharing mode
			0,														// Queue family index count
			nullptr,												// Queue family indices
			swapChainDetails.surfaceCapabilities.currentTransform,	// Pre transform
			vk::CompositeAlphaFlagBitsKHR::eOpaque,					// Composite alpha
			presentationMode,										// Present mode
			VK_TRUE,												// Clipped
			nullptr 												// Old swapchain
		};
		
		// If Graphics and Presentation families are different, then swapchain must let images be shared between families
		if (context.hasDiscretePresentationQueue())
		{
			std::array<uint32_t, 2> queueIndices{
				context.getGraphicsQueueIndex(),
				context.getPresentationQueueIndex() };

			swapChainCreateInfo.imageSharingMode = vk::SharingMode::eConcurrent;
			swapChainCreateInfo.queueFamilyIndexCount = 2;
			swapChainCreateInfo.pQueueFamilyIndices = queueIndices.data();
		}
		//// If old swap chain being destroyed, and this one replaces it, then link old one to quickly hand over responsibilities
		//// Create Swap Chain

		return context.device.createSwapchainKHRUnique(swapChainCreateInfo);
	}

	vk::Extent2D SwapChain::chooseSwapExtent(const VkSurfaceCapabilitiesKHR& surfaceCapability, Context& context)
	{
		// If current extent is at numeric limits, then exent can vary. Otherwise, it is the size of the window.
		if (surfaceCapability.currentExtent.width != std::numeric_limits<uint32_t>::max()) {
			return surfaceCapability.currentExtent;
		}
		else
		{
			int width, height;
			glfwGetFramebufferSize(context.getWindow() , &width, &height);

			VkExtent2D newExtent = {};
			newExtent.width = static_cast<uint32_t>(width);
			newExtent.height = static_cast<uint32_t>(height);

			// Surface also defines max and min, so make sure withing boundries by clamping value
			newExtent.width = std::max(std::min(surfaceCapability.maxImageExtent.width, newExtent.width), surfaceCapability.minImageExtent.width);
			newExtent.height = std::max(std::min(surfaceCapability.maxImageExtent.height, newExtent.height), surfaceCapability.minImageExtent.height);

			return newExtent;
		}
	}
	/*
	void SwapChain::recreate(Context& context)
	{
		destroy();
		context.reCreateSurface();

		swapChain = createSwapchain(context);// , swapChain.get());
		swapChainImages = createSwapChainImages(context);
		depthStencilImage = ImageHelper::createDepthBufferImage(getExtent(), context);
		auto samples = context.getConfig().msaaSamples;
		colorMultisampleImage = ImageHelper::createImage2D(
			getExtent(),
			getSurfaceFormat(),
			vk::ImageTiling::eOptimal,
			vk::ImageUsageFlagBits::eTransientAttachment | vk::ImageUsageFlagBits::eColorAttachment,
			vk::MemoryPropertyFlagBits::eDeviceLocal,
			context,
			vk::ImageAspectFlagBits::eColor,
			1,
			samples
		);
	}
	*/
	SwapChainDetails SwapChain::getSwapChainDetails(Context& context)
	{
		SwapChainDetails swapChainDetails = {};
		// -- CAPABILITIES --
		// Get the surface capabilities for the given surface on the given physical device
		swapChainDetails.surfaceCapabilities = context.physicalDevice.getSurfaceCapabilitiesKHR(context.getSurface());
		// -- FORMATS --
		swapChainDetails.formats = context.physicalDevice.getSurfaceFormatsKHR(context.getSurface());
		// -- PRESENTATION MODS --
		swapChainDetails.presentationModes = context.physicalDevice.getSurfacePresentModesKHR(context.getSurface());
		return swapChainDetails;
	}

	// Best format is subjective, but ours will be :
	// format		:	VK_FORMAT_R8G8B8A8_UNORM
	// colorSpace	:	VK_COLOR_SPACE_SRGB_NONLINEAR_KHR
	vk::SurfaceFormatKHR SwapChain::chooseBestSurfaceFormat(const std::vector<vk::SurfaceFormatKHR>& formats)
	{
		// If only 1 format is available and is undefined, this means ALL formats are available
		/*if (formats.size() == 1 && formats[0].format == vk::Format::eUndefined)
		{
			return { vk::Format::eR8G8B8A8Srgb, vk::ColorSpaceKHR::eSrgbNonlinear };
		}*/
		for (const auto& format : formats) {
			if (format.format == vk::Format::eB8G8R8A8Srgb && format.colorSpace == vk::ColorSpaceKHR::eSrgbNonlinear)
			{
				return format;
			}

			if (format.format == vk::Format::eR8G8B8A8Srgb && format.colorSpace == vk::ColorSpaceKHR::eSrgbNonlinear)
			{
				return format;
			}
		}

		return formats[0];
	}

	vk::PresentModeKHR SwapChain::chooseBestPresentationMode(Context& context)
	{
		static const std::string logTag{ "ast::VulkanSwapchain::getPresentationMode" };

		// We need to make sure that there is at least one presentation mode compatible with our surface.
		std::vector<vk::PresentModeKHR> availableModes{ context.physicalDevice.getSurfacePresentModesKHR(context.getSurface()) };

		if (availableModes.empty())
		{
			throw std::runtime_error(logTag + ": No compatible present modes found.");
		}

		// Load up the presentation modes into a stack so they are popped
		// in our preferred order for evaluation.
		std::stack<vk::PresentModeKHR> preferredModes;
		preferredModes.push(vk::PresentModeKHR::eImmediate);
		preferredModes.push(vk::PresentModeKHR::eFifoRelaxed);
		preferredModes.push(vk::PresentModeKHR::eFifo);
		preferredModes.push(vk::PresentModeKHR::eMailbox);
#ifdef _DEBUG
		std::stack<std::string> names;
		names.push("Immediate");
		names.push("FifoRelaxed");
		names.push("Fifo");
		names.push("Mailbox");
#endif
		while (!preferredModes.empty())
		{
			// Take the mode at the top of the stack and see if the list of available modes contains it.
			vk::PresentModeKHR mode{ preferredModes.top() };
			
			if (std::find(availableModes.begin(), availableModes.end(), mode) != availableModes.end())
			{
				// If we find the current preferred presentation mode, we are done.
#ifdef _DEBUG
				std::cout << "Using presentation mode: " << names.top() << std::endl;
#endif
				return mode;
			}

			// If our preferred mode is not found, pop the stack ready for the next iteration.
			preferredModes.pop();

#ifdef _DEBUG
			names.pop();
#endif
		}

		// None of our preferred presentation modes were found, can't go further...
		throw std::runtime_error(logTag + ": No compatible presentation modes found.");
	}
}