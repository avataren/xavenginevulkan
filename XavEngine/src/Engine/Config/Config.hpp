#pragma once
#include <Engine/Core/XavHeaders.hpp>

namespace xav
{
	class Config {
	public:
		Config();
		~Config();

		void saveConfig();
		void loadConfig();

		vk::SampleCountFlagBits msaaSamples = vk::SampleCountFlagBits::e1;
		vk::Bool32 enableSampleShading = VK_FALSE;
		std::string runtimePath = "";
		float maxAnisotropy = 1.0;
		float minSampleShading = 0.0f;
		bool autoGenerateMipMaps = true;
		bool fullScreen = false;
		int windowWidth = 1280;
		int windowHeight = 720;
		int windowPosX = 0;
		int windowPosY = 0;
	private:
	 	
		static inline const std::string configFile = "settings.cfg";
	};
}
