#include "Config.hpp"
#include "ini.h"
#include <string>
#include <iostream>

namespace xav {

    Config::Config()
    {
        loadConfig();
    }

    Config::~Config()
    {
        saveConfig();
    }

    void Config::saveConfig()
    {
        // create a file instance
        mINI::INIFile file(configFile);
        // create a data structure
        mINI::INIStructure ini;
        // populate the structure
        ini["Graphics"]["msaaSamples"] = std::to_string((uint32_t)msaaSamples);
        ini["Graphics"]["enableSampleShading"] = std::to_string((uint32_t)enableSampleShading);
        ini["Graphics"]["maxAnisotropy"] = std::to_string(maxAnisotropy);
        ini["Graphics"]["minSampleShading"] = std::to_string(minSampleShading);
        ini["Graphics"]["autoGenerateMipMaps"] = std::to_string(autoGenerateMipMaps);
        ini["Graphics"]["fullScreen"] = std::to_string(fullScreen);
        // generate an INI file (overwrites any previous file)
        if (!file.write(ini, true))
        {
            std::cerr << "Unable to write config file" << std::endl;
        }
    }

    void Config::loadConfig()
    {
        mINI::INIFile file(configFile);
        // // next, create a structure that will hold data
        mINI::INIStructure ini;
        // // now we can read the file
        if (file.read(ini))
        {
            try
            {
                msaaSamples = static_cast<vk::SampleCountFlagBits> (std::stoul(ini["Graphics"]["msaaSamples"]));
                enableSampleShading = static_cast<vk::Bool32> (std::stoul(ini["Graphics"]["enableSampleShading"]));
                maxAnisotropy = std::stof(ini["Graphics"]["maxAnisotropy"]);
                minSampleShading = std::stof(ini["Graphics"]["minSampleShading"]);
                autoGenerateMipMaps = std::stoi(ini["Graphics"]["autoGenerateMipMaps"]) != 0;
                fullScreen = std::stoi(ini["Graphics"]["fullScreen"]) != 0;
            }
            catch (...)
            {
                std::cerr << "Failed to parse config file" << std::endl;
            }
        }
    }

}