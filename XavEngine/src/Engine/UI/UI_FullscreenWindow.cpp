#include "UI_FullscreenWindow.hpp"
#include "imgui/imgui.h"
#include "imgui/implot.h"
#include "imgui/imgui_internal.h"
#include <OS_Integration/WindowManager.hpp>
#include "UI_TopMenu.hpp"

namespace xav::ui
{
    UI_FullscreenWindow::UI_FullscreenWindow(Context& context,UI_State& state) 
    {
        bool open = true;
        //ImGui::SetNextWindowPos(ImVec2(0,32));
        //auto width = WindowManager::getInstance()->width;
        //auto height = WindowManager::getInstance()->height;
        //std::cout << width << "x" << height << std::endl;
        //ImGui::SetNextWindowSize(ImVec2(width/8, height));
        //if (ImGui::Begin("Fullscreen_Window", &open, ImGuiWindowFlags_NoDecoration))// | ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoBackground);
		//{
            //UI_TopMenu menu(context, state);
            //ImGui::End();
        //}
        ImGuiWindowFlags window_flags = 0;//ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoDocking;
        static ImGuiDockNodeFlags dockspace_flags = ImGuiDockNodeFlags_PassthruCentralNode;//ImGuiDockNodeFlags_NoDockingInCentralNode;//  | ;
        const ImGuiViewport* viewport = ImGui::GetMainViewport();
        ImGui::SetNextWindowPos(viewport->WorkPos);
        ImGui::SetNextWindowSize(viewport->WorkSize);
        ImGui::SetNextWindowViewport(viewport->ID);
        //ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
        //ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
        window_flags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove;
        window_flags |= ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;
        window_flags |= ImGuiWindowFlags_NoBackground;
        ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
        ImGui::Begin("DockSpace", &open, window_flags);

        // DockSpace
        ImGuiIO& io = ImGui::GetIO();
        if (io.ConfigFlags & ImGuiConfigFlags_DockingEnable)
        {
            ImGuiID dockspace_id = ImGui::GetID("MyDockSpace");
            ImGui::DockSpace(dockspace_id, ImVec2(0.0f, 0.0f), dockspace_flags);
        }
       
        ImGui::End();
        ImGui::PopStyleVar();

    }
}