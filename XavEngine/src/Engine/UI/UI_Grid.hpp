#pragma once
#include <Engine/Scene/SceneComponents.hpp>
#include <Engine/Scene/Entity.hpp>
#include <Engine/Scene/Scene.hpp>
#include <Engine/Scene/SceneComponents.hpp>
#include <Engine/Core/GeometryBuffers.hpp>
#include <Engine/ResourceHandling/TextureManager.hpp>
#include <Engine/Core/TextureSampler.hpp>

namespace xav::ui{

    class UI_Grid{
        public:
        UI_Grid(Context& context, GeometryBuffers* geometryBuffers,TextureSampler* sampler, Scene* scene, TextureManager* textureManager, vk::CommandPool* commandPool);
        private:
        Entity ent;
    };
    
}