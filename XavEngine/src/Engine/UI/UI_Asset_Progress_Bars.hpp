#pragma once
#include "Engine/Renderer/Renderer.hpp"

namespace xav::ui
{

	class UI_Asset_Progress_Bars
	{
	public:
		UI_Asset_Progress_Bars(Renderer& renderer, bool* open);
	private:
		bool BufferingBar(const char* label, float value, const ImVec2& size_arg, const ImU32& bg_col, const ImU32& fg_col);
	};

}

