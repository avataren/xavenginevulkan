#include "UI_Gizmos.hpp"
#include <imgui/imgui.h>
#include <imgui/ImGuizmo/ImGuizmo.h>
#include <Engine/Scene/SceneComponents.hpp>

namespace xav::ui
{
    UI_Gizmos::UI_Gizmos(Camera& camera, UI_State& uiState, bool* open)
    {
        ImGuiIO& io = ImGui::GetIO();
        float viewManipulateRight = io.DisplaySize.x;
        float viewManipulateTop = 0;
        ImGuizmo::SetRect(0, 0, io.DisplaySize.x, io.DisplaySize.y);
        auto gridTransform = glm::mat4(1.f);

        auto viewMatrix = (glm::mat4(camera.getViewMatrix()));
        auto projMatrix = glm::mat4(camera.getProjectionMatrix());
        projMatrix[1][1] *= -1.f;

        auto viewPtr = (float*)glm::value_ptr(viewMatrix);
        auto projtr = (float*)glm::value_ptr(projMatrix);

        auto gridTransformPtr = (float*)glm::value_ptr(gridTransform);

        if (!uiState.selectedEntity.isNull() && uiState.selectedEntity.hasComponent<TransformComponent>())
        {
            auto& transformComponent = uiState.selectedEntity.getComponent<TransformComponent>();
            auto transform = transformComponent.getTransform();
            auto transPtr = (float*)glm::value_ptr(transform);
            if (ImGuizmo::Manipulate(viewPtr, projtr,  uiState.currentGizmoOperation, uiState.currentGizmoMode, transPtr, NULL, NULL))//useSnap ? &snap.x : NULL);)
            {
                 transformComponent.setTransform(transform);
            }
        }

        ImGuizmo::ViewManipulate(viewPtr, 5.f, ImVec2(viewManipulateRight - 128, viewManipulateTop), ImVec2(128, 128), 0x10101040);
        //ImGuizmo::DrawGrid(viewPtr, projtr, gridTransformPtr, 100.f);

        projMatrix[1][1] *= -1.f;
        camera.setProjectionMatrix(projMatrix);
        camera.setViewMatrix((viewMatrix));
    }
}