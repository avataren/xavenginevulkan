#pragma once

#include <imgui/imgui.h>

namespace xav::ui
{
    class UI_Log {
    public:

    UI_Log()
    {
        AutoScroll = true;
        Clear();
    }

    void Clear();
    void AddLog(const char* fmt, ...) IM_FMTARGS(2);
    void Draw(const char* title, bool* p_open = NULL);
    private:
        ImGuiTextBuffer     Buf;
        ImGuiTextFilter     Filter;
        ImVector<int>       LineOffsets; // Index to lines offset. We maintain this with AddLog() calls.
        bool                AutoScroll;  // Keep scrolling if already at the bottom.
    };
}