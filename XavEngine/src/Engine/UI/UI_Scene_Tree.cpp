#include "UI_Scene_Tree.hpp"
#include <Engine/Scene/SceneComponents.hpp>
#include <imgui/ImGuizmo/ImGuizmo.h>
namespace xav::ui
{

    UI_Scene_Tree::UI_Scene_Tree(Scene& scene, UI_State& uiState, bool* open)
    {
        if (!*open)
            return;

            // try
            // {
                auto flags = 0;//ImGuiWindowFlags_NoResize;
                ImGui::Begin("SceneView", open,flags);
                ImGui::LabelText("", "Scene Objects");
                {
                    auto currentNode = scene.Root();
                    if (currentNode.hasComponent<HierarchyParentComponent>())
                    {
                        //rare random crash here?
                        currentNode = currentNode.getComponent<HierarchyParentComponent>().firstChild;
                        iterateNode(currentNode, scene, uiState);
                    }
                }

                ImGui::End();        
                /* code */
            // }
            // catch(const std::exception& e)
            // {
            //     std::cerr << e.what() << '\n';
            // }

    }

    void UI_Scene_Tree::iterateNode(Entity& currentNode, Scene& scene, UI_State& uiState)
    {
        if (currentNode == Entity::null())
            return;
        auto& registry = scene.GetRegistry();
        bool hasChildren = currentNode.hasComponent<HierarchyParentComponent>();

        ImGuiTreeNodeFlags node_flags = hasChildren ? ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick
            : ImGuiTreeNodeFlags_Leaf;
        node_flags |= uiState.selectedEntity == currentNode ? ImGuiTreeNodeFlags_Selected : 0;

        auto& name = currentNode.getComponent<NameComponent>().name;
        auto opened = ImGui::TreeNodeEx(name.c_str(), node_flags);

        if (ImGui::IsItemClicked())
        {
            uiState.selectedEntity = currentNode;   //select item when clicked
        }
        else if (ImGui::IsWindowHovered() &&
        (node_flags&ImGuiTreeNodeFlags_Selected && !ImGui::IsItemHovered() && ImGui::IsMouseClicked(0)))
        {
            if (!(ImGui::GetMouseCursor()==ImGuiMouseCursor_ResizeEW || ImGui::GetMouseCursor()==ImGuiMouseCursor_ResizeNESW || ImGui::GetMouseCursor()==ImGuiMouseCursor_ResizeNS || ImGui::GetMouseCursor()==ImGuiMouseCursor_ResizeNWSE))
            {
                uiState.selectedEntity = entt::null; //deselect item if user clicks in blank area on window tree
            }
        }

        if (opened)
        {
            if (hasChildren)
            {
                auto& nextNode = currentNode.getComponent<HierarchyParentComponent>().firstChild;
                iterateNode(nextNode, scene, uiState);
            }
            ImGui::TreePop();
        }

        if (currentNode.hasComponent<HierarchyComponent>())
        {
            auto& hierarchy = currentNode.getComponent<HierarchyComponent>();
            iterateNode(hierarchy.next, scene, uiState);
        }
    }

} // namespace xav