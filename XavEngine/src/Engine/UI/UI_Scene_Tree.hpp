#pragma once
#include <imgui/imgui.h>
#include <Engine/Scene/Scene.hpp>
#include "UI_State.hpp"

namespace xav::ui
{
    class UI_Scene_Tree
    {
    public:
        UI_Scene_Tree(Scene& scene, UI_State& uiState, bool* open);

    private:
        void iterateNode(Entity& currentNode, Scene& scene, UI_State& uiState);
        
    };

} // namespace xav