#ifdef _WIN32
#include <Windows.h>
#else
#include <unistd.h>
#endif

#include "UI_PerformanceMonitor.hpp"
#include "imgui/imgui.h"
#include "imgui/implot.h"
#include "imgui/imgui_internal.h"


std::string string_format(const std::string fmt, ...) {
	int size = ((int)fmt.size()) * 2 + 50;   // Use a rubric appropriate for your code
	std::string str;
	va_list ap;
	while (1) {     // Maximum two passes on a POSIX system...
		str.resize(size);
		va_start(ap, fmt);
		int n = vsnprintf((char*)str.data(), size, fmt.c_str(), ap);
		va_end(ap);
		if (n > -1 && n < size) {  // Everything worked
			str.resize(n);
			return str;
		}
		if (n > -1)  // Needed size returned
			size = n + 1;   // For null char
		else
			size *= 2;      // Guess at a larger size (OS specific)
	}
	return str;
}



namespace xav::ui
{
	ScrollingBuffer UI_PerformanceMonitor::timeData{};
	static float t = 0;

	UI_PerformanceMonitor::UI_PerformanceMonitor(Renderer& renderer, Context& context, bool* open)
	{
		if (!*open)
			return;
		ImGui::Begin("Performance monitor", open, ImGuiWindowFlags_NoResize);
		{
			
			float deltaTime = ImGui::GetIO().DeltaTime;
			t += deltaTime;


			timeData.AddPoint(t, deltaTime * scale);

			ImGui::SetWindowSize(ImVec2(420, 275));
			ImGui::LabelText("##fpsLabel", "%.0f FPS, %.3f ms pr frame", ImGui::GetIO().Framerate, deltaTime * 1000.f);
			ImGui::SliderFloat("Scale", &scale, 1.0f, 100.0f,"%.0f");
			ImGui::SliderFloat("History", &history, 0.01f, 5.0f);
			 
			static ImPlotAxisFlags rt_axis = ImPlotAxisFlags_NoTickLabels;
			ImPlot::SetNextPlotLimitsY(0, 1, ImGuiCond_Once, 1);
			ImPlot::SetNextPlotLimitsX(t - history, t, ImGuiCond_Always);
			
			if (ImPlot::BeginPlot("##frameplot", NULL, NULL, ImVec2(-1, 120), 0, rt_axis, rt_axis | ImPlotAxisFlags_LockMin)) {
				ImPlot::PlotShaded("", &timeData.Data[0].x, &timeData.Data[0].y, timeData.Data.size(), 0, timeData.Offset, 2 * sizeof(float));
				ImPlot::EndPlot();
			}
			/*
			const ImU32 col = ImGui::GetColorU32(ImGuiCol_PlotHistogram);
			const ImU32 bg = ImGui::GetColorU32(ImGuiCol_Button);
			PerformanceBars("frameHistory", 1, ImVec2(400, 50), bg, col);*/

		}
		
		ImGui::End();
	}

	bool UI_PerformanceMonitor::PerformanceBars(const char* label, float value, const ImVec2& size_arg, const ImU32& bg_col, const ImU32& fg_col) {
		auto window = ImGui::GetCurrentWindow();
		if (window->SkipItems)
			return false;

		ImGuiContext& g = *ImGui::GetCurrentContext();
		const ImGuiStyle& style = g.Style;
		const ImGuiID id = window->GetID(label);

		ImVec2 pos = window->DC.CursorPos;
		ImVec2 size = size_arg;
		size.x -= style.FramePadding.x * 2;

		ImRect bb(pos, ImVec2(pos.x + size.x, pos.y + size.y));
		ImGui::ItemSize(bb, style.FramePadding.y);
		if (!ImGui::ItemAdd(bb, id))
			return false;

		// Render
		float barWidth = (size.x / (float)timeData.bufferSize);

		//window->DrawList->AddRectFilled(pos,ImVec2( pos.x+size.x, pos.y+size.y), bg_col);
		float xPosOffset = size.x;
		auto dataSize = timeData.Data.size();
		for (int i = 0; i < dataSize; i++)
		{
			float h = timeData.Data[i].y * scale / size.y;
			ImVec2 startRect = ImVec2(pos.x + xPosOffset, pos.y + size.y);
			ImVec2 endRect = ImVec2(startRect.x + barWidth * 0.7f, startRect.y - size.y * h);
			window->DrawList->AddRectFilled(startRect, endRect, fg_col);
			xPosOffset -= barWidth;
		}
		/*
		for (int i = timeData.Offset; i < timeData.Data.size(); i++)
		{
			float h = timeData.Data[i- timeData.Offset].y * scale / size.y;
			ImVec2 startRect = ImVec2(pos.x + xPosOffset, pos.y + size.y);
			ImVec2 endRect = ImVec2(startRect.x + barWidth * 0.7f, startRect.y - size.y * h);
			window->DrawList->AddRectFilled(startRect, endRect, fg_col);
			xPosOffset += barWidth;
		}
		*/
		#ifdef _WIN32
		Sleep(1000 / 50);
		#else
		sleep(1000 / 50);
		#endif
		
		return true;
	}
}
