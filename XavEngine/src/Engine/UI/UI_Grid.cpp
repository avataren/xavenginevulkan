#include "UI_Grid.hpp"
#include <Engine/Helpers/MeshHelper.hpp>
#include <Engine/Geometry/Vertex.hpp>
#include <vector>

namespace xav::ui
{
    UI_Grid::UI_Grid(Context& context, GeometryBuffers* geometryBuffers,TextureSampler* sampler, Scene* scene, TextureManager* textureManager, vk::CommandPool* commandPool) 
    {
        ent = scene->CreateEntity("Grid");

        std::vector<Vertex> vertices = {
            {glm::vec3(-0.5f,0.0f,-0.5f),   glm::vec3(1.0f,1.0f,1.0f), glm::vec2(0.0f,1000.0f)},
            {glm::vec3(0.5f,0.0f,-0.5f),    glm::vec3(1.0f,1.0f,1.0f), glm::vec2(1000.0f,1000.0f)},
            {glm::vec3(0.5f,0.0f,0.5f),     glm::vec3(1.0f,1.0f,1.0f), glm::vec2(1000.0f,0.0f)},
            {glm::vec3(-0.5f,0.0f,0.5f),    glm::vec3(1.0f,1.0f,1.0f), glm::vec2(0.0f,0.0f)}
        };
        std::vector<uint32_t> indices = {0,1,3,1,2,3, 3,2,1,3,1,0};

        auto gridTextureId = textureManager->loadTexture("grid2.png",  commandPool);

		GeometryGPUComponent geometryComponent;
		
		geometryBuffers->copyToVertexBuffer(context, &vertices, &geometryComponent.vertexBufferOffset, commandPool);
		geometryBuffers->copyToIndexBuffer(context, &indices, &geometryComponent.indexBufferOffset, commandPool);
		geometryComponent.vertexCount = vertices.size();
		geometryComponent.indexCount = indices.size();
		MaterialComponent 	materialComponent;
		materialComponent.textureDescriptorIndex = gridTextureId;

		ent.addComponent(geometryComponent);
		ent.addComponent(materialComponent);
        auto transformMatrix = glm::scale(glm::mat4(1.0f), glm::vec3(1000.0f));
        ent.addComponent<TransformComponent>(transformMatrix);
        ent.addComponent<TransparentComponent>(TransparentComponent());
        scene->AddChild(scene->Root(),ent);
    }
}