#include "UI_XavUI.hpp"

namespace xav::ui
{
	UI_XavUI::UI_XavUI(Renderer& renderer, Context& context) :
		gizmos(renderer.camera, uiState, &alwaysOpen),
		menu(context, uiState),
		dockspace(context, uiState),
		settings(renderer, context, &uiState.settingsOpen),
		perfMon(renderer, context, &uiState.showPerfMon),
		loadingBar(renderer, &alwaysOpen),
		sceneTree(renderer.getScene(), uiState, &uiState.showSceneTree),
		propertiesPanel(renderer.getScene(), uiState, &uiState.showPropertyPanel)
	{

	}
	UI_XavUI::~UI_XavUI()
	{
		
	}
}