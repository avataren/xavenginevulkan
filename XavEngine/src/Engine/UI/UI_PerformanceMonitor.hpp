#pragma once
#include "Engine/Core/Context.hpp"
#include "Engine/Renderer/Renderer.hpp"

namespace xav::ui
{
    struct ScrollingBuffer {
        int MaxSize;
        int Offset;
        ImVector<ImVec2> Data;
        const int bufferSize = 2000;
        ScrollingBuffer() {
            MaxSize = bufferSize;
            Offset = 0;
            Data.reserve(MaxSize);
        }
        void AddPoint(float x, float y) {
            if (Data.size() < MaxSize)
                Data.push_back(ImVec2(x, y));
            else {
                Data[Offset] = ImVec2(x, y);
                Offset = (Offset + 1) % MaxSize;
            }
        }
        void Erase() {
            if (Data.size() > 0) {
                Data.shrink(0);
                Offset = 0;
            }
        }
    };


    // utility structure for realtime plot
    struct RollingBuffer {
        float Span;
        ImVector<ImVec2> Data;
        RollingBuffer() {
            Span = 10.0f;
            Data.reserve(2000);
        }
        void AddPoint(float x, float y) {
            float xmod = fmodf(x, Span);
            if (!Data.empty() && xmod < Data.back().x)
                Data.shrink(0);
            Data.push_back(ImVec2(xmod, y));
        }
    };


	class UI_PerformanceMonitor
	{
	public:
		UI_PerformanceMonitor(Renderer& renderer, Context& context, bool* open);
    private:
        bool PerformanceBars(const char* label, float value, const ImVec2& size_arg, const ImU32& bg_col, const ImU32& fg_col);
        //static RollingBuffer timeDataRolling;
        static ScrollingBuffer timeData;
        static inline float history = 0.5f;
        static inline float scale = 10.f;
        static inline float fps = 0.0f;
        static inline float startTime = 0.0f;
        
	};

}