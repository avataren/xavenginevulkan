#pragma once
#include "UI_State.hpp"
#include <Engine/Scene/Scene.hpp>

namespace xav::ui{
    class UI_Properties_Panel
    {
        public:
        UI_Properties_Panel(Scene& scene,UI_State& uiState, bool* open);

        private:
    };
}