#include "UI_TopMenu.hpp"
#include "imgui/imgui.h"
#include "imgui/imgui_internal.h"
#include <GLFW/glfw3.h>

namespace xav::ui {
	UI_TopMenu::UI_TopMenu(Context& context,UI_State& state)
	{
		if (state.showDemo)
			ImGui::ShowDemoWindow(&state.showDemo);
		//if (state.showMetrix)
			//ImGui::ShowMetricsWindow(&state.showMetrix);

		if (ImGui::BeginMainMenuBar())
		{

			if (ImGui::BeginMenu("File"))
			{
				if (ImGui::MenuItem("Exit"))
				{
					glfwSetWindowShouldClose(context.getWindow(), GLFW_TRUE);
				}
				ImGui::EndMenu();
			}
			
			if (ImGui::BeginMenu("Preferences"))
			{
				if (ImGui::MenuItem("Settings"))
				{
					state.settingsOpen = true;
				}
				if (state.showPerfMon)
				{
					if (ImGui::MenuItem("Hide Performance monitor"))
					{
						state.showPerfMon = false;
					}
				}
				else
					if (ImGui::MenuItem("Show Performance monitor"))
					{
						state.showPerfMon = true;
					}
				if (state.showDemo)
				{
					if (ImGui::MenuItem("Hide ImGui demo"))
					{
						state.showDemo = !state.showDemo;
					}
				}
				else
				if (ImGui::MenuItem("Show ImGui demo"))
				{
					state.showDemo = !state.showDemo;
				}
				ImGui::EndMenu();
			}
			
			ImGui::EndMainMenuBar();	
		}
	}
	
	UI_TopMenu::~UI_TopMenu()
	{
	}
}