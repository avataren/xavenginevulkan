#pragma once
#include <Engine/Core/Context.hpp>
#include <Engine/Renderer/Renderer.hpp>
#include <imgui/imgui.h>

namespace xav::ui
{
	class UI_Settings
	{
	public:
		UI_Settings(Renderer& renderer, Context& context, bool* open);
	private:
		static inline const char* current_sampling = nullptr;
		static inline const char* current_aniso = nullptr;
	};
}

