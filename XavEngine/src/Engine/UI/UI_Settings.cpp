#include "UI_Settings.hpp"
#include "imgui/imgui.h"
#include "imgui/imgui_internal.h"
#include <vector>

namespace xav::ui {
	UI_Settings::UI_Settings(Renderer& renderer, Context& context, bool* open)
	{
		if (!*open)
			return;
		
		ImGui::Begin("Settings", open);
		
		const char* samplingItems[] = { "1x", "2x", "4x", "8x"};
		std::vector < vk::SampleCountFlagBits > samples = {
		vk::SampleCountFlagBits::e1,
		vk::SampleCountFlagBits::e2,
		vk::SampleCountFlagBits::e4,
		vk::SampleCountFlagBits::e8
		};
		
		auto it = find(samples.begin(), samples.end(), context.getConfig().msaaSamples);
		int index =static_cast<int>( it - samples.begin() );
		if (current_sampling == nullptr)
			current_sampling = samplingItems[index];


		const char* anisoItems[] = { "1x", "2x", "4x", "8x", "16x" };
		std::vector < float > samplesAniso = {
			1,2,4,8,16
		};
		
		auto it2 = find(samplesAniso.begin(), samplesAniso.end(), context.getConfig().maxAnisotropy);
		int index2 = static_cast<int>( it2 - samplesAniso.begin() );
		if (current_aniso == nullptr)
			current_aniso = anisoItems[index2];


		ImGui::PushItemWidth(100.f);
		if (ImGui::BeginCombo("MultiSampling", current_sampling)) // The second parameter is the label previewed before opening the combo.
		{
			for (int n = 0; n < IM_ARRAYSIZE(samplingItems); n++)
			{
				bool is_selected = (current_sampling == samplingItems[n]); // You can store your selection however you want, outside or inside your objects
				if (ImGui::Selectable(samplingItems[n], is_selected))
					current_sampling = samplingItems[n];
				if (is_selected)
				{
					ImGui::SetItemDefaultFocus();   // You may set the initial focus when opening the combo (scrolling + for keyboard navigation support)
				}
			}
			ImGui::EndCombo();
		}

		bool msaaDisabled = true;
		if (msaaDisabled)
		{
			ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
			ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
		}
		if (ImGui::BeginCombo("Anisotropy", current_aniso)) // The second parameter is the label previewed before opening the combo.
		{
			for (int n = 0; n < IM_ARRAYSIZE(anisoItems); n++)
			{
				bool is_selected = (current_aniso == anisoItems[n]); // You can store your selection however you want, outside or inside your objects
				if (ImGui::Selectable(anisoItems[n], is_selected))
					current_aniso = anisoItems[n];
				if (is_selected)
				{
					ImGui::SetItemDefaultFocus();   // You may set the initial focus when opening the combo (scrolling + for keyboard navigation support)
				}
			}
			ImGui::EndCombo();
		}
		if (msaaDisabled)
		{
			ImGui::PopStyleVar();
			ImGui::PopItemFlag();
		}
		ImGui::PopItemWidth();

		if (ImGui::Checkbox("Fullscreen", &context.getConfig().fullScreen))
		{

		}

		if(ImGui::Button("Apply")) {
			bool shouldRecreate = false;
			vk::SampleCountFlagBits aasamples = context.getConfig().msaaSamples;
			float aniso = context.getConfig().maxAnisotropy;
			for (int i = 0; i < IM_ARRAYSIZE(samplingItems); i++)
			{
				if (current_sampling == samplingItems[i])
				{
					aasamples = samples[i];
				}
			}

			for (int i = 0; i < IM_ARRAYSIZE(anisoItems); i++)
			{
				if (current_aniso == anisoItems[i])
				{
					aniso = samplesAniso[i];
				}
			}
			bool isFullscreen = glfwGetWindowMonitor(context.getWindow()) != nullptr;

			if (!isFullscreen && context.getConfig().fullScreen)
			{
				GLFWmonitor* monitor = glfwGetPrimaryMonitor();
				const GLFWvidmode* mode = glfwGetVideoMode(monitor);

				// backup window position and window size
				glfwGetWindowPos(context.getWindow(), &context.getConfig().windowPosX, &context.getConfig().windowPosY);
				glfwGetWindowSize(context.getWindow(), &context.getConfig().windowWidth, &context.getConfig().windowHeight);
				glfwSetWindowMonitor(context.getWindow(), monitor, 0, 0, mode->width, mode->height, mode->refreshRate);
				shouldRecreate = true;
			}
			else if (isFullscreen && !context.getConfig().fullScreen)
			{
				glfwSetWindowMonitor(context.getWindow(), nullptr, 
					context.getConfig().windowPosX, 
					context.getConfig().windowPosY, 
					context.getConfig().windowWidth,
					context.getConfig().windowHeight,0);
				shouldRecreate = true;
			}
			if (context.getConfig().msaaSamples != aasamples)
				shouldRecreate = true;
			if (context.getConfig().maxAnisotropy != aniso)
				shouldRecreate = true;

			context.getConfig().msaaSamples = aasamples;
			context.getConfig().maxAnisotropy = aniso;

			if (shouldRecreate)
				renderer.recreate();
		};
		
		ImGui::End();
	}

}