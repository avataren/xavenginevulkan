#pragma once
#include "Engine/Core/Context.hpp"
#include "Engine/Renderer/Renderer.hpp"
#include "UI_Settings.hpp"
#include "UI_TopMenu.hpp"
#include "UI_State.hpp"
#include "UI_PerformanceMonitor.hpp"
#include "UI_Asset_Progress_Bars.hpp"
#include "UI_Scene_Tree.hpp"
#include "UI_FullscreenWindow.hpp"
#include "UI_Properties_Panel.hpp"
#include "UI_Gizmos.hpp"

namespace xav::ui
{

	class UI_XavUI
	{
	public:
		UI_XavUI(Renderer& renderer, Context& context);
		~UI_XavUI();
		UI_Gizmos gizmos;
		UI_TopMenu menu;
		UI_FullscreenWindow dockspace;
		UI_Settings settings;
		UI_PerformanceMonitor perfMon;
		UI_Asset_Progress_Bars loadingBar;
		UI_Scene_Tree sceneTree;
		UI_Properties_Panel propertiesPanel;
	private:
		static inline UI_State uiState = UI_State();
		static inline bool alwaysOpen = true;
	};
}
