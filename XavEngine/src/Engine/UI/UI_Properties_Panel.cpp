#include "UI_Properties_Panel.hpp"
#include <imgui/imgui.h>
#include <imgui/ImGuizmo/ImGuizmo.h>
#include <Engine/Scene/SceneComponents.hpp>
#include "UI_Helpers.hpp"

namespace xav::ui
{
    UI_Properties_Panel::UI_Properties_Panel(Scene& scene, UI_State& uiState, bool* open)
    {
        if (!*open)
            return;

        ImGui::Begin("Properties", open);
        
        // uiState.propertiesHovered = false;

        if (!uiState.selectedEntity.isNull())
        {
            //uiState.propertiesHovered = ImGui::IsWindowHovered();//ImGui::IsMouseHoveringRect(ImGui::GetWindowPos(),ImVec2(ImGui::GetWindowWidth(), ImGui::GetWindowHeight()));

            auto& entity = uiState.selectedEntity;
            if (entity.hasComponent<NameComponent>())
            {
                std::string& name = entity.getComponent<NameComponent>().name;
                char buf[256];
                memset(buf, 0, sizeof(buf));
                if (name.size() >= sizeof(buf))
                {
                    name.resize(sizeof(buf));
                }
                memcpy(buf, name.data(), name.size());
                ImGui::Text("Name:");

                ImGui::SameLine();
                ImGui::InputText("##EntityName", buf, sizeof(buf));

                entity.getComponent<NameComponent>().name = buf;

                bool isHidden = entity.hasComponent<HiddenComponent>();
                ImGui::Checkbox("Hidden", &isHidden);
                if (isHidden)
                {
                    if (!entity.hasComponent<HiddenComponent>())
                        entity.addComponent<HiddenComponent>(HiddenComponent());
                }
                else
                {
                    if (entity.hasComponent<HiddenComponent>())
                        entity.removeComponent<HiddenComponent>();
                }

                ImGuiTreeNodeFlags node_flags = ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick;// | ImGuiTreeNodeFlags_DefaultOpen;
                auto opened = ImGui::TreeNodeEx("Transform Gizmo", node_flags);
                if (opened)
                {
                    static int e = 0;
                    static int e2 = 0;
                    static ImGuizmo::OPERATION operations[3] = {ImGuizmo::TRANSLATE, ImGuizmo::ROTATE, ImGuizmo::SCALE};
                    static ImGuizmo::MODE modes[2] = {ImGuizmo::LOCAL, ImGuizmo::WORLD};

                    ImGui::RadioButton("Translation", &e, 0); ImGui::SameLine();
                    ImGui::RadioButton("Rotation", &e, 1); ImGui::SameLine();
                    ImGui::RadioButton("Scale", &e, 2);           

                    ImGui::RadioButton("Local", &e2, 0); ImGui::SameLine();
                    ImGui::RadioButton("World", &e2, 1); 
                    uiState.currentGizmoOperation = operations[e];
                    uiState.currentGizmoMode = modes[e2];
                    ImGui::TreePop();
                }
            }

            if (entity.hasComponent<TransformComponent>()) {
                ImGuiTreeNodeFlags node_flags = ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick | ImGuiTreeNodeFlags_DefaultOpen;
                auto opened = ImGui::TreeNodeEx("Transform", node_flags);
                if (opened)
                {
                    auto& transform = entity.getComponent<TransformComponent>();
                    auto euler = transform.euler;
                    
                    UI_Helpers::vec3Slider("Position",transform.translation,0.01f);
                    UI_Helpers::vec3Slider("Rotation",euler);
                    UI_Helpers::vec3Slider("Scale",transform.scale,0.1f, 80.f, 1.0f);

                    transform.setEulerPitchYawRollDegrees(euler);
                    ImGui::TreePop();
                }
            }
        }

        ImGui::End();
    }
}