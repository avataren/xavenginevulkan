#pragma once
#include <Engine/Scene/Scene.hpp>
#include "UI_State.hpp"
#include <Engine/Renderer/Camera.hpp>

namespace xav::ui
{
    class  UI_Gizmos{
    public:
        UI_Gizmos(Camera& camera, UI_State& uiState, bool* open);
    private:
    };
};
