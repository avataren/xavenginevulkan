#pragma once

#include "UI_State.hpp"
#include "Engine/Core/Context.hpp"

namespace xav::ui{
    class UI_FullscreenWindow{
        public:
        UI_FullscreenWindow(Context& context,UI_State& state);
        
        private:

    };
}