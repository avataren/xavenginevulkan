#include "UI_Asset_Progress_Bars.hpp"
#include "imgui/imgui_internal.h"
namespace xav::ui {
	UI_Asset_Progress_Bars::UI_Asset_Progress_Bars(Renderer& renderer,  bool* open)
	{
		if (!*open)
			return;
		
		ImGui::Begin("Asset loader progress", open, ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoBackground | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoDecoration);
		
        //ImGui::SetNextWindowPos(ImVec2(0,32));
        auto width = WindowManager::getInstance()->width;
        auto height = WindowManager::getInstance()->height;        
        ImGui::SetWindowPos(ImVec2(width/2-200, 50));
        auto progressList = renderer.getAssetProgressList();
		for (auto& it : progressList) {
			std::string name = it.first;
			float pct = it.second;
            ImGui::Text(name.c_str(), "");
            const ImU32 col = ImGui::GetColorU32(ImGuiCol_PlotHistogram);
            const ImU32 bg = ImGui::GetColorU32(ImGuiCol_Button);
            BufferingBar((name+"buffering").c_str(), pct, ImVec2(400, 5), bg, col);
		}
		ImGui::End();
	}

    bool UI_Asset_Progress_Bars::BufferingBar(const char* label, float value, const ImVec2& size_arg, const ImU32& bg_col, const ImU32& fg_col) {
        auto window = ImGui::GetCurrentWindow();
        if (window->SkipItems)
            return false;

        ImGuiContext& g = *ImGui::GetCurrentContext();
        const ImGuiStyle& style = g.Style;
        const ImGuiID id = window->GetID(label);

        ImVec2 pos = window->DC.CursorPos;
        ImVec2 size = size_arg; 
        size.x -= style.FramePadding.x * 2;

        const ImRect bb(pos, ImVec2(pos.x + size.x, pos.y + size.y));
        ImGui::ItemSize(bb, style.FramePadding.y);
        if (!ImGui::ItemAdd(bb, id))
            return false;

        // Render
        const float circleStart = size.x * 0.7f;
        const float circleEnd = size.x;
        const float circleWidth = circleEnd - circleStart;

        window->DrawList->AddRectFilled(bb.Min, ImVec2(pos.x + circleStart, bb.Max.y), bg_col);
        window->DrawList->AddRectFilled(bb.Min, ImVec2(pos.x + circleStart * value, bb.Max.y), fg_col);

        //const float t = g.Time;
        //const float r = size.y / 2;
        //const float speed = 1.5f;

        //const float a = speed * 0;
        //const float b = speed * 0.333f;
        //const float c = speed * 0.666f;

        //const float o1 = (circleWidth + r) * (t + a - speed * (int)((t + a) / speed)) / speed;
        //const float o2 = (circleWidth + r) * (t + b - speed * (int)((t + b) / speed)) / speed;
        //const float o3 = (circleWidth + r) * (t + c - speed * (int)((t + c) / speed)) / speed;

        //window->DrawList->AddCircleFilled(ImVec2(pos.x + circleEnd - o1, bb.Min.y + r), r, bg_col);
        //window->DrawList->AddCircleFilled(ImVec2(pos.x + circleEnd - o2, bb.Min.y + r), r, bg_col);
        //window->DrawList->AddCircleFilled(ImVec2(pos.x + circleEnd - o3, bb.Min.y + r), r, bg_col);
        return true;
    }

}