#pragma once
#include <Engine/Scene/Entity.hpp>
#include <imgui/imgui.h>
#include <imgui/ImGuizmo/ImGuizmo.h>

namespace xav::ui
{
	class UI_State {
	public:
		bool settingsOpen = true;
		bool showMetrix = false;
		bool showPerfMon = false;
		bool showDemo = false;
		bool showSceneTree = true;
		bool showPropertyPanel = true;
		bool propertiesHovered = false;
		ImGuizmo::OPERATION currentGizmoOperation = ImGuizmo::TRANSLATE;
		ImGuizmo::MODE currentGizmoMode = ImGuizmo::LOCAL;
		Entity selectedEntity = Entity::null();
	};
}
