#pragma once
#include <Engine/Core/XavHeaders.hpp>
#include <imgui/imgui.h>
#include <imgui/imgui_internal.h>
#include <string>

namespace xav::ui
{
    class UI_Helpers {
    public:
        static void vec3Slider(const std::string& title, glm::vec3& values, float speed = 0.1f, float columnWidth = 80.0f, float defaultValue = 0.0f) {
            float lineHeight = GImGui->Font->FontSize + GImGui->Style.FramePadding.y * 2.0f;
            ImVec2 buttonSize = {lineHeight+3.0f, lineHeight};
            ImGui::PushID(title.c_str());
            ImGui::Columns(2);
            ImGui::SetColumnWidth(0, columnWidth);
            ImGui::Text("%s", title.c_str());
            ImGui::NextColumn();
            ImGui::PushMultiItemsWidths(3, ImGui::CalcItemWidth());
            ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(0.0f,1.f));
            // X
            ImGui::PushStyleColor(ImGuiCol_Button,          ImVec4{ 0.7f, 0.1f, 0.1f, 1.0f });
            ImGui::PushStyleColor(ImGuiCol_ButtonHovered,   ImVec4{ 0.7f, 0.3f, 0.3f, 1.0f });
            ImGui::PushStyleColor(ImGuiCol_ButtonActive,    ImVec4{ 1.0f, 0.5f, 0.5f, 1.0f });
            ImGui::Button("X", buttonSize);
            ImGui::PopStyleColor(3);
            if (ImGui::IsItemClicked())
            {
                values.x = defaultValue;
            }
            ImGui::SameLine();
            ImGui::DragFloat("##X", &values.x, speed, -10000, 10000, "%.2f");
            ImGui::PopItemWidth();
            ImGui::SameLine();
            // Y
            ImGui::PushStyleColor(ImGuiCol_Button,          ImVec4{ 0.05f, 0.4f, 0.05f, 1.0f });
            ImGui::PushStyleColor(ImGuiCol_ButtonHovered,   ImVec4{ 0.3f, 0.6f, 0.3f, 1.0f });
            ImGui::PushStyleColor(ImGuiCol_ButtonActive,    ImVec4{ 0.5f, 0.9f, 0.5f, 1.0f });
            ImGui::Button("Y", buttonSize);
            if (ImGui::IsItemClicked())
            {
                values.y = defaultValue;
            }
            ImGui::PopStyleColor(3);
            ImGui::SameLine();
            ImGui::DragFloat("##Y", &values.y, speed, -10000, 10000, "%.2f");
            ImGui::PopItemWidth();
            ImGui::SameLine();
            // Z
            ImGui::PushStyleColor(ImGuiCol_Button,          ImVec4{ 0.1f, 0.2f, 0.6f, 1.0f });
            ImGui::PushStyleColor(ImGuiCol_ButtonHovered,   ImVec4{ 0.1f, 0.3f, 0.9f, 1.0f });
            ImGui::PushStyleColor(ImGuiCol_ButtonActive,    ImVec4{ 0.3f, 0.5f, 1.0f, 1.0f });
            ImGui::Button("Z", buttonSize);
            if (ImGui::IsItemClicked())
            {
                values.z = defaultValue;
            }
            ImGui::PopStyleColor(3);
            ImGui::SameLine();
            ImGui::DragFloat("##Z", &values.z, speed, -10000, 10000, "%.2f");
            ImGui::PopItemWidth();
            ImGui::PopStyleVar();
            ImGui::NextColumn();
            ImGui::Columns(1);
            ImGui::PopID();

        }
    };
}