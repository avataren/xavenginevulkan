
#include "OS_Integration/WindowManager.hpp"
#include "Engine/Renderer/Renderer.hpp"
#include <iostream>
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <iomanip>


#ifdef WIN32
#include <Windows.h>
#endif
#ifdef LINUX
#include <dirent.h>
#include <dlfcn.h>
#include <limits.h>
#endif

// references
// https://github.com/jherico/Vulkan
// https://nvpro-samples.github.io/vk_raytracing_tutorial_KHR/
// https://marcelbraghetto.github.io/a-simple-triangle/2019/07/20/part-23/
// https://blog.selfshadow.com/publications/s2013-shading-course/
// https://casual-effects.com/data/  -- various obj scenes
// https://frguthmann.github.io/posts/vulkan_imgui/
// https://github.com/thedmd/imgui-node-editor
// https://github.com/KhronosGroup/Vulkan-Docs/wiki/Synchronization-Examples
// https://www.youtube.com/watch?v=5p0e7YNONr8 -- pbr shaders in glsl
// https://learnopengl.com/PBR/IBL/Diffuse-irradiance
// https://stackoverflow.com/questions/17549906/c-json-serialization/34165367
// https://youtu.be/1tjkSpoa7V8 test metahuman
// https://github.com/skypjack/entt/wiki

int main(int argc, char* argv[]) {
	try
	{
		char runtimePath[MAX_PATH];
		#ifdef WIN32
			GetFullPathNameA(argv[0], MAX_PATH, runtimePath, NULL);
		#endif    
		#ifdef LINUX
			realpath(argv[0], runtimePath);
		#endif

		xav::Config config;
		config.runtimePath = std::string(runtimePath);

		xav::WindowManager wm(1920, 1080,"XavEngine",false);
		//xav::WindowManager wm(3440, 1440, "XavEngine", false);
		xav::Renderer XavRenderer("XavEngine", wm.getWindow(), config);

		double nowfps = glfwGetTime();
		double frames = 1.0;

		while (!wm.shouldClose()) {

			if (!wm.isMinimized())
				XavRenderer.Draw();

			//simple fps counter
			double then = glfwGetTime();
			double delta = then - nowfps;
			if (delta > 0.5f)
			{
				double fps = 1.0 / (delta / frames);
				frames = 1.f;
				std::ostringstream out;
				out << "XavEngine " << std::setprecision(5) << round(fps) << " FPS";
				glfwSetWindowTitle(wm.getWindow(), out.str().c_str());
				nowfps = glfwGetTime();;
			}
			else
				frames += 1.0;
		}
		#ifdef _DEBUG
		//std::cin.get();
		#endif
	}
	catch (vk::SystemError& err)
	{
		std::cout << ansi::foreground_red << "vk::SystemError: " << err.what() << std::endl;
		std::cout << ansi::foreground_white;
		return EXIT_FAILURE;
	}
	catch (std::exception& err)
	{
		std::cout << ansi::foreground_red << "std::exception: " << err.what() << std::endl;
		std::cout << ansi::foreground_white;
		return EXIT_FAILURE;
	}
	catch (...)
	{
		std::cout << ansi::foreground_red << "unknown error\n";
		std::cout << ansi::foreground_white;
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}