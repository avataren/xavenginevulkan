#pragma once

template <typename T>
struct Counter
{
    static inline int objects_created = 0;
    static inline int objects_alive = 0;

    Counter()
    {
        ++objects_created;
        ++objects_alive;
    }

    Counter(const Counter&)
    {
        ++objects_created;
        ++objects_alive;
    }
protected:
    ~Counter()
    {
        --objects_alive;
    }
};